


doubleHelical=1; 
gearHeight=7; 
pitchDiam=45; 
shaftDiam=5; 
teethNum=30; 
addendum=2;
dedendum=2;
toothWidth=2;
angle=-30; 
orientation=-1; 
centerShape=3; 
starNum=6; 
starWidth=2; 
circleNum=7; 
circleDiam=7; 
extrudeOut=1; 
extrudeOutHeight=3;
extrudeOutWidth=7; 
extrudeIn=1; 
extrudeInDiam=pitchDiam/2-dedendum*2;
extrudeInHeight=3;

rome=0; 
romeDiam=pitchDiam/2; 
romeHeight=gearHeight; 
romeTeeth=teethNum*2/3;
romeAdd=2; 
romeDed=2; 
romeToothWidth=2; 
romeAngle=5; 

scale([20,20,20]) { 
  blah(); 
  example(); 
}

module blah() {
  difference() { 
    cube(size=5,center=true);
    sphere(r=3.2); 
  }
}

module example() { 

   difference() { 
       size = 10; 
	   group() {     
        for (i = [1:10]) {
	  	   translate([sin(360*i/size)*15, cos(360*i/size)*15, 0 ]){
			    rotate([0,0,-360*i/size]) 
            cube(size=2,center=true); 
		      }
	      }
      } 
      group() { 
        for (i = [1:10]) {
	  	    translate([sin(360*i/size)*10, cos(360*i/size)*10, 0 ]){
			      rotate([90,0,-360*i/size]) 
              cylinder(h=20,r1=0.5,r2=0.5,center=true); 
		      }
	      }
      } 
      cylinder(5, 15, 15);
  }
   

} 

module tooth(heightGear, diamPitch) {
	toothHeight=addendum+dedendum*2;
	union(){
		translate([0,toothHeight/2,0]) 
    rotateTooth(1,angle,toothWidth,toothHeight,heightGear,dedendum);
		if (doubleHelical==1){
			translate([0,toothHeight/2,-heightGear+0.1]) 
      rotateTooth(-1,angle,toothWidth,toothHeight,heightGear,dedendum);
		}
	}
}

module rotateTooth(direction,angle,toothWidth,toothHeight,heightGear,dedendum){
  difference(){
		rotate([0,direction*angle,0])
		box(toothWidth,toothHeight,heightGear*1.5);
		translate([0,0,heightGear*0.75+dedendum/2])
		box(heightGear,toothHeight+0.5,dedendum+heightGear/2);
		translate([0,0,-heightGear*0.75-dedendum/2])
		box(heightGear,toothHeight+0.5,dedendum+heightGear/2);
	}
}


module helical(numTeeth, diamPitch, heightGear) {
	rootRad=diamPitch/2-dedendum;

   difference() { 
      group() { 
	    for (i = [1:numTeeth]) {
	  	  translate([sin(360*i/numTeeth)*(rootRad-dedendum), cos(360*i/numTeeth)*(rootRad-dedendum), 0 ]){
			    rotate([0,0,-360*i/numTeeth]) tooth(heightGear, diamPitch);
		    }
	    }
     } 

    translate([0,0,-gearHeight/2-1]) 
      cylinder(heightGear+2, diamPitch/2-dedendum, diamPitch/2-dedendum);
   }

}

module solid(heightGear, diamPitch) {
	rootRad=diamPitch/2-dedendum;
	translate([0,0,-(heightGear*doubleHelical)+0.1*doubleHelical])
	cylinder(heightGear+(heightGear-0.1)*doubleHelical, rootRad, rootRad);
}

module star(diamPitch) {
	starAngle=360/starNum;
	rootRad=diamPitch/2-dedendum;
	union(){
		for (s=[1:starNum]){
			translate([0,0,gearHeight/2-(gearHeight/2*doubleHelical)]){
				rotate([0, 0, s*starAngle]) dislocateBox(starWidth, (pitchDiam/2-dedendum*2)/2, gearHeight+(gearHeight*doubleHelical));
			}
		}
		translate ([0,0,-gearHeight*doubleHelical])
		tube(gearHeight+(gearHeight*doubleHelical), pitchDiam/2-dedendum, starWidth);
		translate ([0,0,-gearHeight*doubleHelical]){
			tube(gearHeight+(gearHeight*doubleHelical), pitchDiam/4+dedendum, starWidth);
		}
		translate([0,0,-gearHeight*doubleHelical])
		cylinder(gearHeight+(gearHeight*doubleHelical), shaftDiam/2+starWidth, shaftDiam/2+starWidth);
	}
}

module docircle() {
	rootRad=pitchDiam/2-dedendum;
	difference(){
		solid(gearHeight-0.1*doubleHelical, pitchDiam);
		for (c=[1:circleNum]){
			translate([sin(360*c/circleNum)*(rootRad/2+shaftDiam/2), cos(360*c/circleNum)*(rootRad/2+shaftDiam/2), -gearHeight*doubleHelical]){
				cylinder(gearHeight+gearHeight*doubleHelical, circleDiam/2, circleDiam/2);
			}
		}
	}
}

module romeGear(){
	translate([0,0,romeHeight/2+gearHeight]){
		rotate([0,0,romeAngle]){
			union(){
				helical(romeTeeth, romeDiam, romeHeight);
				translate ([0,0,-romeHeight/2]) solid(romeHeight, romeDiam);
			}
		}
	}
}

module bits() { 
  translate([0,0,gearHeight/2]) helical(teethNum, pitchDiam, gearHeight);

  docircle();

}

module gear(){

	difference () {

		union() {
			translate([0,0,gearHeight/2]) helical(teethNum, pitchDiam, gearHeight);

			if (centerShape==1) solid(gearHeight, pitchDiam);

			if (centerShape==2) star();

			if (centerShape==3) docircle();

			if (rome==1) romeGear();

			if (extrudeOut==1) {
translate([0,0,-gearHeight*doubleHelical+0.1*doubleHelical])	cylinder(gearHeight-(extrudeInHeight*extrudeIn)+extrudeOutHeight+romeHeight*rome+gearHeight*doubleHelical-0.1*doubleHelical, extrudeOutWidth/2+shaftDiam/2, extrudeOutWidth/2+shaftDiam/2);
			}

		}

		if (extrudeIn==1) {
			difference(){
				translate([0,0,gearHeight-extrudeInHeight+0.1*doubleHelical]) cylinder(extrudeInHeight, extrudeInDiam, extrudeInDiam);
				cylinder(gearHeight+extrudeOutHeight, (extrudeOutWidth+shaftDiam)/2*extrudeOut, (extrudeOutWidth+shaftDiam)/2*extrudeOut);
			}
		}

	translate([0,0,-gearHeight*doubleHelical]) cylinder(gearHeight+extrudeOutHeight+romeHeight+(gearHeight*doubleHelical), shaftDiam/2, shaftDiam/2);
	}
}

module dislocateBox(xBox, yBox, zBox){
	translate([0,yBox,0]){
		difference(){
			box(xBox, yBox*2, zBox);
			translate([-xBox,0,0]) box(xBox, yBox*2, zBox);
		}
	}
}

module box(xBox, yBox, zBox) {
	scale ([xBox, yBox, zBox]) cube(1, true);
}

module cone(height, radius) {
		cylinder(height, radius, 0);
}

module oval(xOval, yOval, zOval) {
	scale ([xOval/100, yOval/100, 1]) cylinder(zOval, 50, 50);
}

module tube(height, radius, wall) {
	difference(){
		cylinder(height, radius, radius);
		cylinder(height, radius-wall, radius-wall);
	}
}

module hexagon(height, depth) {
	boxWidth=height/1.75;
		union(){
			box(boxWidth, height, depth);
			rotate([0,0,60]) box(boxWidth, height, depth);
			rotate([0,0,-60]) box(boxWidth, height, depth);
		}
}

module octagon(height, depth) {
	intersection(){
		box(height, height, depth);
		rotate([0,0,45]) box(height, height, depth);
	}
}

module dodecagon(height, depth) {
	intersection(){
		hexagon(height, depth);
		rotate([0,0,90]) hexagon(height, depth);
	}
}

module hexagram(height, depth) {
	boxWidth=height/1.75;
	intersection(){
		box(height, boxWidth, depth);
		rotate([0,0,60]) box(height, boxWidth, depth);
	}
	intersection(){
		box(height, boxWidth, depth);
		rotate([0,0,-60]) box(height, boxWidth, depth);
	}
	intersection(){
		rotate([0,0,60]) box(height, boxWidth, depth);
		rotate([0,0,-60]) box(height, boxWidth, depth);
	}
}

module rightTriangle(adjacent, opposite, depth) {
	difference(){
		translate([-adjacent/2,opposite/2,0]) box(adjacent, opposite, depth);
		translate([-adjacent,0,0]){
			rotate([0,0,atan(opposite/adjacent)]) dislocateBox(adjacent*2, opposite, depth);
		}
	}
}

module equiTriangle(side, depth) {
	difference(){
		translate([-side/2,side/2,0]) box(side, side, depth);
		rotate([0,0,30]) dislocateBox(side*2, side, depth);
		translate([-side,0,0]){
			rotate([0,0,60]) dislocateBox(side*2, side, depth);
		}
	}
}

module SillyStar(height, depth) {
	starNum=3;
	starAngle=360/starNum;
	for (s=[1:starNum]){
		rotate([0, 0, s*starAngle]) box(height, height, depth);
	}
}
