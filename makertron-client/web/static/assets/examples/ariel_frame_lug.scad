// =================================================
// Steering tube lug for 1928 Ariel
// =================================================
module neck_lug(top_dia=1,bottom_dia=1,
                top_flare_height=1,
		bottom_flare_height=1,
                height=1,dia=1,id=1,
                top_tube_dia_a=1,
                top_tube_dia_b=1,
                top_tube_height=1,
                top_tube_offset=1,
                top_tube_angle=1,
                bot_tube_dia_a=1,
                bot_tube_dia_b=1,
                bot_tube_height=1,
                bot_tube_offset=1,
                bot_tube_angle=1
 ) { 

 difference() { 
  union() { 
   cylinder( h = height , d = dia , center=true );
   translate([0,0,-(height/2)+(top_flare_height/2) ]){ 
    cone( r2=dia/2 , r1=top_dia/2 , h=top_flare_height,center=true); 
   } 
   translate([0,0,+(height/2)-(bottom_flare_height/2) ]){
    cone( r1=dia/2,  r2=bottom_dia/2 , h=bottom_flare_height,center=true); 
   }
   translate([dia-10,0,-(height/2)+(top_flare_height/2)+top_tube_dia_a/2 ]){
    rotate([0,90+top_tube_angle,0]) { 
    difference() { 
     cone( r1=top_tube_dia_a/2 , r2=top_tube_dia_b/2 , h = top_tube_height+top_tube_offset , center=true ); 
     cone( r1=(top_tube_dia_a-2)/2 , r2=(top_tube_dia_b-2)/2 , h = top_tube_height+top_tube_offset , center=true ); 
}                    
    }}
    translate([dia-bot_tube_offset,0,+(height/2)-(bottom_flare_height/2)-bot_tube_dia_a/2 ]){
     rotate([0,90-bot_tube_angle,0]) { 
      difference() { 
       cone( r1=bot_tube_dia_a/2 , r2=bot_tube_dia_b/2 , h = bot_tube_height+bot_tube_offset , center=true ); 
       cone( r1=(bot_tube_dia_a-5)/2 , r2=(bot_tube_dia_b-5)/2 , h = bot_tube_height+bot_tube_offset , center=true );                      
      }
    }}
   }
   cylinder( h = height , d = id , center = true);
  } 
}

neck_lug( top_dia = 51.2 , 
          bottom_dia = 56.5 , 
          top_flare_height = 10.5 ,
          bottom_flare_height = 24 , 
          height = 165 , 
          dia = 43 , 
          id = 34.3,
          top_tube_dia_a=41.5,
          top_tube_dia_b=34.5,
          top_tube_height=43,
          top_tube_offset=10,
          top_tube_angle=5.5,     
          bot_tube_dia_a=47.5,
          bot_tube_dia_b=41.5,
          bot_tube_height=165,
          bot_tube_offset=-20,
          bot_tube_angle=29.5
);  
