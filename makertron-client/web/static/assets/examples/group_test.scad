// --------------------------------------------------------
// Standard OpenSCAD code in which groups are optimised.  
// Openscad has no concept of parallelization but it does 
// have groups. They are just 'implied'. That is it will 
// add them automatically to the AST as needed or you can specify 
// them yourself. 
// --------------------------------------------------------
module example_implicit_groups() { 
  difference() { 
    size = 1; 
      for (i = [1:size]) {
	  	  translate([sin(360*i/size)*15, cos(360*i/size)*15, 0 ]) {
			    rotate([0,0,-360*i/size]) 
            cube(size=2,center=true); 
		     }
	     }
       for (i = [1:size]) {
	  	   translate([sin(360*i/size)*10, cos(360*i/size)*10, 0 ]) {
			     rotate([90,0,-360*i/size]) 
             cylinder(h=20,r1=0.5,r2=0.5,center=true); 
		     }
	     }
   cylinder(5, 15, 15);
 }  
} 
//example_implicit_groups(); 

// --------------------------------------------------------
// Example with user defined groups. In this example note 
// that the difference operation may also be wrapped in a group.
// Openscad has made groups explicit or implicit depending on 
// its mood but you cannot assign them to anything as tasks. 
// --------------------------------------------------------
module example_explicit_groups() { 
  difference() { 
    size = 1; 
	  group() {     
      for (i = [1:size]) {
	  	  translate([sin(360*i/size)*15, cos(360*i/size)*15, 0 ]) {
			    rotate([0,0,-360*i/size]) 
            cube(size=2,center=true); 
		      }
	      }
      } 
      group() { 
        for (i = [1:size]) {
	  	    translate([sin(360*i/size)*10, cos(360*i/size)*10, 0 ]) {
			      rotate([90,0,-360*i/size]) 
              cylinder(h=20,r1=0.5,r2=0.5,center=true); 
		      }
	      }
      } 
    cylinder(5, 15, 15);
  }
   
} 
//example_explicit_groups(); 

// --------------------------------------------------------
// With the Makertron groups can have names. Now you can
// divide a job over nodes by giving groups a specific name. 
// In this example literally the names of compute modules 
// on the network. 
// --------------------------------------------------------
module example_explicit_groups_with_names() { 
  difference() { 
    size = 1; 
	  group("pine6400") {     
      for (i = [1:size]) {
	  	  translate([sin(360*i/size)*15, cos(360*i/size)*15, 0 ]) {
			    rotate([0,0,-360*i/size]) 
            cube(size=2,center=true); 
		      }
	      }
      } 
      group("pine6400") { 
        for (i = [1:size]) {
	  	    translate([sin(360*i/size)*10, cos(360*i/size)*10, 0 ]) {
			      rotate([90,0,-360*i/size]) 
              cylinder(h=20,r1=0.5,r2=0.5,center=true); 
		      }
	      }
      } 
    cylinder(5, 15, 15);
  }
   
} 
//example_explicit_groups_with_names();

// --------------------------------------------------------
// This following code performs the same set of geometrical 
// operations but is now optimised to take advantage of a 
// cluster. Note how we have now divided the boolean operation 
// up across two sets of nodes. Also note how the machine names 
// are programmatically available so that they could be generated 
// iteratively if required. 
// --------------------------------------------------------
module secton(star,en,siz,node) { 
       for (i = [star:en]) {
        rotate([0,0,-360*i/siz]) {
        difference() { 
           group(node) {     
               translate([0, 15, 0 ]) {
                rotate([0,0,-360]) 
                cube(size=2,center=true); 
              }
            } 
            group(node) { 
                translate([0, 10, 0 ]) {
                    rotate([90,0,-360]) 
                    cylinder(h=20,r1=0.5,r2=0.5,center=true); 
                }
            }
            cylinder(5, 15, 15);
            }
        }
    }
} 
  
 
group("pine6401") { secton(0,4  ,25,"pine6401"); }
group("pine6400") { secton(5,9  ,25,"pine6400"); }
group("pine6403") { secton(10,14,25,"pine6403"); }
//group("khadas00") { secton(15,19,25,"khadas00"); }


