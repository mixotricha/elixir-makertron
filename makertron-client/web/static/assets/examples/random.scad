



// --------------------------------------------------------
// This following code performs the same set of geometrical 
// operations but is now optimised to take advantage of a 
// cluster. Note how we have now divided the boolean operation 
// up across two sets of nodes. Also note how the machine names 
// are programmatically available so that they could be generated 
// iteratively if required. 
// --------------------------------------------------------
module secton(star,en,siz,node) { 
       for (i = [star:en]) {
        rotate([0,0,-360*i/siz]) {
        difference() { 
           group(node) {     
               translate([0, 15, 0 ]) {
                rotate([0,0,-360]) 
                cube(size=2.3,center=true); 
              }
            } 
            group(node) { 
                translate([0, 10, 0 ]) {
                    rotate([90,0,-360]) 
                    cylinder(h=22,r1=0.7,r2=0.7,center=true); 
                }
            }
            cylinder(4, 14, 14);
            }
        }
    }
} 
  
 
//group("pine6401") { secton(0,4  ,25,"pine6401"); }
group("pine6400") { secton(5,9  ,25,"pine6400"); }
//group("pine6403") { secton(10,14,25,"pine6403"); }
//group("khadas00") { secton(15,19,25,"khadas00"); }


