
/***************************************************************************
 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2017 *
 *                                                                         *
 *   This file is part of the Makertron CSG cad system.                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/
  
	"use strict";

 /*global $,widgets,window,CSG,document,makertron*/
 /*jshint -W069 */

  import {Socket} from "/js/demonseed.js"
 
	// =============================================================
	// Post log results back from worker 
	// =============================================================
	var postLog = function(result) {
		postMessage( JSON.stringify({ type: 'log' , data: result }))
	}

	// =============================================================
	// Post results back from worker 
	// =============================================================
	var postResult = function(result) { 
		postMessage(JSON.stringify({ type: 'result' , data: result }))
	}

  // =============================================================
	// Fetch The Geometry   
	// ====================================================
  const fetchGeometry = (data) => { 
     console.log("Updating Scenes") 
    
     let socket = new Socket("/socket", {params: {token: data.token }}) 
     socket.connect()
      
      // Now that you are connected, you can join channels with a topic:room pair
      let channel = socket.channel("room:lobby", {})
      channel.join()
        .receive("ok", resp => { postLog("Joined successfully " + resp) })
          .receive("error", resp => { postLog("Unable to join" + resp) })	

       channel.push("process_geometry", {body: data.codetree }) 

       channel.on("RESULT", payload => {
        console.log("Got result") 
        postResult( payload.body )  
       })

       channel.on("HELLO", payload => {
        postLog( payload.body) 
       })
  } 
	
  // Output our scene to the renderer 
	onmessage = function(e) {
	  fetchGeometry(e['data'])    
	}
  


