	/***************************************************************************
	 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2018 *
	 *                                                                         *
	 *   This file is part of the Makertron CSG cad system.                    *
	 *                                                                         *
	 *   This library is free software; you can redistribute it and/or         *
	 *   modify it under the terms of the GNU Library General Public           *
	 *   License as published by the Free Software Foundation; either          *
	 *   version 2 of the License, or (at your option) any later version.      *
	 *                                                                         *
	 *   This library  is distributed in the hope that it will be useful,      *
	 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
	 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
	 *   GNU Library General Public License for more details.                  *
	 *                                                                         *
	 *   You should have received a copy of the GNU Library General Public     *
	 *   License along with this library; see the file COPYING.LIB. If not,    *
	 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
	 *   Suite 330, Boston, MA  02111-1307, USA                                *
	 *                                                                         *
	 ***************************************************************************/
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Editor module 
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    'use strict'

    import React from 'react'
    import ReactDOM from 'react-dom'
    import {Editor, EditorBlock , EditorState, ContentState , convertFromHTML  , convertToRaw , convertFromRaw } from 'draft-js';

    import $ from "jquery"
    import async from 'async'
	
    import { saveAs } from 'file-saver'
    import FileDialog from 'file-dialog' 
  
    import { startParse } from '../js/parser.js' 
	
    import PrismDecorator from 'draft-js-prism'
    import CodeUtils from 'draft-js-code'
 
    class Line extends React.Component {
        render() {
            const { block, contentState } = this.props;
                const lineNumber =
                    contentState
                        .getBlockMap()
                        .toList()
                        .findIndex(item => item.key === block.key) + 1;
        return (
            <div className="line" data-line-number={lineNumber}>
              <div className="line-text">
                <EditorBlock {...this.props} />
              </div>
            </div>
        );
        }
    }

    const blockRendererFn = () => ({ component: Line})

    const myBlockStyleFn = (contentBlock) => { return 'superFancyBlockquote'; }

    // --------------------------------------------------------
    // Load up editor 
    // --------------------------------------------------------

    module.exports =  class EditorComponent extends React.Component {

        constructor(props) {
            super(props);
            let decorator = new PrismDecorator({ defaultSyntax: 'javascript' });
            let contentState = convertFromRaw({ entityMap: {} , blocks: [{ type: 'code-block', text: this.props.text }] });
            this.saveScad    = this.saveScad.bind(this)
            this.loadScad    = this.loadScad.bind(this)
            this.refreshData = this.refreshData.bind(this)
            this.onChange = this.onChange.bind(this) 
            this.state = {  firstLoad : true ,  editorState: EditorState.createWithContent(contentState, decorator) };	            
      	}

        // When editor changes update editor state
        onChange(editorState) {
            this.setState({editorState:editorState});
        }

    // Component will update and on first update load scad in to the editorState
    componentDidUpdate() {
        if ( this.state.firstLoad === true && this.props.text !== "" ) {
            console.log("To Here");  
            let decorator = new PrismDecorator({ defaultSyntax: 'javascript' });
            let blockset = this.props.text.split("\n").reduce(function (stack, line) {
                return stack.concat({ type: 'code-block', text: line });
            }, []);
            let contentState = convertFromRaw({ entityMap: {} , blocks: blockset });
            this.setState({ firstLoad: false, editorState: EditorState.createWithContent(contentState, decorator) });
        } 
    } 

	// save scad file 
	saveScad() { 
     let scad = 
        convertToRaw(this.state.editorState.getCurrentContent())
        .blocks 
        .reduce( (stack,token) => stack += token.text , "" ) 
			let blob = new Blob([scad], {type: "text/plain;charset=utf-8"});
	    saveAs( blob , "output.scad" ) 
	} 

	// load scad file 
	loadScad ()  { 
	    let _this = this
		FileDialog( file => {
		    let reader = new FileReader();
		    reader.onload = function(e) {	
                let decorator = new PrismDecorator({ defaultSyntax: 'javascript' });
                let blockset = reader.result.split("\n").reduce(function (stack, line) {
                    return stack.concat({ type: 'code-block', text: line });
                }, []);
                let contentState = convertFromRaw({ entityMap: {} , blocks: blockset });
                _this.setState({ editorState: EditorState.createWithContent(contentState, decorator) });            
			}
			reader.readAsText(file[0]);
	    })
	}

	// Takes the current scad and sends it to server for rendering. 
	refreshData(expStl) {			
        let scad = convertToRaw(this.state.editorState.getCurrentContent()).blocks.reduce(function (stack, token) {
            return stack += "\n" + token.text;
        }, "");
  		let result = startParse(scad)
	  	if ( expStl === false ) this.props.patronus.updateScene(result)
        if ( expStl === true  ) this.props.patronus.exportScene() 	   			 
	}


    textarea() { 
      return <Editor editorState={this.state.editorState}  onChange={this.onChange} blockStyleFn={myBlockStyleFn} blockRendererFn={blockRendererFn} />
    }

	render() {
        return (
            <div>
                <div className="editbar" >
			        <button className="editor_button" type="button" id="load"    onClick={this.loadScad}               >Load SCAD</button>
					<button className="editor_button" type="button" id="save"    onClick={this.saveScad}               >Save SCAD</button>
					<button className="editor_button" type="button" id="stl"     onClick={() => this.refreshData(true)}>Export STL</button> 
					<button className="editor_button" type="button" id="refresh" onClick={() => this.refreshData(false)}>Render</button>
                </div>
                {this.textarea()}	
			</div>
    	);
  	}
	}
					



