/***************************************************************************
 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2017 *
 *                                                                         *
 *   This file is part of the Makertron CSG cad system.                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/
// ------------------------------------------------------
// jsx module using for rendering core  
// ------------------------------------------------------

  'use strict'

	import $ from "jquery"; 
  import React from 'react'
  import ReactDOM from 'react-dom'	
  import async from 'async'
  import { saveAs } from 'file-saver'
  import SplitPane from 'react-split-pane' 

  import ThreeComponent  from './three.jsx'  
  import EditorComponent from './editor.jsx'	
  import ConsoleComponent from './console.jsx'

  import {Socket} from "phoenix"

  // --------------------------------------------------------
	// Generate a hashed string
	// --------------------------------------------------------
	var makeId = function() {
		var text = ""
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
		for( var i=0; i < 5; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length))
		return text
	};

	// ------------------------------------------------------
	// Handle user login and hook up editor if successful 
	// ------------------------------------------------------
	class Login extends React.Component	{
	  constructor(props) {
      super(props)
      this.state = { username: '',password:'' }
    }	
	  onUsernameChange(event) {}
	  onPasswordChange(event) {}
	  error( err ) {} 
	  rejected( data ) {}
	  onLoginClick(event) {}
	  onLogoutClick(event) {}
	  onRegisterClick(event) {}
	  componentWillMount() {}
    render() {
      return (
			  <div className="topbar">
				  <div className="logo">MAKERTRON</div>		    
        </div>				
    	);
  	}
	}

  // ---------------------------------------------------------
  // Tool bar. Empty for now
  // ---------------------------------------------------------
  class Tools extends React.Component	{
	  constructor(props) {
      super(props)
      this.state = {}
    }	
    about() {
		  var url = "https://gitlab.com/mixotricha/elixir-makertron" 
		  window.open(url);
	  }
	  render() {
      return ( 
        <div className="toolbar" >
          <div className="about">
            <button className="about_button" type="button" id="about" onClick={this.about}>About</button>
          </div>
        </div>
      );
    }
  }

  // ---------------------------------------------------------
  // Makertron Core 
  // ---------------------------------------------------------
  export default class MakertronCore extends React.Component {   
    constructor(props) {
      super(props);
        this.state = { resultObjs: [] , log: "" , text: "" , component: false , connected : false , time : 0 , start : 0 }
        this.updateScene = this.updateSceneNoWorker.bind(this);
        this.handleDrag = this.handleDrag.bind(this)	
        this.updateDimensions = this.updateDimensions.bind(this) 
    }

    // turn progress bar on 
    progressOn() { 
      $("#gearstart").css('opacity'  ,   1)
      $("#gearstop").css('opacity'   ,   0)			
    }

    // turn progress bar off
    progressOff() { 
      $("#gearstart").css('opacity'  ,   0)
      $("#gearstop").css('opacity'   ,   0)			
    }

    // stop progress bar
    progressStop() { 
      $("#gearstart").css('opacity'  ,   0)
      $("#gearstop").css('opacity'   ,   1)			
    }

    // output an stl string 
		stlGenerate(obj) { 
		  let stl = "" 
			for ( let i = 0; i < obj.length; i+=3*3 ) {
				stl += "facet normal 1 1 1\n" 
    		stl += "	outer loop\n" 
        stl += "  vertex " + obj[i+2] + " " + obj[i+1] + " " + obj[i+0] + "\n"  
        stl += "  vertex " + obj[i+5] + " " + obj[i+4] + " " + obj[i+3] + "\n"
        stl += "  vertex " + obj[i+8] + " " + obj[i+7] + " " + obj[i+6] + "\n"
				stl += " endloop\n"       
				stl += " endfacet\n"
			}
			return stl
 	  }

    exportScene() { 
      let stl = this.state.resultObjs.reduce( (stl,obj) => stl.concat(obj.map((mesh)=> this.stlGenerate(mesh))) , "solid spewchickens\n" ) 
			stl += "endsolid spewchickens\n"
			let blob = new Blob([stl], {type: "text/plain;charset=utf-8"});
			saveAs( blob , "output.stl" )  
		}

  // update our scene 
    //updateSceneWorker(codeTree) {     
    //  let _this = this 
    //  this.setState({ resultObjs:[] })
    //  this.timer = Date.now() 
    //  let myWorker = new Worker("../js/makertron_worker.js?hash="+makeId(), { type:'module'}) 
    //  myWorker.postMessage( { token : window.userToken , codetree : codeTree } );
    //  myWorker.onmessage = (e)=> {
    //		  let data = JSON.parse(e['data'])['data'] 
    //    if ( data['type'] === "result" ) {
    //      this.updateLog( "Time: " + (Date.now() - _this.timer) / 1000 )  
    //      this.setState({ resultObjs: [...this.state.resultObjs , data['data'] ] })  
    //    }
    //    if ( data['type'] === "log" ) { 
    //      this.updateLog( data['data'] )
    //    }
    //  } 
    //}

    // update our scene 

   updateSceneNoWorker(codeTree) {
      let _this = this;
      this.setState({ resultObjs: [] });
      this.timer = Date.now();
      console.log("Updating Scenes");
      var socket = new Socket("/socket", { params: { token: window.userToken } });
      socket.connect();
      // Now that you are connected, you can join channels with a topic:room pair
      var channel = socket.channel("room:lobby", {});
      channel.join().receive("ok", function (resp) {
        _this.updateLog("Joined successfully " + resp);
      }).receive("error", function (resp) {
        _this.updateLog("Unable to join" + resp);
      });
      channel.push("process_geometry", { body: codeTree });
      channel.on("RESULT", function (payload) {
        console.log("Got result");
        _this.updateLog("Time: " + (Date.now() - _this.timer) / 1000);
        _this.setState({ resultObjs: [..._this.state.resultObjs , payload.body['data'] ] })  
      });
      channel.on("HELLO", function (payload) {
        _this.updateLog(payload.body);
      });
    }

    // output to log 
    updateLog(string) {
      $("#logtext").val($("#logtext").val()+"\n"+string) 
    }

    // handle drag event 
    handleDrag(event) {
      this.setState({ component: true  })	  
    } 

    // return tools component 
    tools() { 
      return (<Tools patronus={this}/>)
    } 

    // return editor component 
    editor() { 
      if ( this.state.text !== undefined ) { 
			  return (<EditorComponent patronus={this} text={this.state.text}/>)
      }
    }

    // return console component 
    console() { 	  
      return (<ConsoleComponent patronus={this} />)		
    }

    //return viewer component 
    viewer() { 	 
		  return (<ThreeComponent patronus={this} data={this.state.resultObjs} />)		
    }

    //return login component 
    login() { 
      return(<Login />)
    }

    // update our viewer dimensions 
    updateDimensions() { 
      this.setState({component:true})
    }

    // Load our default model on component being mounted 
    componentWillMount() { 
      if ( this.state.text === "" ) {
			  $.get( "/examples/ariel_frame_lug.scad", ( data ) => { 
			    this.setState({text:data})
				  this.updateLog("Loading default example...\n") 	
        })
      } 			 
    }

    // We did mount the component. Disable things like context menus from the viewer
    componentDidMount() {
      window.addEventListener("resize", this.updateDimensions)
      $('#threejs').on("contextmenu",()=>{return false;}) 
    }

    // On a change of state decide if we do a componentDidUpdate or not 
    shouldComponentUpdate( nextProps , nextState ) { 
		  return nextState.resultObjs !== this.state.resultObjs || 
             nextState.text !== this.state.text || 
             nextState.log  !== this.state.log ?  true : false 
    }

    // Something in our state changed now update	
    componentDidUpdate() {
		  this.viewer()
		  this.editor()  
		  this.console() 
    }
	
    render() {
      return (
          <div className="whole_page">
          {this.login()}
          {this.tools()}
          <div id="threejs" className="threejs">{this.viewer()}</div>
          <div className="overlay">
            <SplitPane split="vertical" primary="first" defaultSize="50%">
              <div className="kickpanel" />
              <SplitPane split="horizontal" defaultSize="50%">
                {this.editor()} 
                {this.console()}
              </SplitPane>
            </SplitPane>
          </div>
        </div>
      )
    }

  }





