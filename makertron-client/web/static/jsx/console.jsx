	/***************************************************************************
	 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2018 *
	 *                                                                         *
	 *   This file is part of the Makertron CSG cad system.                    *
	 *                                                                         *
	 *   This library is free software; you can redistribute it and/or         *
	 *   modify it under the terms of the GNU Library General Public           *
	 *   License as published by the Free Software Foundation; either          *
	 *   version 2 of the License, or (at your option) any later version.      *
	 *                                                                         *
	 *   This library  is distributed in the hope that it will be useful,      *
	 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
	 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
	 *   GNU Library General Public License for more details.                  *
	 *                                                                         *
	 *   You should have received a copy of the GNU Library General Public     *
	 *   License along with this library; see the file COPYING.LIB. If not,    *
	 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
	 *   Suite 330, Boston, MA  02111-1307, USA                                *
	 *                                                                         *
	 ***************************************************************************/
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Console module 
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	'use strict'

	import React from 'react'
	import ReactDOM from 'react-dom'

  import {Editor, EditorBlock , EditorState, ContentState , convertFromHTML  , convertToRaw } from 'draft-js';

	import $ from "jquery"
	import async from 'async'
	
	// --------------------------------------------------------
	// Load up log console 
	// --------------------------------------------------------

	module.exports =  class ConsoleComponent extends React.Component {

		constructor(props) {
    	super(props)
      this.state = {  content : "" };	
  	}

    // Component will update and on first update load scad in to the editorState
    //componentDidUpdate() {  
    //} 

		// return textarea component 
		textArea() {   
      return <textarea className="logtext" id="logtext" />
		}

		render() {
    	return (
       <div className="console">{this.textArea()}</div>
    	);
  	}
	}
					









