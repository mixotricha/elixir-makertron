	 
		/***************************************************************************
		 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2018 *
		 *                                                                         *
		 *   This file is part of the Makertron CSG cad system.                    *
		 *                                                                         *
		 *   This library is free software; you can redistribute it and/or         *
		 *   modify it under the terms of the GNU Library General Public           *
		 *   License as published by the Free Software Foundation; either          *
		 *   version 2 of the License, or (at your option) any later version.      *
		 *                                                                         *
		 *   This library  is distributed in the hope that it will be useful,      *
		 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
		 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
		 *   GNU Library General Public License for more details.                  *
		 *                                                                         *
		 *   You should have received a copy of the GNU Library General Public     *
		 *   License along with this library; see the file COPYING.LIB. If not,    *
		 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
		 *   Suite 330, Boston, MA  02111-1307, USA                                *
		 *                                                                         *
		 ***************************************************************************/
   	"use strict";   
		/*global require,console,__dirname,Buffer*/
		/*jshint -W069 */ 
//		const lodash = require('lodash') 

    // =================================================================
		// Log errors to console 
		// =================================================================
    const logger = (...args) => {
      console.log( args )  
    } 
  
    // ======================================================================
		// Build list of args originally assigned by other rather than assignment 
		// ======================================================================
    const argsByOrder = (args) => { 
      let keys = Object.keys(args)      
      return keys.map( (key) => key.split("arg_").length !== 1 ? args[key] : false )
              .filter( (fuckoff) => fuckoff !== false ? fuckoff : false ) 
             
    } 

    // =================================================================
		// Group ( takes a node argument unlike OpenSCAD ) 
		// =================================================================
    export const groupParameters = (args) => { 
      return [args["arg_0"]]
    }

  	// =================================================================
		// Openscad translate
		// =================================================================
		export const translateParameters = (args) => { 
		  logger("Translate: ",args )
			// Bit smarter management of arguments Remember first array wins and we reject anything else.   
			let i = 0; 
			let keys = Object.keys(args);
  		let vector = []; 
  		for ( i = 0; i < keys.length; i++ ) {
  			let arg = args[keys[i]];
    		if ( arg instanceof Array) { vector = arg; break; } // Will always take first vector given to us  
  		}
			if ( vector.length === 0 ) logger("Invalid Arguments To Translate: ",args);				
			let x   = parseFloat(vector[0]); 
			let y   = parseFloat(vector[1]); 
			let z   = parseFloat(vector[2]); 
			return [x , y , z] 
		}

		// =================================================================
		// Openscad scale
		// =================================================================
		export const scaleParameters = (args) => { 
		  logger("Scale: ",args)
			// Bit smarter management of arguments Remember first array wins and we reject anything else.   
			let keys = Object.keys(args);
  		let vector = []; 
  		for ( let i = 0; i < keys.length; i++ ) {
  			let arg = args[keys[i]];
    		if ( arg instanceof Array) { vector = arg; break; } // Will always take first vector given to us  
  		}
			if ( vector.length === 0 ) logger("Invalid Arguments To Scale: ",args);				
			let x   = parseFloat(vector[0]) 
			let y   = parseFloat(vector[1]) 
			let z   = parseFloat(vector[2])
			return [x,y,z] 		
		}

		// ==================================================================
		// Openscad Rotate 
		// ==================================================================
		export const rotateParameters = (args) => { 
		  logger("Rotate: ",args)
			let r = Math.PI / 180;
			// Bit smarter management of arguments Remember first array wins and we reject anything else.   
			let keys = Object.keys(args);
  		let vector = []; 
  		for ( let i = 0; i < keys.length; i++ ) {
  			let arg = args[keys[i]];
    		if ( arg instanceof Array)  { // Will always take first vector given to us  
          vector = arg; break; 
        }   
        else { 
          vector = [ 0.0 , 0.0 , arg ] // Otherwise assume it is single value 
        } 
  		}
			if ( vector.length === 0 ) logger("Invalid Arguments To Rotate: ",args);
			let x_rotate   = parseFloat(vector[0]) * r; 
			let y_rotate   = parseFloat(vector[1]) * r; 
			let z_rotate   = parseFloat(vector[2]) * r;
			return [ x_rotate ,  y_rotate , z_rotate ]
		}

    
	  // =======================================================
		// Openscad radius convention for spheres 
		// Note that openscads calling convention for various prims
		// is a bit of a mess and a bit ambiguous but we want our 
		// code to follow it as close as we can. This is transposed
		// from the c++ code in OpenScad 
		// ========================================================
		const lookup_radius = ( diameter_let, radius_let) => {
		  if ( isNaN(diameter_let) === false ) {
			  if (isNaN(radius_let) === false ) {
				  logger("WARNING: Ignoring radius letiable " , radius_let , " as diameter ", diameter_let , " is defined too.");
				}
				return diameter_let / 2.0;
			} else if ( isNaN(radius_let) === false ) {			
				return radius_let;
			} else {
				return undefined;
			}
		}

		// =====================================================
		// Openscad Sphere 
		// =====================================================
	  export const sphereParameters = (args) => { 
		  logger("Sphere: ",args)
			let r = undefined , d = undefined; 
			// Bit smarter management of arguments. Accept arrays. Each value overides a previous value but only one other argument will be size.    
			let keys = Object.keys(args);
  		for ( let i = 0; i < keys.length; i++ ) {
  			let arg = args[keys[i]];
   			if ( typeof(arg) === "number" && keys[i] !== "r" && keys[i] !== "d" ) { 
					r = parseFloat(arg) 
					break 
				}	
  		}			
			if ( args['r'] !== undefined ) { r = parseFloat(args['r']) }
			if ( args['d'] !== undefined ) { d = parseFloat(args['d']) }
			r = lookup_radius( d , r );
			if ( r === 0 || r === undefined ) { r = 1 }
			return [r] 	 	
		} 


    // =====================================================
		// Openscad Circle 
		// =====================================================
	  export const circleParameters = (args) => { 
		  logger("Circle: ",args)
			let r = undefined , d = undefined; 
			// Bit smarter management of arguments. Accept arrays. Each value overides a previous value but only one other argument will be size.    
			let keys = Object.keys(args);
  		for ( let i = 0; i < keys.length; i++ ) {
  			let arg = args[keys[i]];
   			if ( typeof(arg) === "number" && keys[i] !== "r" && keys[i] !== "d" ) { 
					r = parseFloat(arg) 
					break 
				}	
  		}			
			if ( args['r'] !== undefined ) { r = parseFloat(args['r']) }
			if ( args['d'] !== undefined ) { d = parseFloat(args['d']) }
			r = lookup_radius( d , r );
			if ( r === 0 || r === undefined ) { r = 1 }
			return [r] 	 	
		} 


    // =====================================================
		// Openscad Linear Extrude 
		// =====================================================
	  export const linearExtrudeParameters = (args) => { 
		  logger("Linear Extrude: ",args)
			let h = undefined  
			// Bit smarter management of arguments. Accept arrays. Each value overides a previous value but only one other argument will be size.    
			let keys = Object.keys(args);
  		for ( let i = 0; i < keys.length; i++ ) {
  			let arg = args[keys[i]];
   			if ( typeof(arg) === "number" && keys[i] !== "h" ) { 
					h = parseFloat(arg) 
					break 
				}	
  		}			
			if ( args['h'] !== undefined ) { h = parseFloat(args['h']) }
			if ( h === 0 || h === undefined ) { h = 1 }
			return [h] 	 	
		} 

    // ===============================================================
	  // Openscad Cube 
		// ===============================================================

		export const cubeParameters = (args) => {
      logger("Cube:" , args )  
		  let x = 0;
			let y = 0;
			let z = 0;
			let xs = 1; 
			let ys = 1; 
			let zs = 1; 
			let center = false; 
      
			// Bit smarter management of arguments. Accept arrays. Each value overides a previous value but only one other argument will be size.    

			let keys = Object.keys(args);
    	for ( let i = 0; i < keys.length; i++ ) {
    		let arg = args[keys[i]];
      	if ( (arg instanceof Array)) { 
					xs = parseFloat(arg[0]);
					ys = parseFloat(arg[1]);
					zs = parseFloat(arg[2]);
				} 
				else if ( typeof(arg) === "number" ) { 
				  xs = parseFloat(arg);
					ys = parseFloat(arg);
					zs = parseFloat(arg);
				}	
        else 	if ( typeof(arg) === "boolean" ) {
          center = arg
        }
      }			

			if ( center === true ) { 
				x = -(xs / 2);
				y = -(ys / 2); 
				z = -(zs / 2); 
			}			  
			return [x,y,z,xs,ys,zs]	
	  }
   
	  // ===============================================================
		// Openscad Cylinder 
		// ===============================================================
		export const cylinderParameters = (args) => { 
		  logger("Cylinder",args)
			let r  = 1
			let r1 = 1 
			let r2 = 1 
			let h  = 1
			let y = 0 
			let index = 0 
			let center = false 

      if ( args["arg_0"] !== undefined ) h  = args["arg_0"]
      if ( args["arg_1"] !== undefined ) r1 = args["arg_1"]
      if ( args["arg_2"] !== undefined ) r2 = args["arg_2"]

      if ( args["d"]     !== undefined ) { 
        r1 = args["d"] / 2 
        r2 = args["d"] / 2     
      }

      if ( args["r"] !== undefined ) { 
        r1 = args["r"]
        r2 = args["r"] 
      }

      if ( args["r1"] !== undefined ) r1 = args["r1"]
      if ( args["r2"] !== undefined ) r2 = args["r2"]

      if ( args["h"] !== undefined ) h = args["h"]

      if ( args["height"] !== undefined ) h = args["height"]
      if ( args["center"] !== undefined ) center = true 

			// Bit smarter management of arguments. Reject arrays. Each value overides a previous value.    
			/*let keys = Object.keys(args)
  		for ( let i = 0; i < keys.length; i++ ) {
  			let arg = args[keys[i]];
    		if ( !(arg instanceof Array)) { // reject arrays 
					if ( keys[i] === "d" ) { 
						r1 = arg / 2
						r2 = arg / 2  
					}						
					if ( keys[i] === "r" ) { 
						r1 = arg
						r2 = arg
					}
					if ( keys[i] === "r1" ) {	
						r1 = arg; 
					}
					if ( keys[i] === "r2" ) { 
						r2 = arg;
					}
					if ( keys[i] === "h"  ) { 
						h  = arg; 
					}
					if ( keys[i] !== "h" && keys[i] !== "r" && keys[i] !== "r1" && keys[i] !== "r2" && keys[i] !== "d" )  { 
						if ( index === 0 ) { h = arg; }
						if ( index === 1 ) { r1 = arg; }
						if ( index === 2 ) { r2 = arg; }
						// any arg beyond this rejected  
						index++;  					
					}
				}  
  		}			
			// If we are given a single argument 
			if ( typeof(args['d']) === "number"      ) { 
			  r1 = parseFloat(args['d'])/2          
				r2 = parseFloat(args['d'])/2
			}
			if ( typeof(args['r1']) === "number"      ) { r1 = parseFloat(args['r1'])          }
			if ( typeof(args['r2']) === "number"      ) { r2 = parseFloat(args['r2'])          }
			if ( typeof(args['h'] ) === "number"      ) { h =  parseFloat(args['h'] )          }*/ 
			//if ( typeof(args['center']) === "boolean" ) { center =        args['center']       }

			if ( center === true                              ) { y = -(h / 2);                  }			
			if ( r1 !== r2 ) { 
				return [r1,r2,h,y]
			}
			else { 
        console.log("Which kind?") 
				return [r1,h,y] 
			} 
	  } 

    // ===============================================================
		// Openscad Polyhedron 
		// ===============================================================
		export const polyhedronParameters = (args) => { 
      let i = 0
			let ii = 0
      let faces = []    
      let points = [] 
      let face_set = [] 
      let point_set = [[0,0,0]]
      // If faces and points given by argument order take these first ( must have at least two arrays )  
      let argsOrdered = argsByOrder(args[0])  
      if ( argsOrdered.length >= 2 ) {
        point_set = argsOrdered[0]  // points first 
        face_set = argsOrdered[1]   // faces next 
      }
      // points will superceed array that was given by order  
		  if ( args[0]['points'] !== undefined ) {
        point_set = args[0]['points']  
      }
      // If we do not have three points or more we have a degenerate face
			if ( point_set.length < 2 ) { 
			  logger("WARNING: PolySet has degenerate polygons");
				return false 
			}
      // triangles depreciated but take as faces if given 
			if ( args[0]['triangles'] !== undefined ) {
				logger("DEPRECATED: polyhedron(triangles=[]) will be removed in future releases. Use polyhedron(faces=[]) instead."); 
				face_set = args[0]['triangles']
	 		}
      // faces superceeds order argument and triangles if given 
      if ( args[0]['faces'] !== undefined ) { 
        face_set = args[0]['faces'] 
      } 
			// Really want a sanity check that makes sure that indexes in to point space actually return relevent points not just this 
			// trusting this create and fail principle where points defaults to [[0,0,0]].
      point_set = point_set.reverse() 
			// let convexity = args[0]['convexity'] // disregarding convexity for now
			// Openscad if a component of the point is missing just adds it ... 
			for ( i in point_set ) { 
				if ( point_set[i][0] !== undefined ) { points.push( point_set[i][0] ); } else { points.push(0.0); }
				if ( point_set[i][1] !== undefined ) { points.push( point_set[i][1] ); } else { points.push(0.0); }
				if ( point_set[i][2] !== undefined ) { points.push( point_set[i][2] ); } else { points.push(0.0); }
			}
      
  	 return [face_set,points]
		}

    // ===================================================================
		// Create a polgon openscad 
		// ===================================================================
		export const polygonParameters = (args) => { 
      let i = 0
			let ii = 0
      let faces = []    
      let points = [] 
      let face_set = [] 
      let point_set = [[0,0,0]]
      
      let argsOrdered = argsByOrder(args[0]) // If paths and points given by argument order take these    

      if ( point_set = argsOrdered[0] !== undefined ) { point_set = argsOrdered[0] } // take points as first non named argument 

      // key named points will superceed array that was given by order  
		  if ( args[0]['points'] !== undefined ) {
        point_set = args[0]['points']  
      }

      // If we do not have at least three points or we have a degenerate polygon
			if ( point_set.length < 2 ) { 
			  logger("WARNING: PolySet has degenerate polygons");
				return false 
			}

      if ( argsOrdered[1] !== undefined )  { 
        face_set = argsOrdered[1] // take path as second non named argument  
      }
      else { 
         face_set = [point_set.map( (v,ind) => ind )]  
      } 

      // Note that in the context of the polygon faces are called paths by OpenSCAD. I've stuck with the term 
      // face as it fits nicely with the convention for polyhedron also. Paths(faces) superceed order argument 
      if ( args[0]['paths'] !== undefined ) { 
        face_set = args[0]['paths'] 
      } 		
      // faces must be reversed for holes to work properly 
	    //faces = face_set.map( (face) => face.reverse() ) 
      faces = face_set 
      for ( i in point_set ) { 
			  if ( point_set[i][0] !== undefined ) { points.push( point_set[i][0] ); } else { points.push(0.0); }
				if ( point_set[i][1] !== undefined ) { points.push( point_set[i][1] ); } else { points.push(0.0); }
				if ( point_set[i][2] !== undefined ) { points.push( point_set[i][2] ); } else { points.push(0.0); }
			} 

      //console.log( "point set" , points ); 
    
      return [faces,points]
		}
