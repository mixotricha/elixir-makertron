	 /***************************************************************************
	  *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2019 *
	  *                                                                         *
	  *   This file is part of the Makertron CSG cad system.                    *
	  *                                                                         *
	  *   This library is free software; you can redistribute it and/or         *
	  *   modify it under the terms of the GNU Library General Public           *
	  *   License as published by the Free Software Foundation; either          *
	  *   version 2 of the License, or (at your option) any later version.      *
	  *                                                                         *
	  *   This library  is distributed in the hope that it will be useful,      *
	  *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
	  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
	  *   GNU Library General Public License for more details.                  *
	  *                                                                         *
	  *   You should have received a copy of the GNU Library General Public     *
	  *   License along with this library; see the file COPYING.LIB. If not,    *
	  *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
	  *   Suite 330, Boston, MA  02111-1307, USA                                *
	  *                                                                         *
	  ***************************************************************************/

	 "use strict"
	 
	 /*global require */
	 /*jshint -W069   */
	 /*jshint -W049   */
	 /*jshint esversion: 6 */
	 /*jshint asi: true*/ 

	 const lodash = require('lodash')

	 import {
	     groupParameters,
	     translateParameters,
	     scaleParameters,
	     rotateParameters,
	     cubeParameters,
	     sphereParameters,
	     cylinderParameters,
	     polyhedronParameters,
	     polygonParameters,
	     circleParameters,
	     linearExtrudeParameters
	 } from '../js/parseparams.js'


	 // =========================================================================================
	 // Scrubbing code comments out of a buffer 
	 // =========================================================================================
	 const scrubComments = buffer => {
	     let i = 0,block = false,line = false
	     let text = ""
	     let buffer_length = buffer.length
	     for (i = 0; i < buffer_length; i++) {
	         if (buffer[i] === "/" && buffer[i + 1] === "*") {
	             block = true
	         }
	         if (buffer[i] === '*' && buffer[i + 1] === "/" && block === true) {
	             i += 2
	             block = false
	         }
	         if (buffer[i] === "/" && buffer[i + 1] === "/" && block === false) {
	             line = true
	         }
	         if (buffer[i] === '\n' && block === false) {
	             line = false
	         }
	         if (block === false && line === false) {
	             text += buffer[i]
	         }
	     }
	     return text
	 }

	 // =========================================================================================
	 // Lex the input string against regexp set
	 // =========================================================================================
	 const preProcess = buffer => {
	     buffer = scrubComments(buffer)
	     const expressions = [
	         /(\{)/, /(\})/, /(\()/,
	         /(\))/, /(\[)/, /(\])/,
	         /(\;)/, /(\:)/, /(\=)/,
	         /(\+)/, /(\-)/, /(\*)/,
	         /(\<)/, /(\/)/, /(\,)/,
	         /(\#\$\%\^)/, /(module)/, /(function)/, /\n/
	     ]
	     buffer = buffer.replace(/(==)/g, '\#\$\%\^')
	     buffer = buffer.replace(/(!=)/g, '\@\^\!\^')
	     buffer = buffer.replace(/([\t])/g, '')
	     buffer = buffer.split(/ /)
	     expressions.forEach((regExp) => {
	         buffer = lodash.flatten(buffer.map(tkn => tkn.split(regExp)))
	     })
	     buffer = buffer.map(tkn => tkn.replace(/(\#\$\%\^)/g, '===').replace(/(\@\^\!\^)/g, '!=='))
	     return buffer.filter(tkn => tkn !== '' ? true : false)
	 }

	 // =========================================================================================
	 // Generate a hashed string
	 // =========================================================================================
	 const makeId = () => {
	     const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	     return ["0", "0", "0", "0", "0"].map(tkn => tkn + possible.charAt(Math.floor(Math.random() * possible.length))).join('')
	 }

	 // =========================================================================================
	 // Generate a key based on nodes index position 
	 // =========================================================================================
	 const makeCount = (index) => {
	     let length = Math.log(index) * Math.LOG10E + 1 | 0 // length of digits 
	     let str = new Array(16 - length).join("0") // if we need a key larger than 16 digits will fail 
	     return str + index.toString()
	 }

	 // =========================================================================================
	 // Is this token an operation // operations is currently global :( 
	 // =========================================================================================
	 const isToken = (tkn, ops) => {
	     return ops.reduce((stack, op) => op === tkn ? stack = true : stack, false)
	 }

	 // =========================================================================================
	 // Is a token a string 
	 // =========================================================================================
	 const isString = token => {
	     token = token.replace(/([A-Z-a-z])/g, '')
	     token = token.replace(/([0-9])/g, '')
	     token = token.replace(/(['_'])/g, '')
	     token = token.replace(/(['$'])/g, '') // because some variables start with $ 
	     token = token.replace(/(['_'])/g, '')
	     return token.length === 0 ? true : false
	 }

	 // =========================================================================================
	 // Is a token a number ?
	 // =========================================================================================
	 const isNumber = token => {
	     token = token.replace(/([0-9])/g, '')
	     token = token.replace(/([.])/g, '')
	     token = token.replace(/([-])/g, '')
	     token = token.replace(/([+])/g, '')
	     return token.length === 0 ? true : false
	 }

	 // =========================================================================================
	 // Tokens : find matching closure 
	 // =========================================================================================
	 const findPair = (lft, rt, lftCount, rtCount, index, lst, pullThrough) => {
	     if (pullThrough(lst[index]) === lft) {
	         lftCount++
	     }
	     if (pullThrough(lst[index]) === rt) {
	         rtCount++
	     }
	     return (index > lst.length) ? -1 :
	         (lftCount === 0 && rtCount === 0) ? -1 :
	         (lftCount === rtCount) ? index :
	         findPair(lft, rt, lftCount, rtCount, index + 1, lst, pullThrough)
	 }

	 // =============================================================
	 // enumerate through a json data structure 
	 // =============================================================
	 const enumerate = (data, task, eva) => {
	     task(data)
	     data.state = eva(data)
	     return data.state ? enumerate(data, task, eva) : data
	 }

	 // =========================================================================================
	 // Convert stream to string 
	 // =========================================================================================
	 const streamToString = stream => stream.reduce((stack, tkn) => stack + tkn + " ", '')

	 // =========================================================================================
	 // Build list of modules from stream 
	 // =========================================================================================
	 const buildList = (stream, nme) => {
	     let stack = []
	     stream.forEach((element, index) => {
	         if (element === nme) {
	             stack.push(stream[index + 1])
	         }
	     })
	     return stack
	 }

	 // ======================================================
	 // convert function declarations to js functions 
	 // ======================================================
	 // Example : 
	 //function polar_to_cartesian (polar) = [polar[1]*cos(polar[0]),polar[1]*sin(polar[0])];
	 // Becomes : 
	 //function polar_to_cartesian(polar) { return (data) => [ polar[1]*cos(polar[0]), polar[1]*sin(polar[0]) ]; } 
	 const reformatFunctions = stream => {
	     // *** enumerate task func
	     const task = (data) => {
	         let rowA = data.stream[data.index]
	         if (rowA.fBody !== false) {
	             rowA.head = lodash
	                 .flatten(['function', rowA.tkn, '(', rowA.funParam, ')', '{', 'return', rowA.fBody.slice(1, rowA.fBody.length), '}'])
	             data.stream[data.index] = rowA
	         }
	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	             index: 0,
	             stream: stream,
	             len: stream.length,
	             state: true
	         }, task, eva)
	         .stream.reduce((stack, row) => row.tkn !== '' ? stack.concat(row) : stack, [])
	 }

	 // =======================================================
	 // convert module declarations to js functions 
	 // =======================================================
	 const reformatModules = stream => {
	     // *** enumerate task func
	     const task = (data) => {
	         let rowA = data.stream[data.index]
	         if (rowA.mod === true) {
	             rowA.head = lodash.flatten(['function', rowA.tkn, '(', '{', rowA.modParam, '}', ')'])
	             data.stream[data.index] = rowA
	         }
	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	             index: 0,
	             stream: stream,
	             len: stream.length,
	             state: true
	         }, task, eva)
	         .stream.reduce((stack, row) => row.tkn !== '' ? stack.concat(row) : stack, [])
	 }

	 // ==================================================
	 // Reformat parameters to functions to be of a jsn format 
	 // ==================================================
	 const reformatParameters = stream => {
	     // convert an argument string to key:value pairs with non expressions becoming arg_...n key pairs 
	     const argumentsToJson = (parameters) => {
	         if (parameters.length === 1 && parameters[0] === " ") {
	             return ['(', '{', '}', ')']
	         } else {
	             return ['(', '{'].concat(parameters.reduce((stack, ch) => {
	                     if (ch === "[" || ch === "(" || ch === "{") {
	                         stack.depth++
	                     }
	                     if (ch === "]" || ch === ")" || ch === "}") {
	                         stack.depth--
	                     }
	                     stack.result = ch === "," && stack.depth > 0 ? stack.result.concat("|||||") : stack.result.concat(ch)
	                     return stack
	                 }, {
	                     depth: 0,
	                     result: []
	                 })
	                 .result
	                 .reduce((stack, ch) => stack.concat(ch))
	                 .split(",")
	                 .map((para, index) => para.replace("=", ":") === para ? "arg_" + index + ":" + para : para.replace("=", ":"))
	                 .map((p) => p.replace(/\|\|\|\|\|/g, ",") + ",")
	                 .concat(['}', ')']))
	         }
	     }
	     // We will need a slightly different approach to calls to user defined modules 
	     // because order of arguments must become assignment to module definitions key pairs.
	     // convert an argument string to key:value pairs where the keys are taken from the user defined module 
	     const modArgumentsToJson = (rows, index) => {
	         let row = rows[index]
	         if (row.modParam.length === 1 && row.modParam[0] === " ") {
	             return ['(', '{', '}', ')']
	         } else {
	             // Assign all pure values to each key a+j , b/k , 5 , "hi" and assign in order to parameters 
	             // convert all expressions a=b => a:b and assign to parameters 
	             // generate list of keys to match against
	             let mPara = rows
	                 .filter((drow) =>
	                     drow.tkn === row.tkn && drow.mod === true ? true : false
	                 )[0]
	                 .modParam
	                 .reduce((stack, ch) => {
	                     if (ch === "[" || ch === "(" || ch === "{") {
	                         stack.depth++
	                     }
	                     if (ch === "]" || ch === ")" || ch === "}") {
	                         stack.depth--
	                     }
	                     stack.result = ch === "," && stack.depth > 0 ? stack.result.concat("|||||") : stack.result.concat(ch)
	                     return stack
	                 }, {
	                     depth: 0,
	                     result: []
	                 })
	                 .result
	                 .reduce((stack, ch) => stack.concat(ch))
	                 .split(",")
	                 .map((para, index) => para.split("=")[0])
	             // generate list of left hand assignments to assign against 
	             return ['(', '{'].concat(row
	                 .modParam
	                 .reduce((stack, ch) => {
	                     if (ch === "[" || ch === "(" || ch === "{") {
	                         stack.depth++
	                     }
	                     if (ch === "]" || ch === ")" || ch === "}") {
	                         stack.depth--
	                     }
	                     stack.result = ch === "," && stack.depth > 0 ? stack.result.concat("|||||") : stack.result.concat(ch)
	                     return stack
	                 }, {
	                     depth: 0,
	                     result: []
	                 })
	                 .result
	                 .reduce((stack, ch) => stack.concat(ch))
	                 .split(",")
	                 .map((para, index) => para.replace("=", ":") === para ? para : para.replace("=", ":"))
	                 .map((p) => p.replace(/\|\|\|\|\|/g, ",") + ",")
	                 .map((p, index) => p.split(":").length === 1 ? mPara[index] + ":" + p : p)
	                 .concat(['}', ')']))
	         }
	     }
	     // *** enumerating task for reformatCalls
	     const task = (data) => {
	         let row = data.stream[data.index]
	         // if we have module parameters but we are not the module definition 	
	         if (row.modParam !== false && row.mod === false) {
	             row.modParam = modArgumentsToJson(data.stream, data.index)
	             //row.modParam = argumentsToJson( row.modParam )  
	             data.stream[data.index] = row
	         }
	         // if we have csg parameters we will be a csg call 
	         if (row.csgParam !== false) {
	             row.csgParam = argumentsToJson(row.csgParam)
	             data.stream[data.index] = row
	         }
	         data.index++
	         return data
	     }
	     // *** end condition for end of list 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	         state: true,
	         index: 0,
	         stream: stream,
	         len: stream.length
	     }, task, eva).stream
	 }

	 // =============================================================================
	 // Reformats for loop(s) in to js format. Does not handle forEach equivalents yet
	 // =============================================================================
	 const reformatLoops = stream => {
	     // *** enumerateing task 
	     const task = (data) => {
	         let rowA = data.stream[data.index]
	         if (rowA.forParam !== false) {
	             const parameter = rowA.forParam
	             const variable = parameter[0]
	             const fields = parameter.slice(3, parameter.length - 1)
	                 .reduce((stack, tkn) => tkn !== ":" && tkn !== ")" && tkn !== "]" ? stack.concat(tkn) :
	                     stack, [])
	             rowA.head = ['if', '(', variable, '===', 'undefined', ')', '{', 'var', variable, '=', '0', '}']
	             if (isNumber(fields[1]) === false) {
	                 let unique = makeId()
	                 //if ( fields.length === 3 ) { //  [0,45,360] set the size of step here //}
	                 // if fields.length != 2 || 3 are we a forEach? will contain , values if we are
	                 // Since we cannot evaluate ahead of time we must select which direction loop will run in and step direction first
	                 rowA.head = rowA.head.concat([
	                     'let', 'direction_' + unique + ';', 'let', 'step_' + unique + ';',
	                     'if', '(', variable, '<', fields[1], ')', '{',
	                     'direction_' + unique, '=', '(', variable, ',', fields[1], ')', '=>', '{', 'return', variable, '<=', fields[1], '?', 'true', ':', 'false', '}', ';', 'step_' + unique, '=', '1', ';', '}',
	                     'if', '(', variable, '>', fields[1], ')', '{',
	                     'direction_' + unique, '=', '(', variable, ',', fields[1], ')', '=>', '{', 'return', variable, '>=', fields[1], '?', 'true', ':', 'false', '}', ';',
	                     'step_' + unique, '=', '-1', ';', '}'
	                 ])
	                 rowA.head =
	                     rowA.head.concat(["for", '(', variable, "=", fields[0], ";",
	                         'direction_' + unique, '(', variable, ',', fields[1], ')', ";", variable, "+=", 'step_' + unique, ')'
	                     ])
	             } else {
	                 if (fields.length === 2) { // [0,10]
	                     rowA.head = fields[0] < fields[1] ?
	                         rowA.head.concat(["for", '(', variable, "=", fields[0], ";", variable, "<=", fields[1], ";", variable, "++", ')']) // - 
	                         :
	                         rowA.head.concat(["for", '(', variable, "=", fields[0], ";", variable, ">=", fields[1], ";", variable, "--", ')']) //+ 
	                 }
	                 if (fields.length === 3) { //  [0,45,360]
	                     rowA.head = fields[0] < fields[2] ?
	                         rowA.head.concat(["for", '(', variable, "=", fields[0], ";", variable, "<=", fields[2], ";", variable, "+=", fields[1], ')']) //+ 
	                         :
	                         rowA.head.concat(["for", '(', variable, "=", fields[0], ";", variable, ">=", fields[2], ";", variable, "-=", fields[1], ')']) //-   
	                 }
	                 // if fields.length != 2 || 3 are we a forEach? will contain , values if we are
	             }
	             data.stream[data.index] = rowA
	         }
	         data.index++
	         return data
	     }
	     // *** enumerating condition 
	     let eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	         index: 0,
	         stream: stream,
	         len: stream.length,
	         state: true
	     }, task, eva).stream
	 }

	 // =======================================================
	 // convert assignments to 'var'  
	 // =======================================================
	 const reformatVariables = stream => {
	     // *** enumerate task func
	     const task = (data) => {
	         let rowA = data.stream[data.index]
	         if (rowA.varParam !== false) {
	             rowA.head = lodash.flatten(['if (', rowA.tkn, '=== undefined ) { var ', rowA.tkn, ' }', rowA.tkn, rowA.varParam])
	             data.stream[data.index] = rowA
	         }
	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	         index: 0,
	         stream: stream,
	         len: stream.length,
	         state: true
	     }, task, eva).stream
	     //return stream 
	 }

	 // =======================================================
	 // convert general operations to stack operations  
	 // =======================================================
	 const reformatGenOps = stream => {
	     // *** enumerate task func
	     const task = (data) => {
	         let rowA = data.stream[data.index]
	         // general operations 
	         if (rowA.genParam !== false) {
	             rowA.head = lodash.flatten([rowA.tkn, '(', rowA.genParam, ')'])
	             data.stream[data.index] = rowA
	         }
	         // else statements. Note that 'else if' statements will become else { if(){} }  
	         if (rowA.els === true) {
	             rowA.head = lodash.flatten([rowA.tkn])
	             data.stream[data.index] = rowA
	         }
	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	         index: 0,
	         stream: stream,
	         len: stream.length,
	         state: true
	     }, task, eva).stream
	 }

	 // =======================================================
	 // convert csg operations to stack operations  
	 // =======================================================
	 const reformatCsgOps = stream => {
	     // *** enumerate task func
	     const task = (data) => {
	         let rowA = data.stream[data.index]
	         if (rowA.csgParam !== false) {
	             if (rowA.tkn !== "echo") {
	                 rowA.head = lodash.flatten(['stack.push(', rowA.tkn, rowA.csgParam, ')'])
	             } else { // echo becomes console.log for now. Express this better later. 
	                 rowA.head = lodash.flatten(['console.log(', rowA.csgParam, ')'])
	             }
	             data.stream[data.index] = rowA
	         }

	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	         index: 0,
	         stream: stream,
	         len: stream.length,
	         state: true
	     }, task, eva).stream
	 }

	 // =============================================================================
	 // Distributes attributes of tree to appropriate buckets 
	 // =============================================================================
	 const distributeTree = (stream) => {
	     // Any operations we want to call at the JS level but that are 
	     // not directly native like sin/cos/sqrt.  
	     stream = stream.map(tkn => isToken(tkn, trigOps) ? "O" + tkn : tkn)
	     // *** Generate a row entry for a token. Break out arguments if it has any.   
	     const treeToBuckets = (data) => {
	         let tknA = data.stream[data.index]
	         let tknB = data.stream[data.index + 1] // look forward
	         let tknZ = data.stream[data.index - 1] // look backward
	         let nRow = {
	             'id': makeId(), // id of row  
	             'children': {}, // row has children 
	             'terminal': 1, // terminal row ( end of branch ) 
	             'mod': false, // function definition 
	             'csgParam': false, // csg parameters 
	             'genParam': false, // general parameters 
	             'modParam': false, // Module parameters
	             'funParam': false, // Function parameters 
	             'fBody': false, // Function Body 
	             'varParam': false, // Variable parameters 	
	             'forParam': false, // Loop Parameters
	             'els': false, // else part of condition 
	             'parent': '', // parent of row 
	             'head': false, // head of row
	             'tkn': tknA, // tkn of row 
	             'tail': [], // tail of row
	             'closure': [] // trailing closure     
	         }
	         let parameters = false // parameters 
	         let end = -1 // -1 if no end of closure pair found  
	         // if we have a sub chunk always just get it first 
	         if (tknB === "(") {
	             end = findPair('(', ')', 0, 0, data.index + 1, data.stream, tkn => tkn)
	             if (end !== -1) {
	                 parameters = data.stream.slice(data.index + 2, end)
	                 if (parameters.length === 0) {
	                     parameters = [" "]
	                 }
	             }
	         }
	         if (end !== -1) { // we had some parameters  
	             // if we are csgOp or genOp or modOp pick up parameters and drop in correct bucket 
	             if (isToken(tknA, csgOps)) {
	                 nRow.csgParam = parameters
	                 data.index = end
	             }
	             if (isToken(tknA, genOps)) {
	                 if (tknA !== "for") {
	                     nRow.genParam = parameters
	                 } else {
	                     nRow.forParam = parameters // for is general operation but has own arg struc
	                 }
	                 data.index = end
	             }
	             if (isToken(tknA, modOps)) {
	                 nRow.modParam = parameters
	                 if (tknZ === "module") {
	                     nRow.mod = true
	                 }
	                 data.index = end
	             }
	             // functions will also have a function body as well as parameter
	             if (isToken(tknA, funOps)) {
	                 nRow.funParam = parameters
	                 if (tknZ === "function") {
	                     let st = end + 1
	                     end = findPair('=', ';', 0, 0, st, data.stream, tkn => tkn)
	                     if (end !== -1) {
	                         nRow.fBody = data.stream.slice(st, end)
	                     }
	                 }
	                 data.index = end
	             }
	         }
	         // if we are none of the above perhaps we are a variable assignment ?
	         if (nRow.csgParam === false && nRow.genParam === false && nRow.modParam === false && nRow.funParam === false && nRow.fBody === false) {
	             if (isString(tknA) && tknB === "=") {
	                 end = findPair('=', ';', 0, 0, data.index + 1, data.stream, tkn => tkn) // find tkns between = ; 						
	                 nRow.tkn = tknA.replace("$", 'DOLLAR_SIGN_') // dollar signs bad 
	                 nRow.varParam = data.stream.slice(data.index + 1, end + 1)
	                 data.index = end
	             }
	             // if we have an else statement mark it. Becomes special case of general operation
	             if (tknA === "else") {
	                 nRow.els = true
	             }
	         }
	         data.result = data.result.concat(nRow)
	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     // process rows in stream in to sub groups  	
	     return enumerate({
	             state: true,
	             index: 0,
	             stream: stream,
	             len: stream.length,
	             result: []
	         }, treeToBuckets, eva)
	         .result
	         .reduce((stack, row) => row.tkn !== "module" && row.tkn !== "function" ? stack.concat(row) : stack, [])

	 }

	 // =============================================================================
	 // Bulds Par/Child linked list of operations
	 // =============================================================================
	 const buildFullTree = (table) => {
	     // *** Build parent / child relationship graph 
	     const buildRelations = (data) => {
	         let rowA = data.table[data.index]
	         let rowB = data.table[data.index + 1] // now needs to pick up offset somehow 		 
	         // parent child relationship  
	         if (isToken(rowA.tkn, csgOps) || isToken(rowA.tkn, genOps) || isToken(rowA.tkn, modOps)) { //is operation
	             if (rowB.tkn === "{") { // has closure	
	                 data.table = setChildren(data.table, rowA.id, '{', '}', data.index)
	                 data.table[data.index].terminal = 0
	             }
	             if (isToken(rowB.tkn, csgOps) ||
	                 isToken(rowB.tkn, genOps) ||
	                 isToken(rowB.tkn, modOps) ||
	                 isToken(rowB.tkn, funOps)) { // is another operation following on 
	                 data.table[data.index + 1].parent = rowA.id
	                 data.table[data.index].terminal = 0
	             }
	         }
	         data.index++
	         return data
	     }
	     // *** Set group of children within {} to parent id 
	     const setChildren = (table, id, lft, rht, st) => {
	         const end = findPair(lft, rht, 0, 0, st + 1, table, rec => rec.tkn)
	         if (end !== -1) {
	             table.forEach((row, index) => {
	                 if (index > st && index < end) {
	                     row.parent = id
	                 }
	             })
	         }
	         return table
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     // build parent / child relationship and eliminate unwanted tokens 
	     return enumerate({
	             state: true,
	             index: 0,
	             table: table,
	             len: table.length - 1
	         }, buildRelations, eva)
	         .table
	         .reduce((stack, row) =>
	             row.csgParam !== false ||
	             row.genParam !== false ||
	             row.modParam !== false ||
	             row.funParam !== false ||
	             row.fBody !== false ||
	             row.varParam !== false ||
	             row.forParam !== false ||
	             row.els !== false ? stack.concat(row) : stack, [])
	 }

	 // =======================================
	 // construct code back out from tree 
	 // =======================================
	 const codeFromTree = (stream) => {
	     //stream.map( (row) => [{ id: row.id , parent: row.parent , children: row.children ,  token: row.tkn }] ) ) 
	     // All terminal nodes as indicated in build tree function by children === false  
	     const terminals = stream.reduce((stack, row, index) => row.terminal === 1 ? stack.concat(index) : stack, [])
	     // *** enumerate task works from terminals to specified roots to find the 'furthest' child  
	     const task = (data) => {
	         const row = data.stream[data.index]
	         if (row.id === data.id) {
	             if (data.id === data.root) {
	                 data.result = true
	             }
	             data.id = row.parent
	         }
	         data.index--
	         return data
	     }
	     // *** enumerate end 
	     const eva = (data) => {
	         return data.index >= 0 ? true : false
	     }
	     // iterate through every node specifing as root and handing to enumerate task to see if they own the specified terminal node 
	     let cStack = []
	     stream.forEach(row => {
	         let rStack = []
	         terminals.forEach(term => {
	             let state = enumerate({
	                     state: true,
	                     root: row.id,
	                     id: stream[term].id,
	                     index: term,
	                     stream: stream,
	                     result: []
	                 }, task, eva)
	                 .result
	             if (state === true && row.id !== stream[term].id) {
	                 rStack.push(stream[term].id)
	             }
	         })
	         if (rStack.length !== 0) {
	             cStack.push({
	                 root: row.id,
	                 term: rStack[rStack.length - 1]
	             })
	         }
	     })
	     // iterate through stream to find each root and each 'furthest' child. This is where we put our end scope closure  
	     cStack.forEach(pair => {
	         let csg = false,
	             st = '{',
	             en = '}'
	         stream.forEach(row => {

	             // csg operations need closure/scope converted to start/end stack operations	
	             if (pair.root === row.id) {
	                 if (isToken(row.tkn, csgOps)) {
	                     csg = true
	                 }
	             }
	             if (csg === true) {
	                 st = "\nstack.push(start())\n"
	                 en = "\nstack.push(end())\n"
	             }

	             // Things inside loops need to become grouped. The same will also  
	             // apply for module calls. Booleans in OpenSCAD an accumulated list
	             // but some accumulated items are groups. 
	             //if ( row.tkn === "for" ) { 
	             //  if ( pair.root === row.id ) { row.head = ["\nstack.push(group({}))\n","\nstack.push(start())\n"].concat(row.head) }   
	             //	en += "\nstack.push(end())\n" 
	             //} 

	             if (pair.root === row.id) {
	                 row.tail = row.tail.concat(st)
	             }
	             if (pair.term === row.id) {
	                 row.closure = row.closure.concat(en)
	             }
	         })
	     })
	     // generate concat of head tkn argument and tail for each row 
	     stream.forEach((row) => {
	         if (row.head === false) {
	             row.head = [row.tkn]
	             if (row.csgParam !== false) {
	                 row.head = row.head.concat(row.csgParam)
	             }
	             if (row.modParam !== false) {
	                 row.head = row.head.concat(row.modParam)
	             }
	             if (row.genParam !== false) {
	                 row.head = row.head.concat(row.genParam)
	             }
	             if (row.funParam !== false) {
	                 row.head = row.head.concat(row.funParam)
	             }
	             if (row.varParam !== false) {
	                 row.head = row.head.concat(row.varParam)
	             }
	             if (row.forParam !== false) {
	                 row.head = row.head.concat(row.forParam)
	             }
	         }
	         row.head = row.head.concat(row.tail)
	         row.head = row.head.concat(row.closure.reverse()) // because additions were pushed not appended 
	     })
	     return stream
	 }

	 // ========================================================
	 // Basic reformatting of generated code before revaluation 
	 // ========================================================
	 const PolishOutput = stream => {
	     let res = stream.reduce((output, row) => {
	         output += "\n" + streamToString(row.head) + "\n"
	         return output
	     }, "")
	     return res
	 }

	 // =============================================================================
	 // reDistributes attributes of tree to appropriate buckets 
	 // =============================================================================
	 const reDistributeTree = (stream) => {
	     // *** Generate a row entry for a token. Break out arguments if it has any.   
	     const treeToBuckets = (data) => {
	         let tknA = data.stream[data.index]
	         let tknB = data.stream[data.index + 1] // look forward
	         //let tknZ = data.stream[data.index-1] // look backward
	         let nRow = {
	             'node': "pine6400", // default node 
	             'id': makeCount(data.index), // id of row  
	             'children': [], // rows children 
	             'terminal': 1, // is node terminal ( last node in a branch ) 
	             'csgParam': false, // csg parameters 
	             'parent': '', // parent of row 
	             'tkn': tknA, // tkn of row 
	             'tail': [], // tail of row
	             'closure': [], // trailing closure 
	             'fprint': "" // unique finger print   
	         }
	         let parameters = false // parameters 
	         let end = -1 // -1 if no end of closure pair found  
	         // if we have a sub chunk always just get it first 
	         if (tknB === "(") {
	             end = findPair('(', ')', 0, 0, data.index + 1, data.stream, tkn => tkn)
	             if (end !== -1) {
	                 parameters = data.stream.slice(data.index + 2, end)
	                 if (parameters.length === 0) {
	                     parameters = [" "]
	                 }
	             }
	         }
	         if (end !== -1) { // we had some parameters  
	             // if we are csgOp 
	             nRow.csgParam = parameters
	             data.index = end
	         }
	         data.result = data.result.concat(nRow)
	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     // process rows in stream in to sub groups  	
	     return enumerate({
	             state: true,
	             index: 0,
	             stream: stream,
	             len: stream.length,
	             result: []
	         }, treeToBuckets, eva)
	         .result

	 }

	 // =============================================================================
	 // Bulds Par/Child linked list of operations 
	 // =============================================================================
	 const reBuildFullTree = (table) => {
	     // *** Build parent / child relationship graph 
	     const buildRelations = (data) => {
	         let rowA = data.table[data.index]
	         let rowB = data.table[data.index + 1] // now needs to pick up offset somehow 		 
	         if (rowB !== undefined) {
	             // parent child relationship  
	             if (isToken(rowA.tkn, csgOps)) { //is operation
	                 if (rowB.tkn === "{") { // has closure	
	                     data.table = setChildren(data.table, rowA.id, '{', '}', data.index + 1)
	                     data.table[data.index].terminal = 0
	                 }
	                 // Not entirely clear if I need this. Do not think so. All operations at this 
	                 // point are scoped in {} ??? Why did Damien write this in here originally. 
	                 // He does not know. Expect it will reval itself if required. 
	                 //if( isToken(rowB.tkn,csgOps) ) { // is another operation following on 
	                 //  data.table[data.index+1].parent = rowA.id
	                 //  data.table[data.index].terminal = 0  
	                 //}
	             }
	         }
	         data.index++
	         return data
	     }
	     // *** Set group of children within {} to parent id 
	     const setChildren = (table, id, lft, rht, st) => {
	         const end = findPair(lft, rht, 0, 0, st, table, rec => rec.tkn)
	         if (end !== -1) {
	             table.forEach((row, index) => {
	                 if (index > st && index < end) {
	                     row.parent = id
	                 }
	             })
	         }
	         return table
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     // build parent / child relationship and eliminate unwanted tokens 
	     return enumerate({
	             state: true,
	             index: 0,
	             table: table,
	             len: table.length - 1
	         }, buildRelations, eva)
	         .table
	         .reduce((stack, row) => row.csgParam !== false ? stack.concat(row) : stack, [])
	 }

	 // ====================================================================================
	 // Find every parents children and add to parent so we have top down relationship also 
	 // ====================================================================================
	 const everyParentsChildren = (fTree) => {
	     // *** find every parents children  
	     const findChildren = (data) => {
	         let parent = data.ftree[data.index]
	         //if ( parent.children === true || parent.children === false ) parent.children = [] 
	         let kids = []
	         data.ftree.forEach((row) => {
	             if (row.parent === parent.id) {
	                 kids.push(["pine6400", row.id])
	             }
	         })
	         parent.children = kids
	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	         state: true,
	         index: 0,
	         ftree: fTree,
	         len: fTree.length - 1
	     }, findChildren, eva).ftree
	 }

	 // ====================================================================================
	 // pass parameters through parameter handlers to bring down to simple order arity 
	 // ====================================================================================
	 const reduceParameters = (tree) => {
	     // *** find every parents children  
	     const parseParams = (data) => {
	         let row = data.tree[data.index]
	         let param = row.csgParam.reduce((stack, tkn) => stack += tkn)
	         if (row.tkn === "root") {
	             row.parent = "root"
	             data.tree[data.index] = row
	         }

	         if (row.tkn === "group") {
	             row.csgParam = groupParameters(JSON.parse(param))
	             if (row.csgParam[0] === undefined) row.csgParam[0] = "pine6400"
	             data.tree[data.index] = row
	         }

	         if (row.tkn === "cube") {
	             row.csgParam = cubeParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "sphere") {
	             row.csgParam = sphereParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "circle") {
	             row.csgParam = circleParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "cylinder" || row.tkn === "cone") {
	             row.csgParam = cylinderParameters(JSON.parse(param))
	             if (row.csgParam.length === 3) {
	                 row.tkn = "cylinder"
	             }
	             if (row.csgParam.length === 4) {
	                 row.tkn = "cone"
	             }
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "translate") {
	             row.csgParam = translateParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "scale") {
	             row.csgParam = scaleParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "linear_extrude") {
	             row.csgParam = linearExtrudeParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "rotate") {
	             row.csgParam = rotateParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "polyhedron") {
	             row.csgParam = polyhedronParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         if (row.tkn === "polygon") {
	             row.csgParam = polygonParameters(JSON.parse(param))
	             data.tree[data.index] = row
	         }
	         data.index++
	         return data
	     }
	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	         state: true,
	         index: 0,
	         tree: tree,
	         len: tree.length
	     }, parseParams, eva).tree
	 }


	 // ====================================================================================
	 //  Disperse group names to all child nodes. This is a bit clunky as it involves some 
	 // loops and recursion combined together.  
	 // ====================================================================================
	 const disperseGroups = (tree) => {
	     // *** propogate nodes to children   
	     const parseTree = (data) => {
	         let row = data.tree[data.index]
	         // note that a groups node is the same as parent 
	         if (row.tkn == "group") {
	             data.tree[data.index].children = row.children.map((j) => [row.csgParam[0], j[1]])
	             data.tree = walkChildren(row.csgParam[0], row.id, data.tree)
	         }
	         data.index++
	         return data
	     }

	     // *** walk a tree of children 
	     const walkChildren = (node, id, tree) => {
	         var i = 0
	         for (i = 0; i < tree.length; i++) {
	             if (tree[i].parent == id) {
	                 tree[i].node = node
	                 tree[i].children = tree[i].children.map((j) => [tree[i].node, j[1]])
	                 tree = walkChildren(node, tree[i].id, tree)
	             }
	         }
	         return tree
	     }

	     // *** enumerate end condition 
	     const eva = (data) => {
	         return data.index < data.len ? true : false
	     }
	     return enumerate({
	         state: true,
	         index: 0,
	         tree: tree,
	         len: tree.length
	     }, parseTree, eva).tree
	 }

	 // ====================================================================================
	 // Remove nodes that are duplicates of other nodes and re-link parents of those duplicated
	 // nodes to the remaining node. Or to put this another way pipe line the tree and collapse 
	 // everything that is repeated !       
	 // time openscad group_test.scad -o out.stl
	 // ====================================================================================
	 const removeDuplicates = (tree) => {
	     tree[0].parent = tree[0].id
	     // **
	     // I should be able to specify ascending or descending here for sorts but the 
	     // Lodash documentation confuses. Come back and figure out how 
	     // not to use reverse() later.  
	     // **
	     // Order by parents then reverse giving us deepest nodes first
	     let sorted = lodash.orderBy(tree, (o) => o.parent).reverse()
	     // A sort to get the length seems expensive. Living with it for now.
	     let deepest = parseInt((lodash.orderBy(tree, (o) => o.id).reverse())[0].id)
	     // Repad our nodes since the tree arrived contracted so we can use id for direct lookup 
	     // avoiding a findIndex. Presumes the deepest parent ( largest index ) will always be 
	     // at the top of the sorted tree 
	     let padded = tree.reduce((stack, row) => {
	         stack[parseInt(row.id)] = row
	         return stack
	     }, Array(deepest).fill({
	         fprint: ""
	     }))
	     // Build finger print for each node that summarizes its children.   
	     sorted.forEach((row) => {
	         let id = parseInt(row.id)
	         let parent = parseInt(row.parent)
	         padded[id].fprint += padded[id].tkn + padded[id].csgParam
	         padded[parent].fprint += padded[id].fprint
	     })
	     // Now we will order by the generated fprints 
	     let lookup = lodash.orderBy(padded, (o) => o.fprint)
	     // Mark the duplicates and reassign children of duplicates parent
	     // to the alternative matched node. 
	     let rid = -1,
	         rnode = -1
	     lookup.forEach((row, idx) => {
	         if (row.fprint !== "") {
	             if (row.fprint !== lookup[idx - 1].fprint || rid === -1) {
	                 rid = row.id
	                 rnode = row.node
	             } else {
	                 let id = parseInt(row.id)
	                 let parent = parseInt(row.parent)
	                 padded[id].del = true
	                 padded[parent].children = padded[parent].children.reduce((stack, j) => {
	                     if (j[1] === row.id) {
	                         j = [rnode, rid]
	                     }
	                     stack = stack.concat([j])
	                     return stack
	                 }, [])
	             }
	         }
	     })
	     tree = padded.reduce((stack, row) => row.id !== undefined && row.del !== true ? stack.concat(row) : stack, [])
	     tree[0].parent = "root"
	     return tree
	 }

	 //let scad = fs.readFileSync('test.scad', 'utf8')
	 // general operations 
	 const genOps = ["for", "if", "else", "module"]

	 // constructive solids operations 
	 const csgOps = ["root", "difference", "intersection", "union",
	     "circle", "sphere", "translate",
	     "scale", "rotate", "cube","cone",
	     "cylinder", "linear_extrude", "polygon",
	     "polyhedron", "polygon", "echo", "colour", "color", "group"
	 ]

	 // closure operations 
	 //const cloOps = [ ";" , "{" , "}" ] 

	 // Module operations  
	 let modOps = []
	 let funOps = []

	 // Operations that shall be excluded from having arguments 	
	 //const excOps = [ "else" , "function" ] 

	 const trigOps = ["cos", "sin", "atan2", "pow", "sqrt", "max", "min"]

	 export const startParse = (scad) => {

	     const src = preProcess(scad)

	     modOps = buildList(src, "module")
	     funOps = buildList(src, "function")

	     // Start preamble for scriptise
	     let wrapStart = " function foo ( {   } ) {  stack.push( root ( { arg : ' ' } ) ); stack.push(start());"

	     // Header code preamble for scriptise 
	     let header = " let stack = []; const start = () => ['{']; const end = () => ['}']; const root = (...args) => ['root','()']; const union = (...args) => ['union','(',JSON.stringify(args[0]),')'];const difference = (...args) => ['difference','(',JSON.stringify(args[0]),')']; const intersection = (...args) => ['intersection','(',JSON.stringify(args[0]),')']; const translate = (...args) => ['translate','(',JSON.stringify(args[0]),')']; const rotate = (...args) => ['rotate','(',JSON.stringify(args[0]),')']; const scale = (...args) => ['scale','(',JSON.stringify(args[0]),')']; const linear_extrude = (...args) => ['linear_extrude','(',JSON.stringify(args[0]),')']; const sphere = (...args) => ['sphere','(',JSON.stringify(args[0]),')']; const cone = (...args) => ['cone','(',JSON.stringify(args[0]),')']; const cylinder = (...args) => ['cylinder','(',JSON.stringify(args[0]),')']; const color = (...args) => []; const circle = (...args) => ['circle','(',JSON.stringify(args[0]),');']; const echo = (...args) => ['echo','(',JSON.stringify(args[0]),')']; const version = (...args) => ['version','(',JSON.stringify(args[0]),')']; const cube = (...args) => ['cube','(',JSON.stringify(args[0]),')']; const polyhedron = (...args) => ['polyhedron','(',JSON.stringify(args),')'];  const polygon = (...args) => ['polygon','(',JSON.stringify(args),')']; const group = (...args) => ['group','(',JSON.stringify(args[0]),')'];"

	     // Wrappers for Openscad Trig functions for scriptise
	     let trig = " const truncate = (num, places) => num; const deg2rad  = (deg) => deg * (Math.PI/180);  const rad2deg  = (rad) => (rad * 180)/Math.PI; const Ocos     = (rad) => truncate(Math.cos(deg2rad(rad)),4); const Osin     = (rad) => truncate(Math.sin(deg2rad(rad)),4); const Oatan2   = (a,b) => truncate(Math.atan2(a,b),4); const Opow     = (a,b) => truncate(Math.pow(a,b),4);   const Osqrt    = (a)   => truncate(Math.sqrt(a),4);    const Omax     = (a,b) => truncate(Math.max(a,b),4);   const Omin     = (a,b) => truncate(Math.min(a,b),4);"

	     // End wrapper for scriptise
	     let wrapEnd = " stack.push(end()); } "
	     let footer = "foo({}); return stack"

	     // Generate parent/child tree
	     // Reformat functions
	     // Reformat modules  
	     // Reformat parameters
	     // Reformat csg operations 
	     // Reformat variable assignments 
	     // Reformant general operations 
	     // Reformat loops 
	     const newTree = reformatLoops(reformatGenOps(
	         reformatVariables(reformatCsgOps(reformatParameters(reformatModules(reformatFunctions(buildFullTree(distributeTree(src)))))))))

	     // Build new code from tree
	     const result = header + trig + wrapStart + PolishOutput(codeFromTree(newTree)) + wrapEnd + footer

	     console.log(JSON.stringify(result))

	     // execute 'scriptised' result as js 
	     let stack = new Function(result)()
	     let output = stack.map(row => streamToString(row))

	     //console.log( streamToString(output) )

	     // rebuild unrolled result in to new tree
	     let fTree = reBuildFullTree(reDistributeTree(preProcess(streamToString(output))))

	     //console.log( fTree ) 

	     // generate parent child relationship 
	     let nTree = everyParentsChildren(fTree)

	     // reduce parameters down to arity order and and group relationship	
	     let res = removeDuplicates(disperseGroups(reduceParameters(nTree)))

	     console.log("res", res)

	     // compose final tree for sending to server
	     let composition = res.reduce((stack, row) =>
	         stack.concat({
	             node: row.node,
	             id: row.id,
	             parent: row.parent,
	             children: row.children,
	             tkn: row.tkn,
	             param: row.csgParam
	         }), [])

	     //console.log( JSON.stringify(composition)  ) 

	     return composition

	 }
