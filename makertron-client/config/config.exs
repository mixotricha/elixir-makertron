# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :makertron_client, MakertronClient.Endpoint,
  url: [host: "localhost"],
#  secret_key_base: "P+7R0c/XScITsfoeBzbVgI55gmZGeGrnqR06lMRtF31Ez7LyRa4HC6cQD2HQoDcb",
  render_errors: [view: MakertronClient.ErrorView, accepts: ~w(html json)],
  pubsub: [name: MakertronClient.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
#config :logger, :console,
#  format: "$time $metadata[$level] $message\n",
#  metadata: [:request_id]

# tell logger to load a LoggerFileBackend processes
config :logger,
  backends: [{LoggerFileBackend, :error_log}]

# configuration for the {LoggerFileBackend, :error_log} backend
config :logger,:error_log,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id],
  path: "/var/log/makertron_client/error.log"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
