defmodule MakertronClient.Process do
  @doc """
   ***************************************************************************
   *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2019 *
   *                                                                         *
   *   This file is part of the Makertron CSG cad system.                    *
   *                                                                         *
   *   This library is free software; you can redistribute it and/or         *
   *   modify it under the terms of the GNU Library General Public           *
   *   License as published by the Free Software Foundation; either          *
   *   version 2 of the License, or (at your option) any later version.      *
   *                                                                         *
   *   This library  is distributed in the hope that it will be useful,      *
   *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
   *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
   *   GNU Library General Public License for more details.                  *
   *                                                                         *
   *   You should have received a copy of the GNU Library General Public     *
   *   License along with this library; see the file COPYING.LIB. If not,    *
   *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
   *   Suite 330, Boston, MA  02111-1307, USA                                *
   *                                                                         *
   ***************************************************************************
  """

  # alias Nodex.Cnode
  require Logger
  require MakertronClient.Hooks
  use Phoenix.Channel

  @doc """
  Load json from test file 
  """
  def load_json(path) do
    {state, json} = File.read!(path) |> Poison.decode()
    { state , json } 
  end

  @doc """
    Start a whole session. This counts on us having the
    brep module available for whole session.
  """
  def start(body, socket) do
    Nodex.Distributed.up()
    # {state,body} = File.read!("test7.dat") |> Poison.decode()
    process_tree(body, socket)
  end

  @doc """
    Start genserver for each element in tree putting pid in to map  
  """
  def build_pids(json) do
    json
    |> Enum.reduce(
      %{},
      &Map.put(&2, String.to_atom(&1["node"] <> &1["id"]), MakertronClient.Hooks.start_link())
    )
  end

  @doc """
  Load from file 
  """
  def load_local(name) do
    {_state, json} = File.read!(name) |> Poison.decode()
    process_tree(json, "")
  end

  @doc """
  Begin processing the json tree
  """
  def process_tree(json, socket) do
    start = System.monotonic_time(:microsecond)
    pid_map = build_pids(json)
    root = Enum.at(json, 0)
    # fire all nodes in tree aside from root node  
    json
    |> Enum.map(
      &MakertronClient.Hooks.fire(
        self(),
        {String.to_atom(&1["tkn"])},
        &1["node"],
        &1["id"],
        List.to_tuple(&1["param"]),
        &1["children"],
        pid_map
      )
    )
    # Root process waits for all others to complete
    MakertronClient.Hooks.fire(
      self(),
      {:huffle},
      root["node"],
      root["id"],
      {},
      root["children"],
      pid_map
    )
    # Handle process messages
    recv(json, root, pid_map, socket, start)
  end

  def output(msg, type, socket) do
    # IO.puts "#{inspect(msg)}"
    push(socket, type, %{body: msg})
  end

  @doc """
  Handle all received messages for process. Recurse only when we get a new message. 
  ret is passed back with each successive call until it is handled by the final case
  in complete_geometry. 
  """
  def recv(json, root, pid_map, socket, start) do
    receive do
      {:complete_node} ->
        output(%{"type" => "log", "data" => "Node Complete"}, "HELLO", socket)
        # time_end = (System.monotonic_time(:micro_seconds) - start) * 0.000001
        # IO.puts "Time: #{inspect(time_end)} "
        recv(json, root, pid_map, socket, start)
      {:complete_root} ->
        output(%{"type" => "log", "data" => "Root Complete"}, "HELLO", socket)
        MakertronClient.Hooks.fire(
          self(),
          {:generate_geometry},
          root["node"],
          root["id"],
          {},
          root["children"],
          pid_map
        )
        recv(json, root, pid_map, socket, start)
      {:complete_export} ->
        output(%{"type" => "log", "data" => "Export Complete"}, "HELLO", socket)
        json
        |> Enum.map(
          &MakertronClient.Hooks.fire(self(), {:delete}, &1["node"], &1["id"], {}, [], pid_map)
        )
        json
        |> Enum.map(
          &MakertronClient.Hooks.fire(self(), {:terminate}, &1["node"], &1["id"], {}, [], pid_map)
        )
      {:complete_geometry, result} ->
        output(%{"type" => "log", "data" => "Geometry Complete"}, "HELLO", socket)
        ret =
          case result do
            {true, meshes} -> %{"type" => "result", "data" => meshes}
            #{_, meshes} -> %{"type" => "log", "data" => "Failed To Do Stuff"}
          end
        IO.puts("Geometry sent to client #{inspect(result)}")
        output(ret, "RESULT", socket)
        json
        |> Enum.map(
          &MakertronClient.Hooks.fire(self(), {:delete}, &1["node"], &1["id"], {}, [], pid_map)
        )
        json
        |> Enum.map(
          &MakertronClient.Hooks.fire(self(), {:terminate}, &1["node"], &1["id"], {}, [], pid_map)
        )
      {:err} ->
        output("Node Had Known Error", "HELLO", socket)
      {:unknown} ->
        output("Node Had Uknown Error", "HELLO", socket)
      {nil} ->
        output("Node fell through on atomic nil!", "HELLO", socket)
    end
  end
end
