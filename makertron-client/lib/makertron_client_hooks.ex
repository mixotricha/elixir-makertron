defmodule MakertronClient.Hooks do

  @doc """
   ***************************************************************************
   *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2019 *
   *                                                                         *
   *   This file is part of the Makertron CSG cad system.                    *
   *                                                                         *
   *   This library is free software; you can redistribute it and/or         *
   *   modify it under the terms of the GNU Library General Public           *
   *   License as published by the Free Software Foundation; either          *
   *   version 2 of the License, or (at your option) any later version.      *
   *                                                                         *
   *   This library  is distributed in the hope that it will be useful,      *
   *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
   *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
   *   GNU Library General Public License for more details.                  *
   *                                                                         *
   *   You should have received a copy of the GNU Library General Public     *
   *   License along with this library; see the file COPYING.LIB. If not,    *
   *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
   *   Suite 330, Boston, MA  02111-1307, USA                                *
   *                                                                         *
   ***************************************************************************
  """

  # Using GenServer
  use GenServer
  alias Nodex.Cnode
  #use System 

  @doc """ 
  Start The Gen Server.
  """  
  def start_link() do
    {:ok, pid} = GenServer.start_link(__MODULE__,false)
    {:ok, pid}
  end

  @doc """ 
  Init process state.
  """
  def init(default) do
    {:ok, default}
  end

  @doc """
  Calling process fires long lived tasks in to process here.
  """   
  def fire( caller , tkn , compute_node , id , parameters , children, pid_map ) do 
    # address of compute node
    addr = String.to_atom( "bar@" <> compute_node ) 
    # get pid of this process from map 
    { _ , pid } = pid_map[ String.to_atom( compute_node<>id ) ] 
    
    msg = case tkn do 

      { :ping } -> { :ping , true } 

      { :delete } -> 
        msg = { :delete , {compute_node , id} } 
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,:complete_node}) end)  
 
      { :terminate } -> 
        send(pid, {:kill})

      { :circle } -> 
        r = elem( parameters , 0 ) / 1
        msg = { :circle , { compute_node , id , { r } } } 
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,:complete_node}) end)   

      { :cube } ->          
        x = elem( parameters , 0 ) / 1
        y = elem( parameters , 1 ) / 1
        z = elem( parameters , 2 ) / 1
        xs = elem( parameters , 3 ) / 1
        ys = elem( parameters , 4 ) / 1
        zs = elem( parameters , 5 ) / 1    
        msg = {:cube, { compute_node , id , { x , y, z , xs , ys , zs } } }
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,:complete_node}) end)   
 
      { :sphere } -> 
        r = elem( parameters , 0 ) / 1
        msg = { :sphere , { compute_node , id , { r , 0.0, 0.0 , 0.0 } } } 
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,:complete_node}) end)   

      { :cylinder } -> 
        r1 =   elem( parameters, 0) / 1
        h =    elem( parameters, 1) / 1
        cent = elem( parameters, 2) / 1
        msg = { :cylinder , { compute_node , id , { r1 , h, cent } } } 
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,:complete_node}) end)   

      { :cone } -> 
        r1 =   elem( parameters, 0) / 1
        r2 =   elem( parameters, 1) / 1
        h =    elem( parameters, 2) / 1
        cent = elem( parameters, 3) / 1
        msg = { :cone , { compute_node , id , { r1 , r2, h, cent } } } 
        IO.puts "Cones is cones!"
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,:complete_node}) end)   

      { :polygon } -> 
        faces = list_to_tuple(elem( parameters , 0 ))  
        points = List.to_tuple(Enum.map( elem(parameters,1) , fn x -> x/1.0 end ))  
        msg = { :polygon , { compute_node , id ,  faces , points  } } 
        IO.puts "Call: #{inspect(msg)}" 
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,:complete_node}) end)  

      { :translate }  -> 
        x = elem( parameters , 0 ) / 1
        y = elem( parameters , 1 ) / 1
        z = elem( parameters , 2 ) / 1
        msg = { :translate , { compute_node , id ,  { x, y, z } , list_to_tuple(children) } }
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_node}) end)           

      { :rotate } -> 
        x = elem( parameters , 0 ) / 1
        y = elem( parameters , 1 ) / 1
        z = elem( parameters , 2 ) / 1
        msg = { :rotate , { compute_node , id ,  { x, y, z } , list_to_tuple(children) } }
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_node}) end)           

      { :scale } -> 
          x = elem( parameters , 0 ) / 1
          y = elem( parameters , 1 ) / 1
          z = elem( parameters , 2 ) / 1
          msg = { :scale , { compute_node , id ,  { x, y, z } , list_to_tuple(children) } }
          Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_node}) end)           
 
      { :union } -> 
        msg = { :union ,  { compute_node , id , list_to_tuple(children) } }
        Node.spawn_link( addr, fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_node}) end)           

     { :difference } -> 
        msg = { :difference ,  { compute_node , id , list_to_tuple(children) } }
        Node.spawn_link( addr, fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_node}) end)           

     { :intersection } -> 
        msg = { :intersection ,  { compute_node , id , list_to_tuple(children) } }
        Node.spawn_link( addr, fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_node}) end)           

     { :group } -> 
        msg = { :compound ,  { compute_node , id , list_to_tuple(children) } }
        Node.spawn_link( addr, fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_node}) end)           

      { :huffle } -> 
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.root({caller,pid,children,pid_map,:complete_root}) end) 

      { :export } -> 
        msg = { :export , { compute_node , id , list_to_tuple(children) } } 
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_export}) end)           

      { :generate_geometry } -> 
        msg = { :fetch_geometry , { compute_node , id , list_to_tuple(children) } } 
        Node.spawn_link( addr , fn -> MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,:complete_geometry}) end)           

      { _ } -> false 

    end

  end

  @doc """ 
  Convert series of lists to tuples. 
  """  
  def list_to_tuple(list) do
    list |> Enum.map( &List.to_tuple(&1) ) |> List.to_tuple  
  end 

  @doc """ 
  Root must until all other nodes complete. Sends message to calling process.
  Root is different from other operations. It only waits and spawns no cnode.  
  """
  def root({caller , pid ,  children ,  pid_map,op}) do 
    if is_complete(children,pid_map) == true do 
      send(pid, {:finished,caller,op,{}})
    else 
      MakertronClient.Hooks.root( {caller , pid ,  children , pid_map,op} )
    end 
  end

  @doc """
  Send message. 
  """
  def send_message(cnode_pid,pid, msg) do   
    send(cnode_pid,{:kill})
    IO.puts "Call: #{inspect(msg)}"
    send(pid,msg)
  end
  
  @doc """
  Brep with no children. Will not recurse. Sends message to process self.
  """
  def operation({caller,pid , msg , op }) do   
    { state , cnode_pid } = Cnode.start_link(%{exec_path: "bin/brep" }) 
    case Cnode.call(cnode_pid, msg , :ping ) do     
      { :ok  , result } -> 
        send_message(cnode_pid,pid,{:finished,caller,op,result})
      { :err , result } ->
        send_message(cnode_pid,pid, {:finished,caller,:err,result})
      { _    , result } ->
        send_message(cnode_pid,pid,{:finished,caller,:unknown,result})   
      nil               ->
       send_message(cnode_pid,pid,{:finished,caller,:nil,{}})
     end  
  end

  @doc """ 
  Brep with children. Will recurse until children are complete, Send message to process self. 
  """
  def operation({caller , pid , msg , children ,  pid_map , op }) do 
    if is_complete(children,pid_map) == true do 
      { state , cnode_pid } = Cnode.start_link(%{exec_path: "bin/brep" })
      case Cnode.call(cnode_pid, msg , :ping ) do    
        { :ok  , result } -> 
          send_message(cnode_pid,pid,{:finished ,caller,op,result})
        { :err , result } -> 
          send_message(cnode_pid,pid, {:finished    ,caller,:err,result})
        { _    , result } -> 
          send_message(cnode_pid,pid, {:finished    ,caller,:unknown,result})   
        nil               -> 
          send_message(cnode_pid,pid, {:finished    ,caller,:nil,{}})
        end
    else 
      MakertronClient.Hooks.operation({caller,pid,msg,children,pid_map,op})
    end 
  end

  @doc """ 
  Check all the children pids to see if they are finished. 
  """
  def is_complete(children,pid_map) do
    if children |> Enum.map( fn x -> 
        name = Enum.at(x,0) <> Enum.at(x,1)
        { _, pid } = pid_map[ String.to_atom( name )]
        try do 
          :sys.get_state(pid) 
        rescue
          e in RuntimeError -> false
        end        
        end ) 
       |> Enum.find(&(&1 == false)) == false 
    do 
      false 
    else 
      true 
    end
  end 

  @doc """
  Response to self sends message to caller. Internal message path should always pass through here.  
  """
  def handle_info({:finished,caller,code,result}, state) do 
    case code do 
      :complete_node     -> send( caller , {:complete_node  })
      :complete_root     -> send( caller , {:complete_root  })
      :complete_export   -> send( caller , {:complete_export})
      :complete_geometry -> send( caller , {:complete_geometry,result}) 
      :err               -> send( caller , {:err}) 
      :unknown           -> send( caller , {:unknown}) 
      :nil               -> send( caller , {:nil})  
      nil                -> send( caller , {:nil}) 
    end
    {:noreply, code }
  end 

  @doc """
  Respond to kill request from caller. 
  """
  def handle_info({:kill}, state) do
    #IO.puts "Terminateing Process" 
    {:stop, :normal, state}
  end

  @doc """
  Respond to any messages from anywhere not defined with a process kill. 
  """
  def handle_info(_, state) do
    {:stop, :error , state}
  end

end 





