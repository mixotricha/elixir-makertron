defmodule MakertronClient.Channels do
  use Phoenix.Channel
  require MakertronClient.Process

  def join("room:lobby", _message, socket) do
    {:ok, socket}
  end

  def join("room:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in("process_geometry", %{"body" => body}, socket) do
    MakertronClient.Process.start(body,socket)
    {:noreply, socket}
  end
end
