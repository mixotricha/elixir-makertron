defmodule MakertronTest do
 
  use ExUnit.Case
  doctest MakertronClient.Process

  describe "Implementation tests against test.dat stub" do

    test "Loads Json File" do
      assert {:ok,json} = MakertronClient.Process.load_json("test.dat") 
      assert MakertronClient.Process.index_of( json , "0U0i0g0403" ) == 4
      assert object_node = Enum.at(json,0)  
    end

    test "Tests some functions" do
      assert MakertronClient.Process.has_children([ [ "pine6401", "0k0v010F0H" , true ] , [ "pine6401", "0U0i0g0403" , true ]])  
      assert MakertronClient.Process.has_children([]) == false  
      assert MakertronClient.Process.children_complete([ [ "pine6401", "0k0v010F0H" , true ] , [ "pine6401", "0U0i0g0403" , true ]])
      assert MakertronClient.Process.children_complete([ [ "pine6401", "0k0v010F0H" , false ] , [ "pine6401", "0U0i0g0403" , true ]]) == false
    end

  end



  #test "Terminal End Nodes" do
  #  assert MakertronClient.Process.terminal_end_nodes(%{"terminal" => 1}) == true  
  #end 

  #test "Parses Json Tree" do
  #  assert Enum.at(Makertron.parse_over(Makertron.get_json()),0)["parent"] == "root"
  #end

  #test "Children are complete" do
  #  assert  MakertronClient.Process.children_complete( %{"P5YbO" => [1, 2, 3], "foo" => [1, 2, 3]} ) == true
  #  assert  MakertronClient.Process.children_complete(%{"P5YbO" => [1, 2, 3], "foo" => []})        == false
  #end

  #test "Node has children" do
  #  assert MakertronClient.Process.has_children(%{"P5YbO" => [1, 2, 3], "foo" => [1, 2, 3]}) == true 
  #  assert MakertronClient.Process.has_children(%{})                                         == false 
  #end 

  #test "All Children In Root Node Completed" do 
  #  assert Enum.all?( Enum.at(Makertron.parse_over(Makertron.get_json()),0)["children"], &is_atom/1) == true
  #end

end
