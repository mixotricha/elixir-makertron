#include <gp.hxx>
#include <gp_Pln.hxx>
#include <gp_Ax2.hxx>
#include <gp_Circ.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Face.hxx>
#include <TopExp_Explorer.hxx>
#include <Poly_Triangulation.hxx>

#include <Bnd_Box.hxx>
#include <BRepBndLib.hxx>
#include <OSD_Path.hxx>
#include <OSD_OpenFile.hxx>
#include <RWStl.hxx>

#include <StlAPI_Writer.hxx>

//#include <StlMesh_Mesh.hxx>
//#include <StlTransfer.hxx>

#include <BRep_Tool.hxx>
#include <BRepTools.hxx>

#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>

#include <BRepFeat_MakeCylindricalHole.hxx>
#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>

#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_GTransform.hxx>
#include <BRepBuilderAPI_MakeSolid.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepOffsetAPI_Sewing.hxx>

// Brep To CGAL conversion 

#include <ReadWrite.h>
 
// Streams 
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// Math
#include <math.h>
#include <float.h>
#include <cmath>
#include <assert.h>

// Boost
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.

// Postgres
#include <pqxx/pqxx>

using namespace std;

// Auxiliary tools
namespace
{
  // Tool to get triangles from triangulation taking into account face
  // orientation and location
  class TriangleAccessor
  {
  public:
    TriangleAccessor (const TopoDS_Face& aFace)
    {
      TopLoc_Location aLoc;
      myPoly = BRep_Tool::Triangulation (aFace, aLoc);
      myTrsf = aLoc.Transformation();
      myNbTriangles = (myPoly.IsNull() ? 0 : myPoly->Triangles().Length());
      myInvert = (aFace.Orientation() == TopAbs_REVERSED);
      if (myTrsf.IsNegative())
        myInvert = ! myInvert;
    }

    int NbTriangles () const { return myNbTriangles; } 

    // get i-th triangle and outward normal
    void GetTriangle (int iTri, gp_Vec &theNormal, gp_Pnt &thePnt1, gp_Pnt &thePnt2, gp_Pnt &thePnt3)
    {
      // get positions of nodes
      int iNode1, iNode2, iNode3;
      myPoly->Triangles()(iTri).Get (iNode1, iNode2, iNode3); 
      thePnt1 = myPoly->Nodes()(iNode1);
      thePnt2 = myPoly->Nodes()(myInvert ? iNode3 : iNode2);
      thePnt3 = myPoly->Nodes()(myInvert ? iNode2 : iNode3);	

      // apply transormation if not identity
      if (myTrsf.Form() != gp_Identity)
      {
        thePnt1.Transform (myTrsf);
        thePnt2.Transform (myTrsf);
        thePnt3.Transform (myTrsf);
      }

      // calculate normal
      theNormal = (thePnt2.XYZ() - thePnt1.XYZ()) ^ (thePnt3.XYZ() - thePnt1.XYZ());
      Standard_Real aNorm = theNormal.Magnitude();
      if (aNorm > gp::Resolution()) theNormal /= aNorm;
    }

  private:
    Handle(Poly_Triangulation) myPoly;
    gp_Trsf myTrsf;
    int myNbTriangles;
    bool myInvert;
  };

}

ReadWrite::ReadWrite() {
}
// ----------------------------------------------------------------------

// Error to console 
// ----------------------------------------------------------------------
void ReadWrite::error(std::string poop) { 
  std::cerr << poop << std::endl; 
} 

// Write BREP 
bool ReadWrite::WriteBREP(const TopoDS_Shape shape, std::string& output) {
  try { 
 	  std::stringstream stream;
    BRepTools::Write(shape,stream);		
		output = stream.str(); 
    return true; 
  } 
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}

// Read BREP 
bool ReadWrite::ReadBREP(std::string brep, TopoDS_Shape& rShape ) {
  try { 
		BRep_Builder brepb;
		std::stringstream stream;
		TopoDS_Shape shape;
		stream << brep << std::endl; 
    BRepTools::Read(rShape,stream,brepb);		
		return true; 
  } 
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}

// Write as STL
bool ReadWrite::WriteSTL(const TopoDS_Shape shape, std::string fname )
{
  try { 
    StlAPI_Writer stl_writer;
		BRepMesh_IncrementalMesh ( shape, 0.01 );	
    stl_writer.Write(shape, fname.c_str() );		
    return false; 
  } 
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}

// Iterate through a shape topology dumping out all triangles to an std::vector 
bool ReadWrite::DumpVectors(const TopoDS_Shape& theShape , std::vector<double>& vec ) {
  try { 
	  int count = 0;
    for (TopExp_Explorer exp (theShape, TopAbs_FACE); exp.More(); exp.Next())
    {
     TriangleAccessor aTool (TopoDS::Face (exp.Current()));
     vec.reserve( aTool.NbTriangles() * 9 );  
     for (int iTri = 1; iTri <= aTool.NbTriangles(); iTri++)
     {
       gp_Vec aNorm;
       gp_Pnt aPnt1, aPnt2, aPnt3;
       aTool.GetTriangle (iTri, aNorm, aPnt1, aPnt2, aPnt3);
		   // reversed for three.js 
		   vec.push_back(aPnt1.Z());  
       vec.push_back(aPnt1.Y());  
       vec.push_back(aPnt1.X());  
       vec.push_back(aPnt2.Z());  
       vec.push_back(aPnt2.Y());  
       vec.push_back(aPnt2.X());  
       vec.push_back(aPnt3.Z());   
       vec.push_back(aPnt3.Y());   
       vec.push_back(aPnt3.X()); 
      }
    }
    return true;  
  } 
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}


// Iterate through a shape topology dumping out all triangles to a string 
// const TopoDS_Shape& by reference ? 
bool ReadWrite::Dump(const TopoDS_Shape theShape, std::string& output)
{
  try { 
	  int fc = 0; 
	  std::stringstream stream;
	  stream << "[";
    for (TopExp_Explorer exp (theShape, TopAbs_FACE); exp.More(); exp.Next())
    {
     TriangleAccessor aTool (TopoDS::Face (exp.Current()));
     for (int iTri = 1; iTri <= aTool.NbTriangles(); iTri++)
     {
       gp_Vec aNorm;
       gp_Pnt aPnt1, aPnt2, aPnt3;
       aTool.GetTriangle (iTri, aNorm, aPnt1, aPnt2, aPnt3);
		   // reversed for three.js 
		   if ( fc == 0 ) { 
	       stream << aPnt1.Z() << "," << aPnt1.Y() << "," << aPnt1.X();
		   }
		   else { 
			  stream << "," << aPnt1.Z() << "," << aPnt1.Y() << "," << aPnt1.X();
		   }
       stream << "," << aPnt2.Z() << "," << aPnt2.Y() << "," << aPnt2.X();
       stream << "," << aPnt3.Z() << "," << aPnt3.Y() << "," << aPnt3.X();
		   fc = 1; 
     }
    }
    stream << "]\n"; 
    output = stream.str(); 
    return true; 
  }
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}

// Write a brep out to a char buffer 
bool ReadWrite::ConvertBrepTostring(TopoDS_Shape brep,std::string &output,double quality) { 
  try { 
	  TopoDS_Shape shape = brep; 
	  StlAPI_Writer stl_writer;
	  // Tolerances 
	  Standard_Real tolerance = quality;
    Standard_Real angular_tolerance = 0.5;
    Standard_Real minTriangleSize = Precision::Confusion();
	  // Set the mesh tolerances 7.x and onwards .. forcing us to do our own build and distribution of the library 
	  BRepMesh_FastDiscret::Parameters m_MeshParams;
    m_MeshParams.ControlSurfaceDeflection = Standard_True; 
    m_MeshParams.Deflection = tolerance;
    m_MeshParams.MinSize = minTriangleSize;
    m_MeshParams.InternalVerticesMode = Standard_False;
    m_MeshParams.Relative=Standard_False;
    m_MeshParams.Angle = angular_tolerance;
	  BRepMesh_IncrementalMesh ( shape, m_MeshParams );		
	  if ( Dump(shape,output) == false ) { error("Failed To Dump Vectors"); } 			
    return true; 
  } 
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}

// Write a brep out to an std::vector  
bool ReadWrite::ConvertBrepToVectors(TopoDS_Shape brep,std::vector<double>&vec, double quality) { 
  try { 
	  TopoDS_Shape shape = brep; 
	  StlAPI_Writer stl_writer;
	  // Tolerances 
	  Standard_Real tolerance = quality;
    Standard_Real angular_tolerance = 0.5;
    Standard_Real minTriangleSize = Precision::Confusion();
	  // Set the mesh tolerances 7.x and onwards .. forcing us to do our own build and distribution of the library 
	  BRepMesh_FastDiscret::Parameters m_MeshParams;
    m_MeshParams.ControlSurfaceDeflection = Standard_True; 
    m_MeshParams.Deflection = tolerance;
    m_MeshParams.MinSize = minTriangleSize;
    m_MeshParams.InternalVerticesMode = Standard_False;
    m_MeshParams.Relative=Standard_False;
    m_MeshParams.Angle = angular_tolerance;
	  BRepMesh_IncrementalMesh ( shape, m_MeshParams );
    // dump out vector list
	  if ( DumpVectors(shape,vec) == false ) { error("Failed To Dump Vectors"); return false; } 
    return true; 
  }  	
  catch (const std::exception &e)
  {
    error(e.what());
    return false; 
  }
  return false; 
}

bool ReadWrite::RecordExistsDb( std::string node , std::string hash ) { 
  try
  {
    std::stringstream stream;
  	BRep_Builder brepb;
    pqxx::connection c{"host=" + node + " dbname=makertron user=makertron password=makertron"};
    pqxx::work txn{c};
    pqxx::result r = txn.exec(
      "SELECT hash "
      "FROM cache "
      "WHERE hash =" + txn.quote(hash));
    if ( r.size() == 0 ) { 		
      return false; 
    } 
    else { 
      return true; 
    } 
  }
  catch (const pqxx::sql_error &e)
  {
    std::cerr << "SQL error: " << e.what() << std::endl;
    std::cerr << "Query was: " << e.query() << std::endl;
    return false; 
  }
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
} 

// Write Brep out to postgres 
bool ReadWrite::WriteBREPDb(const TopoDS_Shape& shape, std::string node , std::string hash) 
{ 
  std::stringstream stream;
  try {  
    BRepTools::Write(shape,stream);	 
    pqxx::connection c{"host=" + node + " dbname=makertron user=makertron password=makertron"};
    pqxx::work txn{c};
    txn.exec(
      "INSERT INTO cache(hash, stream) "
      "VALUES (" +
      txn.quote(hash) + ", " +
      txn.quote(stream) +
      ")");
    txn.commit();
    return true; 
  } 
  catch (const pqxx::sql_error &e)
  {
    std::cerr << "SQL error: " << e.what() << std::endl;
    std::cerr << "Query was: " << e.query() << std::endl;
    return false; 
  }
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}

// Write Brep out to postgres 
bool ReadWrite::DeleteBREPDb(std::string node, std::string hash) 
{ 
  try { 
    pqxx::connection c{"host=" + node + " dbname=makertron user=makertron password=makertron"};
    pqxx::work txn{c};
    pqxx::result r = txn.exec(
      "DELETE "
      "FROM cache "
      "WHERE cache.hash = " + txn.quote(hash));
    txn.commit();
    return true; 
  } 
  catch (const pqxx::sql_error &e)
  {
    std::cerr << "SQL error: " << e.what() << std::endl;
    std::cerr << "Query was: " << e.query() << std::endl;
    return false; 
  }
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}


// Write Brep out to postgres 
bool ReadWrite::UpdateBREPDb(const TopoDS_Shape& shape, std::string node , std::string hash) 
{ 
  std::stringstream stream;
  try {  
    BRepTools::Write(shape,stream);	 
    pqxx::connection c{"host=" + node + " dbname=makertron user=makertron password=makertron"};
    pqxx::work txn{c};
    pqxx::result r = txn.exec(
      "UPDATE cache SET stream = " + txn.quote(stream) + "WHERE hash = " + txn.quote(hash) );
    txn.commit();
    return true; 
  } 
  catch (const pqxx::sql_error &e)
  {
    std::cerr << "SQL error: " << e.what() << std::endl;
    std::cerr << "Query was: " << e.query() << std::endl;
    return false; 
  }
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}

// Read BREP out of postgres
bool ReadWrite::ReadBREPDb(TopoDS_Shape& rShape ,  std::string node ,  std::string hash) {
  try
  {
    std::stringstream stream;
  	BRep_Builder brepb;
    pqxx::connection c{"host=" + node + " dbname=makertron user=makertron password=makertron"};
    pqxx::work txn{c};
    pqxx::result r = txn.exec(
      "SELECT stream "
      "FROM cache "
      "WHERE hash =" + txn.quote(hash));
    // iterate over result 
    for (auto row: r)
    {
      for (auto field: row) stream << field.c_str();
    }
    BRepTools::Read(rShape,stream,brepb);		
    return true; 
  }
  catch (const pqxx::sql_error &e)
  {
    std::cerr << "SQL error: " << e.what() << std::endl;
    std::cerr << "Query was: " << e.query() << std::endl;
    return false; 
  }
  catch (const std::exception &e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    return false; 
  }
  return false; 
}
