#include  <erl_interface.h>
#include <ei.h>
 
class Nodex { 
	public:
		Standard_EXPORT Nodex(); 

    typedef struct _state_t {int fd; } state_t;
    
    Standard_EXPORT bool handle_emsg(state_t *state, ErlMessage *emsg); 

    Standard_EXPORT bool connect_node( char *sname , char *hostname , char *cookie, char *tname );

    Standard_EXPORT bool handle_message(state_t *state, ErlMessage* emsg); 

    Standard_EXPORT ETERM* err_term(const char *error_atom);

    Standard_EXPORT bool list_to_ints( std::vector<int> &arr , ETERM *elist );
    Standard_EXPORT bool list_to_deep_ints( std::vector<std::vector<int>> &deep_vec , ETERM *children );

    Standard_EXPORT bool list_to_doubles( std::vector<double> &arr , ETERM *elist );
    Standard_EXPORT bool doubles_to_list( std::vector<std::vector<double>> vecs , ETERM *nlist);

    Standard_EXPORT bool list_to_strings( std::vector<std::vector<std::vector<std::string>>> &vec_children , ETERM *elist ); 

    Standard_EXPORT void test_generate();

	protected:
	private:
}; 
