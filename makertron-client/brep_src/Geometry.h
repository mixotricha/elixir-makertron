/***************************************************************************
 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2019 *
 *                                                                         *
 *   This file is part of the Makertron CSG cad system.                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

class StlMesh_Mesh;
class TopoDS_Shape;

class Geometry { 
	public:
		Standard_EXPORT Geometry(); 
		
		//std::vector<TopoDS_Shape> shapeStack; 

    Standard_EXPORT void error(std::string poop); 

    Standard_EXPORT bool add( TopoDS_Shape &shapeA , std::string node , std::string hash ); 
    Standard_EXPORT bool update( TopoDS_Shape &shapeA , std::string node , std::string hash ); 
    Standard_EXPORT bool remove( std::string node , std::string hash );  
    Standard_EXPORT bool get( TopoDS_Shape & hapeA , std::string node , std::string hash );  
    Standard_EXPORT bool hasFaces(TopoDS_Shape &Shape); 

		Standard_EXPORT bool circle(double r1,TopoDS_Shape &aShape);

  	Standard_EXPORT bool polygon(          std::vector<std::vector<int>> faces,std::vector<double> points, TopoDS_Shape &aShape); 
    Standard_EXPORT bool polygonDifference(std::vector<std::vector<int>> faces,std::vector<double> points, TopoDS_Shape &aShape); 

		Standard_EXPORT bool polyhedron(std::vector<std::vector<int>> faces,std::vector<double> points,TopoDS_Shape &aShape); 

		Standard_EXPORT bool sphere(double radius, double x , double y , double z , TopoDS_Shape &aShape );
		Standard_EXPORT bool cube(double x , double y , double z , double xs , double ys , double zs , TopoDS_Shape &aShape);
		Standard_EXPORT bool cone(double r1,double r2,double h,double z, TopoDS_Shape &aShape);
		Standard_EXPORT bool cylinder(double r1,double h,double z , TopoDS_Shape &aShape );
		
		Standard_EXPORT bool translate(double x , double y , double z , TopoDS_Shape &aShape);
		Standard_EXPORT bool scale(double x , double y , double z , TopoDS_Shape &aShape );

		Standard_EXPORT bool rotate(double x ,double y , double z , TopoDS_Shape &aShape);

		Standard_EXPORT bool rotateX(double x , TopoDS_Shape &aShape);
		Standard_EXPORT bool rotateY(double y , TopoDS_Shape &aShape);
		Standard_EXPORT bool rotateZ(double z , TopoDS_Shape &aShape);
		
		Standard_EXPORT bool extrude(double h1, TopoDS_Shape &aShape);
		Standard_EXPORT bool minkowski(TopoDS_Shape &aShape,TopoDS_Shape bShape);

		Standard_EXPORT bool difference( TopoDS_Shape &aShape, TopoDS_Shape &bShape);
		Standard_EXPORT bool uni(TopoDS_Shape &aShape, TopoDS_Shape &bShape);
		Standard_EXPORT bool intersection(TopoDS_Shape &aShape,TopoDS_Shape &bShape);

    Standard_EXPORT bool difference_complex(TopoDS_Shape &aShape, TopoDS_Shape &bShape);
		Standard_EXPORT bool union_complex(TopoDS_Shape &aShape, TopoDS_Shape &bShape);
		Standard_EXPORT bool intersection_complex(TopoDS_Shape &aShape,TopoDS_Shape &bShape);

    Standard_EXPORT TopoDS_Shape getFaceFromShape(TopoDS_Shape shape1); 
    Standard_EXPORT bool IsIntersecting(TopoDS_Shape srf1,TopoDS_Shape srf2); 

	protected:
	private:
}; 
