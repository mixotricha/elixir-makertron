/***************************************************************************
 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2019 *
 *                                                                         *
 *   This file is part of the Makertron CSG cad system.                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

class TopoDS_Shape;

class ReadWrite { 

	public:

		Standard_EXPORT ReadWrite();

    Standard_EXPORT void error(std::string poop);

    Standard_EXPORT bool Dump(const TopoDS_Shape theShape, std::string& output);                // Export as triangle list        
    Standard_EXPORT bool DumpVectors(const TopoDS_Shape& theShape , std::vector<double>& vec ); // Dump vectors to vector list 

    Standard_EXPORT bool WriteBREP(const TopoDS_Shape shape, std::string& output); // Write a brep to a string
    Standard_EXPORT bool WriteBREPDb(const TopoDS_Shape& shape, std::string node , std::string hash); 

    Standard_EXPORT bool DeleteBREPDb(std::string node , std::string hash);

    Standard_EXPORT bool UpdateBREPDb(const TopoDS_Shape& shape, std::string node , std::string hash); // Update to postgres 		   

    Standard_EXPORT bool ReadBREP(std::string brep, TopoDS_Shape& rShape );   // Read a brep from string in to shape
    Standard_EXPORT bool ReadBREPDb(TopoDS_Shape& rShape , std::string node , std::string hash); // Read a brep from posgres in to shape 

    Standard_EXPORT bool RecordExistsDb( std::string node , std::string hash ); 
  	 
    Standard_EXPORT bool ConvertBrepTostring(TopoDS_Shape brep,std::string &output,double quality);       // Convert a brep to char string
    Standard_EXPORT bool ConvertBrepToVectors(TopoDS_Shape brep,std::vector<double>&vec, double quality); // Convert a brep to vector list

    Standard_EXPORT bool WriteSTL(const TopoDS_Shape shape, std::string fname ); // Write an stl to disk

	protected:

	private:

};
