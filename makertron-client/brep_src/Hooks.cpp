/***************************************************************************
 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2017 *
 *                                                                         *
 *   This file is part of the Makertron CSG cad system.                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

#include <gp.hxx>
#include <gp_Pln.hxx>
#include <gp_Ax2.hxx>
#include <gp_Circ.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Face.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_ListOfShape.hxx>
#include <Poly_Triangulation.hxx>

#include <Bnd_Box.hxx>
#include <BRepBndLib.hxx>
#include <OSD_Path.hxx>
#include <OSD_OpenFile.hxx>
#include <RWStl.hxx>

#include <StlAPI_Writer.hxx>

//#include <StlMesh_Mesh.hxx>
//#include <StlTransfer.hxx>

#include <BRep_Tool.hxx>
#include <BRepTools.hxx>

#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>

#include <BRepFeat_MakeCylindricalHole.hxx>
#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>

#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_GTransform.hxx>
#include <BRepBuilderAPI_MakeSolid.hxx>
#include <BRepBuilderAPI_Transform.hxx>

#include <BRepOffset_MakeOffset.hxx>
//#include <BRepOffsetAPI_MakeEvolved.hxx>
#include <BRepOffsetAPI_Sewing.hxx>
#include <GeomAbs_JoinType.hxx> 

// Streams 
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// Math
#include <math.h>
#include <float.h>
#include <cmath>
#include <assert.h>

// standard headers 
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

// Local Headers
#include <Hooks.h>
#include <ReadWrite.h>
#include <Geometry.h>
#include <PrintUtils.h>

#define TRANSLATE 0 
#define ROTATE 1 
#define SCALE 2 
#define COMPOUND 3 
#define EXPORT 4 

#define UNION 0 
#define DIFFERENCE 1 
#define INTERSECTION 2 

using namespace std;

PrintUtils printutils; 
Geometry geometry; 
ReadWrite readwrite; 

Hooks::Hooks() {
}

// --------------------------------------------------------------------
// Generate brep sphere 
// --------------------------------------------------------------------
bool Hooks::sphere(double radius, double x , double y , double z , std::string node , std::string hash ) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.sphere( radius , x , y , z , shape_a ); 
	  geometry.add( shape_a , node , hash ); 
	  return true; 
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ---------------------------------------------------------------------
// Generate brep cube
// ---------------------------------------------------------------------
bool Hooks::cube(double x , double y , double z , double xs , double ys , double zs , std::string node , std::string hash ) { 
  try { 
  	TopoDS_Shape shape_a; 
	  geometry.cube( x , y , z , xs , ys , zs , shape_a ); 
	  geometry.add( shape_a , node , hash ); 
	  return true;
  }
 	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ---------------------------------------------------------------------
// Generate brep cylinder 
// ---------------------------------------------------------------------
bool Hooks::cylinder(double r1,double h,double z, std::string node , std::string hash ) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.cylinder( r1 , h , z , shape_a ); 
	  geometry.add( shape_a , node , hash ); 
	  return true; 
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ---------------------------------------------------------------------
// Generate brep circle 
// ---------------------------------------------------------------------
bool Hooks::circle(double r1 , std::string node , std::string hash) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.circle( r1 ,shape_a ); 
	  geometry.add( shape_a , node , hash ); 
	  return true; 
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep cone 
// ----------------------------------------------------------------------
bool Hooks::cone(double r1,double r2,double h,double z , std::string node , std::string hash) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.cone( r1 , r2 , h , z , shape_a ); 
	  geometry.add( shape_a , node , hash ); 
	  return true;
  } 
 	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep polyhedron  
// ----------------------------------------------------------------------
bool Hooks::polyhedron(std::vector<std::vector<int>> faces,std::vector<double> points , std::string node , std::string hash ) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.polyhedron( faces , points , shape_a ); 
	  geometry.add( shape_a , node , hash ); 
	  return true; 
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep polygon  
// ----------------------------------------------------------------------
bool Hooks::polygon(std::vector<std::vector<int>> faces ,std::vector<double> points, std::string node , std::string hash) { 
  try { 
	  TopoDS_Shape shape_a; 

	  geometry.polygonDifference( faces , points , shape_a ); 

	  geometry.add( shape_a , node , hash  );
    return true;  
	} 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep difference 
// ----------------------------------------------------------------------
bool Hooks::difference_complex( std::string nodeA , std::string hashA , std::string nodeB , std::string hashB ) { 
  try { 
	  TopoDS_Shape shape_a; 
	  TopoDS_Shape shape_b;
	  geometry.get( shape_a , nodeA , hashA ); 
	  geometry.get( shape_b , nodeB , hashB ); 
	  geometry.difference_complex( shape_a , shape_b ); 
	  geometry.update( shape_a , nodeA , hashA ); 
    return true;
  }
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep union 
// ----------------------------------------------------------------------
bool Hooks::union_complex( std::string nodeA , std::string hashA , std::string nodeB , std::string hashB ) { 
  try { 
  	TopoDS_Shape shape_a; 
	  TopoDS_Shape shape_b;
	  geometry.get( shape_a , nodeA , hashA ); 
	  geometry.get( shape_b , nodeB , hashB ); 
	  geometry.union_complex( shape_a , shape_b ); 
	  geometry.update( shape_a , nodeA , hashA );
    return true;
  }  
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep intersection 
// ----------------------------------------------------------------------
bool Hooks::intersection_complex( std::string nodeA , std::string hashA , std::string nodeB , std::string hashB ) { 
  try { 
	  TopoDS_Shape shape_a; 
	  TopoDS_Shape shape_b;
	  geometry.get( shape_a , nodeA , hashA ); 
	  geometry.get( shape_b , nodeB , hashB ); 
	  geometry.intersection_complex( shape_a , shape_b ); 
	  geometry.update( shape_a , nodeA , hashA ); 
	  return true;
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep translate 
// ----------------------------------------------------------------------
bool Hooks::translate(double x , double y , double z , std::string node , std::string hash ) {
  try {  
	  TopoDS_Shape shape_a; 
	  geometry.get( shape_a , node , hash ); 
	  geometry.translate( x , y , z , shape_a  ); 
	  geometry.update( shape_a , node , hash ); 
	  return true; 
  }
	catch(const std::exception& e) { 
		printutils.PrintError(e.what()); 
    return false; 
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep scale
// ----------------------------------------------------------------------
bool Hooks::scale(double x , double y , double z , std::string node , std::string hash ) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.get( shape_a , node , hash ); 
	  geometry.scale( x , y , z , shape_a  ); 
	  geometry.update( shape_a , node , hash ); 
	  return true;
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}


// ----------------------------------------------------------------------
// Generate brep rotation x axis  
// ----------------------------------------------------------------------
bool Hooks::rotate(double x , double y , double z , std::string node , std::string hash ) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.get( shape_a , node , hash ); 
	  geometry.rotate( x , y , z , shape_a  ); 
	  geometry.update( shape_a , node , hash ); 
	  return true; 
  }
 	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}


// ----------------------------------------------------------------------
// Generate brep rotation x axis  
// ----------------------------------------------------------------------
bool Hooks::rotateX(double x , std::string node , std::string hash ) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.get( shape_a , node , hash ); 
	  geometry.rotateX( x , shape_a  ); 
	  geometry.update( shape_a , node , hash ); 
	  return true; 
  }
 	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep rotation y axis  
// ----------------------------------------------------------------------
bool Hooks::rotateY(double y , std::string node , std::string hash )  { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.get( shape_a , node , hash ); 
	  geometry.rotateY( y , shape_a  ); 
	  geometry.update( shape_a , node , hash ); 
	  return true;
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep rotation z axis  
// ----------------------------------------------------------------------
bool Hooks::rotateZ(double z , std::string node , std::string hash) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.get( shape_a , node , hash ); 
	  geometry.rotateZ( z , shape_a  ); 
	  geometry.update( shape_a , node , hash ); 
	  return true;
  } 
 	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate brep extrusion  
// ----------------------------------------------------------------------
bool Hooks::extrude(double h1, std::string node , std::string hash) { 
  try { 
	  TopoDS_Shape shape_a; 
	  geometry.get( shape_a , node , hash );  
	  geometry.extrude( h1 , shape_a  ); 
	  geometry.update( shape_a , node , hash ); 
	  return true; 
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate CGAL minkowiski  
// ----------------------------------------------------------------------
bool Hooks::minkowski(std::string nodeA , std::string hashA , std::string nodeB , std::string hashB  ) {
  try {  
	  TopoDS_Shape shape_a; 
	  TopoDS_Shape shape_b;
	  geometry.get( shape_a , nodeA , hashA ); 
	  geometry.get( shape_b , nodeB , hashB ); 
	  geometry.minkowski( shape_a , shape_b ); 
	  geometry.update( shape_a , nodeA , hashA ); 
	  return true;
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ---------------------------------------------------------------------
// Remove a node
// ---------------------------------------------------------------------
bool Hooks::remove(std::string node , std::string hash) { 
  try { 
	  geometry.remove( node , hash ); 
	  return true; 
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}

// ----------------------------------------------------------------------
// Copy one brep from db to a new name in db
// ----------------------------------------------------------------------
bool Hooks::copy_brep(std::string nodeA,std::string hashA,std::string nodeB,std::string hashB) {  
  try {  
    TopoDS_Shape shape; 
	  geometry.get( shape , nodeA , hashA );   // grab the object
    geometry.add( shape , nodeB , hashB );     // write out to new object   
  }
  catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
  return false; 
}

// ----------------------------------------------------------------------
// Write an stl to console 
// ----------------------------------------------------------------------
bool Hooks::write_stl(std::string node,std::string hash,std::string fname) {  
  TopoDS_Shape result;
  readwrite.ReadBREPDb( result , node , hash ); 
  std::cout << readwrite.WriteSTL( result , fname ) << std::endl; 
  return true; 
}

// -------------------------------------------------------------------------
// transform/rotate/scale applied to a set of breps     
// -------------------------------------------------------------------------
bool Hooks::shift_tuple(std::string 
  nodeA, 
  std::string hashA, 
  std::vector< std::vector<std::vector<std::string>>>& children, 
  double x, double y, double z, 
  int type ) 
{
  int i = 0 , ii = 0; 
  bool state = false;
  TopoDS_Compound compound;
  BRep_Builder profileBuilder;
  profileBuilder.MakeCompound(compound);
  for ( i = 0; i < children.size(); i++ ) {   
    for ( ii = 0; ii < children[i].size(); ii++ ) { 
      TopoDS_Shape shape; 
      geometry.get( shape , children[i][ii][0] , children[i][ii][1] );   // grab the object 
      if ( i == 0 && ii == 0 ) { 
        profileBuilder.Add(compound, shape);           
      }
      else if ( geometry.IsIntersecting(compound,shape) == false ) { 
        profileBuilder.Add(compound, shape);           
      }
      else {
        geometry.union_complex( compound , shape );  
      } 
    }
  }
  if ( type == TRANSLATE ) { 
    if ( geometry.translate( x , y , z , compound  ) == true ) { state = true; }  
  }
  if ( type == ROTATE ) { 
    if ( geometry.rotate( x , y  , z , compound ) == true ) { state = true; } 
  }
  if ( type == SCALE ) { 
    if ( geometry.scale( x , y , z , compound ) == true ) { state = true; } 
  }
  if ( type == COMPOUND ) { 
    // We pass right through
    state = true; 
  } 
  //if ( type == EXPORT ) { 
  // Export to a physical file should be per object not a compound
  //if ( write_stl(node,hash,hash) == true ) { state = true; }  
  //} 
  geometry.add( compound , nodeA , hashA );
  return state;
} 

// ------------------------------------------------------------------------------------------
// Process a complex multi dimensional tuple  
// through accumulated boolean operation  
// TODO : Think a lot of reading and writing can be avoided if instead fetched all breps in one hit. 
//  Then operated on them as actual shapes in an array. Then exported only the final result out to db.
// Also deciding if objects intersect and doing a union or a faster compound instead  
// ------------------------------------------------------------------------------------------
bool Hooks::boolean_tuple( std::string node , std::string hash , std::vector<std::vector<std::vector<std::string>>>& children , int type  ) {

  int i = 0 , ii = 0; 
  bool state = false;
  
  TopoDS_Shape first;   
  TopoDS_Compound result;
  
  // Try to be more intelligent by choosing a compound over a union if possible ... 
  for ( i = 0; i < children.size(); i++ ) {

    TopoDS_Compound compound;
    BRep_Builder profileBuilder;    
    profileBuilder.MakeCompound(compound);  
    geometry.get( first , children[i][0][0] , children[i][0][1] );  
    profileBuilder.Add(compound, first);  

    for ( ii = 1; ii < children[i].size(); ii++ ) {
      TopoDS_Shape shape;   
      geometry.get( shape , children[i][ii][0] , children[i][ii][1] );  
      if ( geometry.IsIntersecting(compound,shape) == true ) { 
        geometry.union_complex( compound , shape );  
      }
      else { 
        profileBuilder.Add(compound, shape);  
      } 
    }

    if ( i != 0 ) { 
      if ( type == UNION ) { 
        geometry.union_complex( result , compound ); 
        state = true; 
      } 
      if ( type == DIFFERENCE ) { 
        geometry.difference_complex( result , compound ); 
        state = true; 
      } 
      if ( type == INTERSECTION ) { 
        geometry.intersection_complex( result , compound ); 
        state = true; 
      } 
    }
    else { 
      result = compound;  
    } 

  } 
  
  //for ( i = 1; i < shape_list.size() ; i++ ) { 
//    if ( type == UNION ) { 
//      geometry.union_complex( shape_list[0] , shape_list[i] ); 
//      state = true; 
//    } 
//    if ( type == DIFFERENCE ) { 
//      geometry.difference_complex( shape_list[0] , shape_list[i] ); 
//      state = true; 
//    } 
//    if ( type == INTERSECTION ) { 
//      geometry.intersection_complex( shape_list[0] , shape_list[i] ); 
//      state = true; 
//    } 
  //}

  geometry.add( result , node , hash );
  return state;
} 


// -------------------------------------------------------------------
// Does record already exist 
// -------------------------------------------------------------------
bool Hooks::exists( std::string node , std::string hash ) { 
  try { 
	  return readwrite.RecordExistsDb(node,hash);
  }
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
  return false; 
} 

// -------------------------------------------------------------------
// Convert brep shape to chars  
// -------------------------------------------------------------------
bool Hooks::convert_brep_tostring(double quality, std::string& result , std::string node , std::string hash ) {
  try { 
	  TopoDS_Shape brep; 
	  geometry.get( brep , node , hash );  
	  readwrite.ConvertBrepTostring(brep,result,quality);
    return true; 
  }
	catch(const std::exception& e) { 
		printutils.PrintError(e.what());
    return false;  
	}
	return false; 
} 

// -------------------------------------------------------------------
// Convert brep shape to vectors 
// -------------------------------------------------------------------
bool Hooks::convert_brep_tovectors(double quality, std::vector<double> vecs, std::string node , std::string hash ) {
  try { 
	  TopoDS_Shape brep; 
	  geometry.get( brep , node , hash );   
    readwrite.ConvertBrepToVectors(brep,vecs,quality);
    return true; 
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what()); 
    return false;
	}
	return false; 
} 

// -------------------------------------------------------------------
// Convert children to vector list 
// -------------------------------------------------------------------
bool Hooks::convert_children_tovectors(
  double quality, 
  std::vector<std::vector<std::vector<std::string>>>& children , 
  std::vector<std::vector<double>> &vecs ) {
  try { 
    int iii = 0; 
    for ( int i = 0; i < children.size(); i++ ) {
      vecs.resize( vecs.size() + children[i].size() ); 
      for ( int ii = 0; ii < children[i].size(); ii++ ) {
        TopoDS_Shape brep;   
        std::cout << "Pair: " << children[i][ii][0] << " " << children[i][ii][1] << std::endl;
        geometry.get( brep , children[i][ii][0] , children[i][ii][1] ); 
        readwrite.ConvertBrepToVectors(brep,vecs[iii],quality);
        iii++; 
      }
    } 
    return true; 
  } 
	catch(const std::exception& e) { 
		printutils.PrintError(e.what()); 
    return false;
	}
	return false; 
} 


bool Hooks::test_generate() { 

  //std::cout << "Writing Stuff" << std::endl; 
  // delete objects from db
  //readwrite.DeleteBREPDb("pine6402", "cube"  ); 
  //readwrite.DeleteBREPDb("pine6402", "sphere"); 
  //readwrite.DeleteBREPDb("pine6402", "cylinder");
  //readwrite.DeleteBREPDb("pine6402", "compound");
  // create prims 
  //TopoDS_Shape cub;
  //cube( -12.5, -2.5, -2.5, 5.0, 5.0, 5.0, "pine6402" , "cube" ); 
  //readwrite.ReadBREPDb( cub , "pine6402" , "cube" ); 
  //TopoDS_Shape sph; 
  //sphere(3.2, 0.0, 0.0,0.0,"pine6402","sphere");
  //readwrite.ReadBREPDb( sph , "pine6402" , "sphere" ); 
  //TopoDS_Shape cyl; 
  //cylinder( 2.0 , 20, -10, "pine6402", "cylinder");
  //rotate( 0.0 , -1.5707963268 , 0.0 , "pine6402" , "cylinder" ); 
  //readwrite.ReadBREPDb( cyl , "pine6402" , "cylinder" );  
  // Add prims to compound 
  //TopoDS_Compound compound;
  //BRep_Builder profileBuilder;
  //profileBuilder.MakeCompound(compound);
  //TopoDS_Shape shapeA; 
  //geometry.get( shapeA , "pine6402" , "cube" );  
  //profileBuilder.Add(compound, shapeA );     
  //TopoDS_Shape shapeB; 
  //geometry.get( shapeB , "pine6402" , "sphere" );  
  //profileBuilder.Add(compound, shapeB );     
  //geometry.difference_complex( compound , cyl ); 
  //TopoDS_Shape shapeC; 
  //geometry.get( shapeC , "pine6402" , "cylinder" );  
  //profileBuilder.Add(compound, shapeC );     
  // Create compound in db
  //geometry.add( compound , "pine6402" , "compound" ); 
  // export compound as stl 
  //std::cout << readwrite.WriteSTL( compound , "test.stl" ) << std::endl; 
  //difference_complex( "cube" , "sphere" ); 
  //cylinder(0.5, 20, -10,"a"); 
  //rotateX( 1.5707963267948966, 1 );  
  //rotateY( 0.0 , 1 );  
  //rotateZ( -2.0943951023931953 , 1 ); 
  //translate( 8.660254037844387, -4.999999999999998, 0 , 1 ); 
  //rotateX( 0, 2 );  
  //rotateY( 0, 2 ); 
  //rotateZ( -2.0943951023931953 , 2 ); 
  //translate( 12.99038105676658, -7.4999999999999964, 0 , 2 ) ; 
  //difference_complex( 2 , 1 ); 
  //difference_complex( 2 , 0 ); 
  //TopoDS_Shape result;
  //geometry.get( 2 , result ); 
  //std::cout <<readwrite.WriteBREP( result ) << std::endl;*/
  //erl_send(state->fd, emsg->from, erl_format((char *)"{ping}"));              

  return true; 
} 
