#include <gp.hxx>
#include <gp_Pln.hxx>
#include <gp_Ax2.hxx>
#include <gp_Circ.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Face.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_ListOfShape.hxx>
#include <Poly_Triangulation.hxx>

#include <Bnd_Box.hxx>
#include <BRepBndLib.hxx>
#include <OSD_Path.hxx>
#include <OSD_OpenFile.hxx>
#include <RWStl.hxx>

#include <StlAPI_Writer.hxx>

//#include <StlMesh_Mesh.hxx>
//#include <StlTransfer.hxx>

#include <BRep_Tool.hxx>
#include <BRepTools.hxx>

#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>

#include <BRepFeat_MakeCylindricalHole.hxx>
#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>

#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_GTransform.hxx>
#include <BRepBuilderAPI_MakeSolid.hxx>
#include <BRepBuilderAPI_Transform.hxx>

#include <BRepOffset_MakeOffset.hxx>

//#include <BRepOffsetAPI_MakeEvolved.hxx>

#include <BRepOffsetAPI_Sewing.hxx>

#include <GeomAbs_JoinType.hxx> 

// Streams 
#include <fstream>
#include <iostream>
#include <string>

// Math
#include <math.h>
#include <float.h>
#include <cmath>
#include <assert.h>
#include <vector>

// standard headers 
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h> 

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

// Erlang headers 
#include  <erl_interface.h>
#include <ei.h>

// Local includes 
#include <Hooks.h> 
#include <Nodex.h>
#include <chrono> 

#define BUFFER_SIZE 50000

#define ROOT 2 
#define FIRST_ELEMENT  1
#define SECOND_ELEMENT 2
#define THIRD_ELEMENT  3
#define FOURTH_ELEMENT 4 

#define TRANSLATE 0 
#define ROTATE 1 
#define SCALE 2 
#define COMPOUND 3 
#define EXPORT 4 

#define UNION 0 
#define DIFFERENCE 1 
#define INTERSECTION 2 

using namespace std;
using namespace std::chrono;

Hooks hooks; 

Nodex::Nodex() {
}

typedef struct _state_t {int fd; } state_t;

// Handle elixir message
bool Nodex::handle_emsg(state_t* state, ErlMessage* emsg) {
  switch(emsg->type)
  {
    case ERL_REG_SEND:
    case ERL_SEND:
      handle_message(state, emsg);
      break;
    case ERL_LINK:
      break;
    case ERL_UNLINK:
      break;
    case ERL_EXIT:
      break;
  }
  // its our responsibility to free these pointers
  erl_free_compound(emsg->msg);
  erl_free_compound(emsg->to);
  erl_free_compound(emsg->from);
}

// Bring up a cnode for elixir communication process 
bool Nodex::connect_node( char *sname , char *hostname , char *cookie, char *tname ) { 
 
  const int full_name_len = strlen(sname) + 1 + strlen(hostname) + 1;
  char full_name[full_name_len];
  stpcpy(stpcpy(stpcpy(full_name, sname), "@"), hostname);
  const int target_node_len = strlen(tname) + 1 + strlen(hostname) + 1;
  char target_node[target_node_len];
  stpcpy(stpcpy(stpcpy(target_node, tname), "@"), hostname);
  struct in_addr addr;
  addr.s_addr = htonl(INADDR_ANY);
  // fd to erlang node
  state_t* state = (state_t*)malloc(sizeof(state_t));
  bool looping = true;
  int buffer_size = BUFFER_SIZE;
  unsigned char* bufferpp = (unsigned char*)malloc(BUFFER_SIZE);
  ErlMessage emsg;
  // initialize all of Erl_Interface
  erl_init(NULL, 0);
  // initialize this node
  printf("initialising %s\n", full_name); fflush(stdout);
  if (erl_connect_xinit(hostname, sname, full_name, &addr, cookie, 0) == -1)
    erl_err_quit("error erl_connect_init");
  // connect to target node
  printf("connecting to %s\n", target_node); fflush(stdout);
  if ((state->fd = erl_connect(target_node)) < 0)
    erl_err_quit("error erl_connect");
  // signal on stdout to cnode helper that we are ready
  printf("%s ready\n", full_name); fflush(stdout);
  while (looping)
  {
    // erl_xreceive_msg adapts the buffer width
    switch(erl_xreceive_msg(state->fd, &bufferpp, &buffer_size, &emsg))
    {
      case ERL_TICK:
        //std::cout << "TICK" << std::endl; 
        break;
      case ERL_TIMEOUT: 
        //std::cout << "TIMEOUT" << std::endl; 
        looping = false; 
        break;
      case ERL_ERROR:
        // On failure, the function returns ERL_ERROR and sets erl_errno to one of:
        //
        // EMSGSIZE
        // Buffer is too small.
        // ENOMEM
        // No more memory is available.
        // EIO
        // I/O error.
        //
        // TODO: report on erl_errno
        std::cout << "Oh lord an error!" << std::endl; 
        looping = false;
        break;
      default:
        handle_emsg(state, &emsg);
    }
  }
}


// Handle messages from exlir cnode

bool Nodex::handle_message(state_t* state, ErlMessage* emsg) {

  ETERM *operation = erl_element(1,emsg->msg);
  ETERM *response;
  if ( erl_match(erl_format((char *)"ping"),operation) ) {
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::cout << erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)) << std::endl;
    std::cout << ERL_INT_VALUE(erl_element(1,erl_element(SECOND_ELEMENT,tuple))) << std::endl; 
    response = erl_format((char *)"{pong, true}");
    erl_free_term(tuple);
  }
  else if ( erl_match(erl_format((char *)"delete"),operation) ) { 
    bool state = false; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));
    if ( hooks.remove( node , hash ) == true ) { state == true; }        
    if ( state == true ) {response = erl_format((char *)"{true,delete}");} else { response = erl_format((char *)"{false,delete}");}
    erl_free_term(tuple); 
  }
  else if ( erl_match(erl_format((char *)"sphere"),operation) ) { 
    bool state = false; 
    std::vector<double> para; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));
    if ( hooks.exists( node , hash ) == false ) { 
      ETERM *vecs_tuple = erl_element(THIRD_ELEMENT,tuple); 
      if ( list_to_doubles(para , vecs_tuple ) == true ) { state = true; }   
      if ( hooks.sphere( para[0] , para[1] , para[2] , para[3] , node , hash ) == true ) { state == true; }        
      erl_free(vecs_tuple);  
    } 
    else {
      state = true;  
    } 
    if ( state == true ) {response = erl_format((char *)"{true,sphere}");} else { response = erl_format((char *)"{false,sphere}");} 
    erl_free_term(tuple);
  }
  else if ( erl_match(erl_format((char *)"circle"),operation) ) { 
    bool state = false; 
    std::vector<double> para; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));
    if ( hooks.exists( node , hash ) == false ) { 
      ETERM *vecs_tuple = erl_element(THIRD_ELEMENT,tuple); 
      if ( list_to_doubles(para , vecs_tuple ) == true ) { state = true; }   
      if ( hooks.circle( para[0]  , node , hash) == true ) { state == true; }  
      erl_free(vecs_tuple);  
    } 
    else {
      state = true;  
    } 
    if ( state == true ) {response = erl_format((char *)"{true,circle}");} else { response = erl_format((char *)"{false,circle}");} 
    erl_free_term(tuple);
  }
  else if ( erl_match(erl_format((char *)"cube"),operation) ) { 
    bool state = false; 
    std::vector<double> para; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));
    if ( hooks.exists( node , hash ) == false ) { 
      ETERM *vecs_tuple = erl_element(THIRD_ELEMENT,tuple); 
      if ( list_to_doubles(para , vecs_tuple ) == true ) { state = true; }   
      if ( hooks.cube( para[0] , para[1] , para[2] , para[3] , para[4] , para[5] , node , hash) == true ) { state == true; }        
      erl_free(vecs_tuple);
    } 
    else { 
      state = true; 
    } 
    if ( state == true ) { response = erl_format((char *)"{true,cube}"); } else { response = erl_format((char *)"{false,cube}"); } 
    erl_free_term(tuple); 
  }
  else if ( erl_match(erl_format((char *)"cylinder"),operation) ) { 
    bool state = false; 
    std::vector<double> para; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));
    if ( hooks.exists( node , hash ) == false ) { 
      ETERM *vecs_tuple = erl_element(THIRD_ELEMENT,tuple); 
      if ( list_to_doubles(para , vecs_tuple ) == true ) { state = true; }   
      if ( hooks.cylinder( para[0] , para[1] , para[2] ,  node , hash ) == true ) { state == true; }    
      erl_free(vecs_tuple);  
    } 
    else { 
      state = true; 
    }   
    if ( state == true ) {response = erl_format((char *)"{true,cylinder}");} else { response = erl_format((char *)"{false,cylinder}");} 
    erl_free_term(tuple); 
  }
  else if ( erl_match(erl_format((char *)"cone"),operation) ) { 
    bool state = false; 
    std::vector<double> para; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));
    if ( hooks.exists( node , hash ) == false ) { 
      ETERM *vecs_tuple = erl_element(THIRD_ELEMENT,tuple); 
      if ( list_to_doubles(para , vecs_tuple ) == true ) { state = true; }   
      if ( hooks.cone( para[0] , para[1] , para[2] , para[3] , node , hash ) == true ) { state == true; }    
      erl_free(vecs_tuple);  
    } 
    else { 
      state = true; 
    }   
    if ( state == true ) {response = erl_format((char *)"{true,cone}");} else { response = erl_format((char *)"{false,cone}");} 
    erl_free_term(tuple); 
  }
  else if ( erl_match(erl_format((char *)"polygon"),operation) ) { 
    bool state = false; 
    std::vector<std::vector<int>> faces;
    std::vector<double> points;
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));
    if ( hooks.exists( node , hash ) == false ) { 
      
      ETERM *faces_tuple =  erl_element(THIRD_ELEMENT,tuple); 
      ETERM *points_tuple = erl_element(FOURTH_ELEMENT,tuple); 
      std::cout << "tuple size: " << erl_size(tuple) << std::endl; 
      std::cout << "faces size: " << erl_size(faces_tuple) << std::endl; 
      std::cout << "points size: " << erl_size(points_tuple) << std::endl; 
      //polygon(points=[[0,0],[100,0],[130,50],[30,50]]);
      if ( list_to_deep_ints(faces  , faces_tuple  ) == true ) { state = true; } 
      if ( list_to_doubles(points   , points_tuple ) == true ) { state = true; } 
      if ( hooks.polygon( faces , points , node , hash) == true ) { state == true; } 
      erl_free(faces_tuple);
      erl_free(points_tuple);
    } 
    else { 
      state = true; 
    } 
    if ( state == true ) { response = erl_format((char *)"{true,polygon}"); } else { response = erl_format((char *)"{false,polygon}"); } 
    erl_free_term(tuple); 
  }
  else  if ( erl_match(erl_format((char *)"translate"),operation) ) {
    bool state = false; 
    std::vector<double> para; 
    std::vector< std::vector<std::vector<std::string>>> children;
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));   
    if ( hooks.exists( node , hash ) == false ) {  
      ETERM *vecs_tuple = erl_element(THIRD_ELEMENT,tuple); 
      if ( list_to_doubles(para , vecs_tuple ) == true ) { state = true; }  
      if ( list_to_strings( children  , erl_element(FOURTH_ELEMENT,tuple) ) == true ) { state = true; }  
      if ( hooks.shift_tuple( node , hash , children , para[0] , para[1] , para[2] , TRANSLATE ) == true ) { state = true; }  
      erl_free(vecs_tuple);
    } 
    else { 
      state = true; 
    } 
    if ( state == true ) { response = erl_format((char *)"{true,translate}"); } else { response = erl_format((char *)"{false,translate}"); } 
    erl_free_term(tuple); 
  }
  else  if ( erl_match(erl_format((char *)"rotate"),operation) ) {
    bool state = false; 
    std::vector<double> para; 
    std::vector< std::vector<std::vector<std::string>>> children;
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));    
    if ( hooks.exists( node , hash ) == false ) { 
      ETERM *vecs_tuple = erl_element(THIRD_ELEMENT,tuple); 
      if ( list_to_doubles(para , vecs_tuple ) == true ) { state = true; }  
      if ( list_to_strings( children  , erl_element(FOURTH_ELEMENT,tuple) ) == true ) { state = true; }  
      if ( hooks.shift_tuple( node , hash , children , para[0] , para[1] , para[2] , ROTATE ) == true ) { state = true; }  
      erl_free(vecs_tuple); 
    } 
    else {
      state = true;  
    } 
    if ( state == true ) { response = erl_format((char *)"{true,rotate}"); } else { response = erl_format((char *)"{false,rotate}"); } 
    erl_free_term(tuple);
  }
  else  if ( erl_match(erl_format((char *)"scale"),operation) ) {
    bool state = false; 
    std::vector<double> para; 
    std::vector< std::vector<std::vector<std::string>>> children;
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple))); 
    if ( hooks.exists( node , hash ) == false ) {    
      ETERM *vecs_tuple = erl_element(THIRD_ELEMENT,tuple); 
      if ( list_to_doubles(para , vecs_tuple ) == true ) { state = true; }  
      if ( list_to_strings( children  , erl_element(FOURTH_ELEMENT,tuple) ) == true ) { state = true; }  
      if ( hooks.shift_tuple( node , hash , children , para[0] , para[1] , para[2] , SCALE ) == true ) { state = true; }  
      erl_free(vecs_tuple);   
    } 
    else {
      state = true;  
    } 
    if ( state == true ) { response = erl_format((char *)"{true,scale}"); } else { response = erl_format((char *)"{false,scale}"); }   
    erl_free_term(tuple);
  }
  else  if ( erl_match(erl_format((char *)"union"),operation) ) {
    bool state = false; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::vector< std::vector<std::vector<std::string>>> children;
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));    
    if ( hooks.exists( node , hash ) == false ) { 
      if ( list_to_strings( children  , erl_element(THIRD_ELEMENT,tuple) ) == true ) { state = true; }  
      if ( hooks.boolean_tuple( node , hash , children , UNION ) == true ) { state = true; }  
    } 
    else { 
      state = true; 
    } 
    if ( state == true ) { response = erl_format((char *)"{true,union}"); } else { response = erl_format((char *)"{false,union}"); }
    erl_free_term(tuple); 
  }
  else  if ( erl_match(erl_format((char *)"difference"),operation) ) {
    bool state = false; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::vector< std::vector<std::vector<std::string>>> children;
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));    
    if ( hooks.exists( node , hash ) == false ) {  
      if ( list_to_strings( children  , erl_element(THIRD_ELEMENT,tuple) ) == true ) { state = true; }  
      if ( hooks.boolean_tuple( node , hash , children , DIFFERENCE ) == true ) { state = true; }  
    }
    else { 
      state = true; 
    } 
    if ( state == true ) { response = erl_format((char *)"{true,difference}"); } else { response = erl_format((char *)"{false,difference}"); }
    erl_free_term(tuple); 
  }
  else  if ( erl_match(erl_format((char *)"intersection"),operation) ) {
    bool state = false; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::vector< std::vector<std::vector<std::string>>> children;
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));    
    if ( hooks.exists( node , hash ) == false ) { 
      if ( list_to_strings( children  , erl_element(THIRD_ELEMENT,tuple) ) == true ) { state = true; }  
      if ( hooks.boolean_tuple( node , hash , children , INTERSECTION ) == true ) { state = true; } 
    } 
    else { 
      state = true; 
    } 
    if ( state == true ) { response = erl_format((char *)"{true,intersection}"); } else { response = erl_format((char *)"{false,intersection}"); }
    erl_free_term(tuple); 
  }
  else  if ( erl_match(erl_format((char *)"compound"),operation) ) {
    bool state = false; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::vector< std::vector<std::vector<std::string>>> children;
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));    
    if ( hooks.exists( node , hash ) == false ) { 
      if ( list_to_strings( children  , erl_element(THIRD_ELEMENT,tuple) ) == true ) { state = true; }  
      if ( hooks.shift_tuple( node , hash , children , 0 , 0 , 0 ,  COMPOUND ) == true ) { state = true; }  
    } 
    else { 
      state = true; 
    } 
    if ( state == true ) { response = erl_format((char *)"{true,compound}"); } else { response = erl_format((char *)"{false,compound}"); }
    erl_free_term(tuple); 
  }
  else if ( erl_match(erl_format((char *)"export"),operation) ) { 
    bool state = false; 
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::vector< std::vector<std::vector<std::string>>> children;
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));    
    if ( list_to_strings( children  , erl_element(THIRD_ELEMENT,tuple) ) == true ) { state = true; }  
    if ( hooks.shift_tuple( node , hash , children , 0 , 0 , 0 ,  EXPORT ) == true ) { state = true; }  
    if ( state == true ) { response = erl_format((char *)"{true,export}"); } else { response = erl_format((char *)"{false,export}"); }
    erl_free_term(tuple); 
  }
  else if ( erl_match(erl_format((char *)"fetch_geometry"),operation) ) { 
    bool state = false; 
    ETERM* result = erl_mk_empty_list(); 
    std::vector<std::vector<double>> vecs;
    std::vector<std::vector<std::vector<std::string>>> children;
    ETERM* tuple = erl_element(ROOT,emsg->msg);
    std::string node(erl_iolist_to_string(erl_element(FIRST_ELEMENT,tuple)));
    std::string hash(erl_iolist_to_string(erl_element(SECOND_ELEMENT,tuple)));    
    if ( list_to_strings( children  , erl_element(THIRD_ELEMENT,tuple) ) == true ) { state = true; } 
    if ( hooks.convert_children_tovectors(0.9,children,vecs) == true  ) { 
      for (int i = 0; i < vecs.size(); i++) {
       ETERM* elist = erl_mk_empty_list();     
       for ( int ii = 0; ii < vecs[i].size(); ii++ ) {   
        elist = erl_cons( erl_mk_float(vecs[i][ii]) ,elist);
       }
       result = erl_cons( elist , result );  
      }
      state = true;
    } 
    if ( state == true ) { response = erl_format((char *)"{true,~w}",result); } else { response = erl_format((char *)"{false,geometry}"); }
    erl_free_term(tuple);
    erl_free_term(result); 
  }
  else {
    response = erl_format((char *)"{false,unknown_call}");
  }

  // send response
  erl_send(state->fd, emsg->from, response);
  // free allocated resources
  erl_free_compound(response); 
  erl_free_term(operation);
  // free the free-list
  erl_eterm_release();


  return false;

} 


// ---------------------------------------------------------
// Convert simple one dimension tuple to ints 
// ---------------------------------------------------------
bool Nodex::list_to_ints( std::vector<int>& arr , ETERM *elist ) {
  try { 
    unsigned i; 
    arr.resize(erl_size(elist)); 
    for ( i = 0; i < arr.size(); i++ ) { 
     arr[i] = ERL_INT_VALUE(erl_element(i+1, elist)); 
    }
    return true; 
  } 
  catch(const std::exception& e) { 
	  //printutils.PrintError(e.what());
    return false;  
	}
	return false; 
} 

// ---------------------------------------------------------
// Convert simple one dimension tuple to floats 
// ---------------------------------------------------------
bool Nodex::list_to_doubles( std::vector<double>& arr , ETERM *elist ) {
  try { 
    unsigned i; 
    arr.resize(erl_size(elist)); 
    for ( i = 0; i < arr.size(); i++ ) { 
     arr[i] = ERL_FLOAT_VALUE(erl_element(i+1, elist)); 
    }
    return true; 
  } 
  catch(const std::exception& e) { 
	  //printutils.PrintError(e.what());
    return false;  
	}
	return false; 
} 

// ---------------------------------------------------
// Convert multi dimensional array of vectors to an elixir list 
// ---------------------------------------------------
bool Nodex::doubles_to_list( std::vector<std::vector<double>> vecs , ETERM *nlist) {
  try { 
    return true; 
  }
  catch(const std::exception& e) { 
	  //printutils.PrintError(e.what());
    return false;  
	}
  return false; 
}

// --------------------------------------------------------------------
// Convert complex tuple of strings [ ["a","b","c","d"] , ["c","d"] ] to vector equivalent. 
// We do this because we want indexed vectors to work with later.
// This is a complicated little bit of unwinding and copying but it makes 
// life more simple later as it frees us from the ETERM.   
// --------------------------------------------------------------------  
bool Nodex::list_to_strings( std::vector< std::vector<std::vector<std::string>>>& vec_children , ETERM *children ) {
  try { 
    unsigned i=0,ii=0,iii=0; 
    unsigned children_size = erl_size(children); 
    vec_children.resize(children_size); // Damien is confused as to why this is not children_size -1    
    unsigned vec_children_size = vec_children.size(); 
    for ( i = 0; i < vec_children_size; i++ ) { 
     iii = 0; 
     ETERM *pairs = erl_element(i+1,children); // pull row of pairs from children tuple 
     unsigned pairs_size = erl_size(pairs);   
     vec_children[i].resize(pairs_size-1); // Resize vec_children row to take pairs which _IS_ SIZE-1  
     for ( ii = 0; ii < pairs_size; ii+=2 ) {  // iterate through each pair     
       std::vector<std::string> pair(2); // pair vector picks up the pair 
       pair[0] = erl_iolist_to_string(erl_element(ii+1,pairs));  
       pair[1] = erl_iolist_to_string(erl_element(ii+2,pairs));  
       vec_children[i][iii] = pair; iii++;    
     }
    }
    return true; 
  } 
  catch(const std::exception& e) { 
	  //printutils.PrintError(e.what());
    return false;  
	}
	return false; 
} 

// ----------------------------------------------------------------------------------------------  
// Convert complex tupple of ints 
// -----------------------------------------------------------------------------------------------
bool Nodex::list_to_deep_ints( std::vector<std::vector<int>> &deep_vec , ETERM *children ) {
  try { 
    int i = 0;    
    deep_vec.resize(erl_size(children));      
    for ( i = 0; i < deep_vec.size(); i++ ) {
      ETERM *face = erl_element(i+1,children);
      deep_vec[i].resize(erl_size(face));   
      list_to_ints(deep_vec[i] , face); 
      erl_free_term(face);
    }
    return true; 
  } 
  catch(const std::exception& e) { 
	  //printutils.PrintError(e.what());
    return false;  
	}
	return false; 
}


// -----------------------------------------------------
// handle an error 
// -----------------------------------------------------
ETERM* Nodex::err_term(const char* error_atom) {
  return erl_format((char *)"{error, ~w}", erl_mk_atom(error_atom));
}


void Nodex::test_generate() {
  hooks.test_generate(); 
} 











