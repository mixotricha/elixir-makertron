/***************************************************************************
 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2019 *
 *                                                                         *
 *   This file is part of the Makertron CSG cad system.                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

class StlMesh_Mesh;
class TopoDS_Shape;
class Geometry; 
class ReadWrite; 

class Hooks { 
	public:
		Standard_EXPORT Hooks(); 

    Standard_EXPORT bool sphere(double radius, double x , double y , double z , std::string node , std::string hash ); ;
    Standard_EXPORT bool cube(double x , double y , double z , double xs , double ys , double zs , std::string node , std::string hash ); ;
    Standard_EXPORT bool cylinder(double r1,double h,double z, std::string node , std::string hash ); ;
    Standard_EXPORT bool circle(double r1 , std::string node , std::string hash);
    Standard_EXPORT bool cone(double r1,double r2,double h,double z , std::string node , std::string hash);
    Standard_EXPORT bool polyhedron(std::vector<std::vector<int>> faces,std::vector<double> points , std::string node , std::string hash ); ; 
    Standard_EXPORT bool polygon(std::vector<std::vector<int>> faces ,std::vector<double> points, std::string node , std::string hash); 
    Standard_EXPORT bool difference_complex( std::string nodeA , std::string hashA , std::string nodeB , std::string hashB );
    Standard_EXPORT bool union_complex( std::string nodeA , std::string hashA , std::string nodeB , std::string hashB );
    Standard_EXPORT bool intersection_complex( std::string nodeA , std::string hashA , std::string nodeB , std::string hashB );
    Standard_EXPORT bool translate(double x , double y , double z , std::string node , std::string hash ); ;
    Standard_EXPORT bool scale(double x , double y , double z , std::string node , std::string hash ); ;
    Standard_EXPORT bool rotate(double x , double y, double z , std::string node , std::string hash ); ;
    Standard_EXPORT bool rotateX(double x , std::string node , std::string hash ); ;
    Standard_EXPORT bool rotateY(double y , std::string node , std::string hash ); ; 
    Standard_EXPORT bool rotateZ(double z , std::string node , std::string hash);  
    Standard_EXPORT bool extrude(double h1, std::string node , std::string hash); 
    Standard_EXPORT bool minkowski( std::string nodeA , std::string hashA , std::string nodeB , std::string hashB );
    Standard_EXPORT bool remove(std::string node , std::string hash ); 
    Standard_EXPORT bool copy_brep(std::string nodeA,std::string hashA,std::string nodeB,std::string hashB);
    Standard_EXPORT bool write_stl(std::string node,std::string hash,std::string fname);
    Standard_EXPORT bool boolean_tuple( std::string node , std::string hash , std::vector< std::vector<std::vector<std::string>>>& children , int type  ); 
Standard_EXPORT bool shift_tuple(std::string nodeA,std::string hashA,std::vector< std::vector<std::vector<std::string>>>& children,double x,double y,double z,int type); 
 
    Standard_EXPORT bool convert_brep_tostring(double quality, std::string& result , std::string node , std::string hash ); 
    Standard_EXPORT bool convert_brep_tovectors(double quality, std::vector<double> vecs , std::string node , std::string hash ); 

Standard_EXPORT bool convert_children_tovectors(double quality, std::vector<std::vector<std::vector<std::string>>>&children,std::vector<std::vector<double>> &vec ); 

    Standard_EXPORT bool exists( std::string node , std::string hash );

    Standard_EXPORT bool test_generate();

	protected:
	private:
}; 
