
/***************************************************************************
 *   Copyright (c) Damien Towning         (connolly.damien@gmail.com) 2017 *
 *                                                                         *
 *   This file is part of the Makertron CSG cad system.                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

// OpenCascade Includes 

#include <gp.hxx>
#include <gp_Pln.hxx>
#include <gp_Ax2.hxx>
#include <gp_Circ.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Face.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_ListOfShape.hxx>
#include <Poly_Triangulation.hxx>

#include <Bnd_Box.hxx>
#include <BRepBndLib.hxx>
#include <OSD_Path.hxx>
#include <OSD_OpenFile.hxx>
#include <RWStl.hxx>

#include <StlAPI_Writer.hxx>

//#include <StlMesh_Mesh.hxx>
//#include <StlTransfer.hxx>

#include <BRep_Tool.hxx>
#include <BRepTools.hxx>

#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>

#include <BRepFeat_MakeCylindricalHole.hxx>
#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>

#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_GTransform.hxx>
#include <BRepBuilderAPI_MakeSolid.hxx>
#include <BRepBuilderAPI_Transform.hxx>

#include <BRepOffset_MakeOffset.hxx>

//#include <BRepOffsetAPI_MakeEvolved.hxx>

#include <BRepOffsetAPI_Sewing.hxx>

#include <GeomAbs_JoinType.hxx> 
#include <GeomAPI_IntSS.hxx> 

// Brep To CGAL conversion 

#include <BrepCgal.h>
#include <ReadWrite.h>
#include <Geometry.h>
 
// Streams 
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// Math
#include <math.h>
#include <float.h>
#include <cmath>
#include <assert.h>

using namespace std;

ReadWrite ReaderWriter; 

Geometry::Geometry() {
}

// ----------------------------------------------------------------------
// Error to console 
// ----------------------------------------------------------------------
void Geometry::error(std::string err) { 
  std::cerr << err << std::endl; 
} 

// ----------------------------------------------------------------------
// Prim 2d circle
// ----------------------------------------------------------------------
 bool Geometry::circle(double r1,TopoDS_Shape &aShape ) {
	try {  
		Standard_Real radius = r1;
		gp_Circ c = gp_Circ(gp_Ax2(gp_Pnt(0,0,0),gp_Dir(0,0,1)), radius );
		TopoDS_Edge Ec = BRepBuilderAPI_MakeEdge(c);
		TopoDS_Wire Wc = BRepBuilderAPI_MakeWire(Ec);
		//TopoDS_Face F = BRepBuilderAPI_MakeFace(Wc);
		aShape = BRepBuilderAPI_MakeFace(Wc).Shape();
		return true; 
	}
	catch(const std::exception& e) { 
		error(e.what()); 
	}
	return false; 
}

// ----------------------------------------------------------------------
// Polygon Simple
// ----------------------------------------------------------------------
bool Geometry::polygon(std::vector<std::vector<int>> faces,std::vector<double> points, TopoDS_Shape &aShape) {
	try { 
  	Standard_Integer a; 
		Standard_Real x1, y1, z1;
    // Construct first polygon to add other polygons to 
    BRepBuilderAPI_MakePolygon P; 
    for ( int ii = 0; ii < faces[0].size(); ii++ ) { 
      a = faces[0][ii]; 
		  x1 = points[(a*3)+0]; y1 = points[(a*3)+1]; z1 = points[(a*3)+2];
			P.Add(gp_Pnt(x1,y1,z1)); // add point  
    }
    P.Close(); 
    TopoDS_Wire PW = P.Wire();
    BRepBuilderAPI_MakeFace Pface(PW); 
    // Iterate through rest of polygon set 
    for ( int i = 1; i < faces.size(); i++ ) { 
      BRepBuilderAPI_MakePolygon Q; 
    	for ( int ii = 0; ii < faces[i].size(); ii++ ) {
      	a = faces[i][ii]; 
				x1 = points[(a*3)+0]; y1 = points[(a*3)+1]; z1 = points[(a*3)+2];
				Q.Add(gp_Pnt(x1,y1,z1)); // add point  
      }
      Q.Close(); 
      TopoDS_Wire QW = Q.Wire();
      Pface.Add(QW);
    } 
    aShape = Pface.Shape(); 
    return true; 
  }	
  catch(const std::exception& e) { 
		error(e.what()); 
	}
	return false; 
} 


// -----------------------------------------------------------------------
// Does shape contain faces
// -----------------------------------------------------------------------
bool Geometry::hasFaces(TopoDS_Shape &Shape) { 
  TopExp_Explorer exp ( Shape , TopAbs_FACE ); 
  if ( exp.More() != 0 ) { return true; } 
  return false;   
}

// ----------------------------------------------------------------------
// Polygon 
// polygon(points=[[0,0],[100,0],[0,100],[10+20,10],[80+20,10],[10+20,80],[0+50,0+50],[20+50,0+50],[0+50,20+50]], paths=[[2,1,0],[3,4,5],[6,7,8]]);
// Steps are as follows 
// Add each polygon to compound object either as a union to that compound or as a compound
// If an object does not intersect the compound it will be added as compound 
// If the object does intersect add the object as a union then do the difference of the 
// intersection. Have not tested extensively. Looking in the original OpenSCAD code 
// they appear to do a triangulation and something involving boundary squares. This 
// code could be copied directly as with the Minkowsi function but I am not sure 
// if this would gain a speed improvement or not. Much like the tuple operations 
// we should note that this gives us a sequence of boolean steps that are not 
// accessible to the parser for pipe line or parallel optimisations. 
// ----------------------------------------------------------------------

bool Geometry::polygonDifference(std::vector<std::vector<int>> faces,std::vector<double> points, TopoDS_Shape &aShape) {
	try { 
   	Standard_Integer a; 
	Standard_Real x1, y1, z1;
    // One unified shape that consists of all polygons both connected and disconnected
    TopoDS_Compound compound;
    BRep_Builder profileBuilder;
    profileBuilder.MakeCompound(compound);
    for ( int i = 0; i < faces.size(); i++ ) { // loop through all faces 
      TopoDS_Shape iShape; 
      TopoDS_Shape uShape;  
      BRepBuilderAPI_MakePolygon P; 
      //compound.Free( Standard_True );
      //iShape.Free( Standard_True );
      //uShape.Free( Standard_True );
      for ( int ii = 0; ii < faces[i].size(); ii++ ) { // loop through all vertices in faces
        a = faces[i][ii]; 
		    x1 = points[(a*3)+0]; y1 = points[(a*3)+1]; z1 = points[(a*3)+2];
		    P.Add(gp_Pnt(x1,y1,z1)); // add point  
      }
      P.Close(); 
      TopoDS_Wire PW = P.Wire();
      BRepBuilderAPI_MakeFace aFace(PW);  
      iShape = aFace.Shape(); // shape from the face
      if ( i == 0 ) { // first shape goes to the compound
        profileBuilder.Add(compound, iShape);      
        //std::cout << "Well we did this" << std::endl;     
      }
      else { 
        //std::cout << "Also this" << std::endl;            
        uShape = iShape; 
        intersection_complex( uShape , compound ); // get the intersection 
        if ( hasFaces( uShape ) == true ) { // do the union then the difference
          //std::cout << "and this" << std::endl;     
          union_complex( compound , iShape );  
          difference_complex( compound , uShape );
        } 
        else { // otherwise to the compound since we might have more than one free floating polygon .. 
         //std::cout << "As well as this" << std::endl;     
         profileBuilder.Add(compound, iShape );           
        } 
      } 
    }
    aShape = compound; 
    return true; 
  }	
  catch(const std::exception& e) { 
		error(e.what()); 
	}
	return false; 
} 

// ----------------------------------------------------------------------
// Polyhedron 
// ----------------------------------------------------------------------
bool Geometry::polyhedron(std::vector<std::vector<int>> faces,std::vector<double> points, TopoDS_Shape &aShape ) {
	try { 

		BRepOffsetAPI_Sewing sew(0.1);
		Standard_Integer a,b; 
		Standard_Integer width = 0; 
		Standard_Real x1, y1, z1;
		Standard_Real x2, y2, z2; 
		TopoDS_Wire wire;		
		BRep_Builder builder;
		std::vector<TopoDS_Vertex> foo;
		for ( int i = 0; i < faces.size(); i++ ) { // through face sets 
			for ( int ii = 0; ii < faces[i].size(); ii++ ) { // make list of points
				a = faces[i][ii]; 
				x1 = points[(a*3)+0]; y1 = points[(a*3)+1]; z1 = points[(a*3)+2];
				foo.push_back( BRepBuilderAPI_MakeVertex(gp_Pnt(x1,y1,z1)) ); 	
			}
			builder.MakeWire(wire);			
			for ( int iii = 0; iii < foo.size()-1; iii++ ) { // build wire from points in foo for a face set
				builder.Add(wire, BRepBuilderAPI_MakeEdge(foo[iii],foo[iii+1]));
			}
			builder.Add(wire, BRepBuilderAPI_MakeEdge(foo[foo.size()-1],foo[0])); // closing wire for face set 
			sew.Add( BRepBuilderAPI_MakeFace( wire ) ); // add to sewing object 
			foo.clear(); 
		} 
		sew.Perform(); // sew it together
		TopoDS_Shape obj = sew.SewedShape(); // get the shape
		BRepBuilderAPI_MakeSolid brep_solid(TopoDS::Shell(obj)); // Now some unclear foo that took a bit to find 
																														 // Yes the shape will show type three as a shell 
																														 // but you have to wrap it TopoDS::shel() anyway :| 	
		aShape = brep_solid.Solid();

		return true; 
	}
	catch(const std::exception& e) { 
		error(e.what()); 
	}
	return false; 
}

// ----------------------------------------------------------------------
// Prim Sphere
// ----------------------------------------------------------------------
bool Geometry::sphere(double radius, double x , double y , double z , TopoDS_Shape &aShape ) {
	try { 
		Standard_Real sphere_radius = radius;
		//gp_Ax2 sphere_origin = gp_Ax2(gp_Pnt(x,y,z), gp_Dir(0,0,1));
		//TopoDS_Shape sphere = BRepPrimAPI_MakeSphere(sphere_origin, sphere_radius ).Shape();
		aShape = BRepPrimAPI_MakeSphere( sphere_radius ).Shape();
		return true; 
	}
	catch(const std::exception& e) { 
    error(e.what());
	}
	return false; 
} 

// ----------------------------------------------------------------------
// Prim box 
// ----------------------------------------------------------------------
bool Geometry::cube(double x , double y , double z , double xs , double ys , double zs , TopoDS_Shape &aShape ) {
	try { 	
		Standard_Real box_xs = xs;
		Standard_Real box_ys = ys;
		Standard_Real box_zs = zs;
		gp_Ax2 box_origin = gp_Ax2(gp_Pnt(x,y,z), gp_Dir(0,0,1));
		aShape = BRepPrimAPI_MakeBox(     box_origin,   box_xs , box_ys  , box_zs ).Shape();
		return true; 
	}
	catch(const std::exception& e) { 
    error(e.what()); 
	}
	return false; 
} 

// ----------------------------------------------------------------------
// Prim cone 
// ----------------------------------------------------------------------
bool Geometry::cone(double r1,double r2,double h,double z , TopoDS_Shape &aShape ) {
	try {   	  	
		Standard_Real cone_r1 = r1;
		Standard_Real cone_r2 = r2;
		Standard_Real cone_h  = h;
		Standard_Real cone_z = z; 
		gp_Ax2 cone_origin = gp_Ax2(gp_Pnt(0,0,cone_z), gp_Dir(0,0,1));
		//aShape = BRepPrimAPI_MakeCylinder( cone_origin , cone_r1, cone_h ).Shape();
		aShape =  BRepPrimAPI_MakeCone( cone_origin , cone_r1 , cone_r2 , cone_h ).Shape(); 
		return true; 
	}
	catch(const std::exception& e) { 
		error(e.what()); 
	}
	return false; 
} 

// ----------------------------------------------------------------------
// Prim Cylinder
// ----------------------------------------------------------------------
bool Geometry::cylinder(double r1, double h,double z , TopoDS_Shape &aShape ) {
	try { 
		Standard_Real cylinder_r1 = r1;
		Standard_Real cylinder_h  = h;
		Standard_Real cylinder_z = z; 
		gp_Ax2 cylinder_origin = gp_Ax2(gp_Pnt(0,0,cylinder_z), gp_Dir(0,0,1));
		aShape = BRepPrimAPI_MakeCylinder( cylinder_origin , cylinder_r1, cylinder_h ).Shape();
		return true; 
	}
	catch(const std::exception& e) { 
    error(e.what());
  }
	return false; 	
} 

// ----------------------------------------------------------------------
// Translate a brep 
// ----------------------------------------------------------------------
bool Geometry::translate( double x , double y , double z , TopoDS_Shape &aShape ) { 
	try { 
		gp_Trsf translate;
 	  translate.SetTranslation(gp_Vec(x, y, z));
		aShape = BRepBuilderAPI_Transform(aShape, translate, false).Shape();
		return true;
	}
	catch(const std::exception& e) { 
		error(e.what());
	}
	return false;
}

// ----------------------------------------------------------------------
// Translate a brep 
// ----------------------------------------------------------------------
bool Geometry::scale( double x , double y , double z , TopoDS_Shape &aShape ) { 
	try {
		gp_GTrsf scale;
		gp_Mat m( x, 0, 0, 0, y, 0, 0, 0, z );	
		scale.SetVectorialPart(m);
		aShape = BRepBuilderAPI_GTransform(aShape, scale, true).Shape();
		return true;
	}
	catch(const std::exception& e) { 
		error(e.what());
	}
	return false;
}

// ----------------------------------------------------------------------
// Rotate brep x axis 
// ----------------------------------------------------------------------
bool Geometry::rotate( double x , double y , double z , TopoDS_Shape &aShape ) { 
	try { 
		Standard_Real xr = x;
		gp_Trsf rotateX;
  	rotateX.SetRotation( gp::OX(), xr );
		aShape = BRepBuilderAPI_Transform(aShape, rotateX).Shape();
		Standard_Real yr = y;
		gp_Trsf rotateY;
  	rotateY.SetRotation( gp::OY(), yr );
		aShape = BRepBuilderAPI_Transform(aShape, rotateY).Shape();
		Standard_Real zr = z;
		gp_Trsf rotateZ;
  	rotateZ.SetRotation( gp::OZ(), zr );
		aShape = BRepBuilderAPI_Transform(aShape, rotateZ).Shape();
		return true;
	}
	catch(const std::exception& e) { 
    error(e.what());
	}
	return false;
}


// ----------------------------------------------------------------------
// Rotate brep x axis 
// ----------------------------------------------------------------------
bool Geometry::rotateX( double x , TopoDS_Shape &aShape ) { 
	try { 
		Standard_Real xr = x;
		gp_Trsf rotate;
  	rotate.SetRotation( gp::OX(), xr );
		aShape = BRepBuilderAPI_Transform(aShape, rotate).Shape();
		return true;
	}
	catch(const std::exception& e) { 
    error(e.what());
	}
	return false;
}

// ----------------------------------------------------------------------
// Rotate brep y axis 
// ----------------------------------------------------------------------
bool Geometry::rotateY( double y ,  TopoDS_Shape &aShape ) { 
	try { 
		Standard_Real yr = y;
		gp_Trsf rotate;
		rotate.SetRotation( gp::OY(), yr );
		aShape = BRepBuilderAPI_Transform(aShape, rotate).Shape();
		return true; 
	}
	catch(const std::exception& e) { 
    error(e.what());
	}
	return false;
}

// ----------------------------------------------------------------------
// Rotate brep z axis 
// ----------------------------------------------------------------------
bool Geometry::rotateZ( double z , TopoDS_Shape &aShape ) { 
	try { 
		Standard_Real zr = z;
		gp_Trsf rotate;
  	rotate.SetRotation( gp::OZ(), zr );
		aShape = BRepBuilderAPI_Transform(aShape, rotate).Shape();
		return true; 
	}
	catch(const std::exception& e) { 
    error(e.what());
	}
	return false;
}

// ----------------------------------------------------------------------
// Extrusion ( does not include twist yet ) 
// ----------------------------------------------------------------------
bool Geometry::extrude(double h1,TopoDS_Shape &aShape) {
	try {  
		Standard_Real height = h1;
		aShape = BRepPrimAPI_MakePrism(aShape,gp_Vec(0,0,height));
		return true; 
	}
	catch(const std::exception& e) {
    error(e.what()); 
	}
	return false; 
}

// ----------------------------------------------------------------------
// Generate minkowski 
// ----------------------------------------------------------------------
bool Geometry::minkowski(TopoDS_Shape &aShape,TopoDS_Shape bShape) { 	
	try { 	
		// Tolerances 
		Standard_Real tolerance = 0.25;
		Standard_Real angular_tolerance = 0.5;
		Standard_Real minTriangleSize = Precision::Confusion();
		// Set the tolerances
		BRepMesh_FastDiscret::Parameters m_MeshParams;
		m_MeshParams.ControlSurfaceDeflection = Standard_True; 
		m_MeshParams.Deflection = tolerance;
		m_MeshParams.MinSize = minTriangleSize;
		m_MeshParams.InternalVerticesMode = Standard_False;
		m_MeshParams.Relative=Standard_False;
		m_MeshParams.Angle = angular_tolerance;
		// Incremental meshes from shapes 
		BRepMesh_IncrementalMesh ( aShape, m_MeshParams );
		BRepMesh_IncrementalMesh ( bShape, m_MeshParams );
		TopoDS_Shape rShape;
	 	// In to the CGAL. Putting cgal geometry operations in thar for now  
		BrepCgal brepcgal;
		brepcgal.minkowski( aShape , bShape , rShape );
		aShape = rShape; 
		return true; 
	}
	catch(const std::exception& e) { 
    error(e.what());
	}
	return false; 
}

// ----------------------------------------------------------------------
// Simple Difference between two objects
// ----------------------------------------------------------------------
bool Geometry::difference(TopoDS_Shape &aShape, TopoDS_Shape &bShape) { 
	try { 
		aShape = BRepAlgoAPI_Cut( aShape ,  bShape ).Shape();
		return true; 
	}
	catch(const std::exception& e) { 
		error(e.what());
    return false;   
	}
	return false; 
}

// ----------------------------------------------------------------------
// Complex Difference between two objects 
// ----------------------------------------------------------------------
bool Geometry::difference_complex(TopoDS_Shape &aShape, TopoDS_Shape &bShape) { 
  try { 
    Standard_Boolean bRunParallel;
    Standard_Real aFuzzyValue;
    BRepAlgoAPI_Cut aBuilder;
    TopTools_ListOfShape aLS;
    TopTools_ListOfShape aLT;
    aLS.Append(aShape);
    aLT.Append(bShape); 
    bRunParallel=Standard_True;
    aFuzzyValue=2.1e-5;
    aBuilder.SetFuzzyValue(aFuzzyValue);
    aBuilder.SetArguments(aLS);
    aBuilder.SetTools(aLT);
    aBuilder.Build(); 
    if (aBuilder.HasErrors()) { error("Failed Complex Difference"); return false; }
	  aShape = aBuilder.Shape(); 
    return true; 
  } 
  catch(const std::exception &e) { 
    error(e.what());
    return false; 
	}
  return false; 
} 

// ----------------------------------------------------------------------
// Simple Union between two objects
// ----------------------------------------------------------------------
bool Geometry::uni(TopoDS_Shape &aShape, TopoDS_Shape &bShape) { 
	try { 
		aShape = BRepAlgoAPI_Fuse( aShape ,  bShape ).Shape();
		return true; 
	}
  catch(const std::exception &e) { 
    error(e.what());
    return false; 
	}
  return false; 
}

// ----------------------------------------------------------------------
// Complex Union between two objects 
// ----------------------------------------------------------------------
bool Geometry::union_complex(TopoDS_Shape &aShape, TopoDS_Shape &bShape) { 
  try { 
    Standard_Boolean bRunParallel;
    Standard_Real aFuzzyValue;
    BRepAlgoAPI_Fuse aBuilder;
    TopTools_ListOfShape aLS;
    TopTools_ListOfShape aLT;
    aLS.Append(aShape);
    aLT.Append(bShape); 
    bRunParallel=Standard_True;
    aFuzzyValue=2.1e-5;
    aBuilder.SetFuzzyValue(aFuzzyValue);
    aBuilder.SetArguments(aLS);
    aBuilder.SetTools(aLT);
    aBuilder.Build(); 
    if (aBuilder.HasErrors()) { error("Failed Complex Union"); return false; }
	  aShape = aBuilder.Shape(); 
    return true; 
  }
  catch(const std::exception &e) { 
    error(e.what());
    return false; 
	}
	return false;
} 

// ----------------------------------------------------------------------
// Simple Intersection between two objects
// ----------------------------------------------------------------------
bool Geometry::intersection(TopoDS_Shape &aShape, TopoDS_Shape &bShape) { 
	try { 
		aShape = BRepAlgoAPI_Common( aShape ,  bShape ).Shape();
		return true; 
	}
	catch(const std::exception &e) { 
    error(e.what());
	}
	return false; 
}

// ----------------------------------------------------------------------
// Complex Intersection between two objects
// ----------------------------------------------------------------------
bool Geometry::intersection_complex(TopoDS_Shape &aShape, TopoDS_Shape &bShape) { 
  try { 
    Standard_Boolean bRunParallel;
    Standard_Real aFuzzyValue;
    BRepAlgoAPI_Common aBuilder;
    TopTools_ListOfShape aLS;
    TopTools_ListOfShape aLT;
    aLS.Append(aShape);
    aLT.Append(bShape); 
    bRunParallel=Standard_True;
    aFuzzyValue=2.1e-5;
    aBuilder.SetFuzzyValue(aFuzzyValue);
    aBuilder.SetArguments(aLS);
    aBuilder.SetTools(aLT);
    aBuilder.Build(); 
    if (aBuilder.HasErrors()) { error("Failed Complex Intersection"); return false; }
	  aShape = aBuilder.Shape(); 
    return true; 
  }  
  catch (const std::exception &e)
  {
    error(e.what());
    return false; 
  }
  return false; 
} 

 // ----------------------------------------------------------------------
// add a new shape to the stack 
// ----------------------------------------------------------------------
bool Geometry::add( TopoDS_Shape& shapeA , std::string node , std::string hash) {
  try { 
    if ( ReaderWriter.WriteBREPDb(shapeA,node,hash) == false ) { error("Failed to write brep into database"); return false; } 
  	return true; 
  } 
 catch (const std::exception &e)
 {
   error(e.what());
   return false; 
 }
 return false; 
}

// ----------------------------------------------------------------------
// get a shape from the stack 
// ----------------------------------------------------------------------
bool Geometry::get( TopoDS_Shape& shapeA , std::string node , std::string hash ) { 
  try { 
    if ( ReaderWriter.ReadBREPDb( shapeA , node , hash ) == false ) { error("Failed to read brep from database"); return false; }  
	  return true; 
  } 
 catch (const std::exception &e)
  {
    error(e.what());
    return false; 
  }
  return false; 
}

// ----------------------------------------------------------------------
// Delete an object from stack 
// ----------------------------------------------------------------------
bool Geometry::remove( std::string node , std::string hash ) {
  try {     
    if ( ReaderWriter.DeleteBREPDb(node,hash) == false ) { error("Failed To delete brep in database"); return false; }  
		return true; 
  } 
 catch (const std::exception &e)
  {
    error(e.what());
    return false; 
  }
  return false; 
}

// ----------------------------------------------------------------------
// alter an existing shape in the stack 
// ----------------------------------------------------------------------
bool Geometry::update( TopoDS_Shape& shapeA , std::string node , std::string hash ) {
  try {     
    if ( ReaderWriter.UpdateBREPDb(shapeA,node , hash) == false ) { error("Failed To update brep in database"); return false; }  
		return true; 
  } 
 catch (const std::exception &e)
  {
    error(e.what());
    return false; 
  }
  return false; 
}

// -------------------------------------------------------------------------------------------
// Get faces from shape 
// -------------------------------------------------------------------------------------------
TopoDS_Shape Geometry::getFaceFromShape(TopoDS_Shape shape1)
{
  TopoDS_Shape myshape;//=shape1 ;
  TopExp_Explorer Ex;
  for (Ex.Init(shape1,TopAbs_FACE); Ex.More(); Ex.Next())
  {
    myshape = Ex.Current();
  }
  return myshape;
}

// ------------------------------------------------------------------------------------------
// Do two shapes intersect 
// ------------------------------------------------------------------------------------------
bool Geometry::IsIntersecting(TopoDS_Shape srf1,TopoDS_Shape srf2) {
  srf1 = getFaceFromShape(srf1);
  srf2 = getFaceFromShape(srf2);
  Handle(Geom_Surface) S1 = BRep_Tool::Surface(TopoDS::Face(srf1));
  Handle(Geom_Surface) S2 = BRep_Tool::Surface(TopoDS::Face(srf2));
  //double precision = Precision::Confusion();
  double precision = 0.1;
  GeomAPI_IntSS Intersector(S1, S2, precision);
  if (Intersector.NbLines() > 0) { return true; } else { return false;}
}

