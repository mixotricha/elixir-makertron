defmodule MakertronClient.Mixfile do
  use Mix.Project

  def project do
    [
      app: :makertron_client,
      version: "6.0.0",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
       prod: [
        include_executables_for: [:unix],
       ]
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {MakertronClient, []},
      extra_applications: [:logger,:logger_file_backend]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_), do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.3.3"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_html, "~> 2.10"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:gettext, "~> 0.11"},
        {:plug_cowboy, "~> 1.0"},
      {:poison, "~> 2.2.0"},
      {:react_phoenix, "~> 0.3.0"}, 
      {:hound, "~> 1.0"},
      {:observer_cli, "~> 1.4"},
     {:logger_file_backend, "~> 0.0.10"}
    ]
  end
end
