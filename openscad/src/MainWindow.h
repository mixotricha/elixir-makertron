#pragma once

#include "qtgettext.h"
#include <QMainWindow>
#include <QIcon>
#include "ui_MainWindow.h"
//#include "UIUtils.h"
#include "openscad.h"
//#include "builtincontext.h"

#include "memory.h"
#include "editor.h"

#include <vector>
#include <QMutex>
#include <QElapsedTimer>
#include <QTime>
#include <QIODevice>
#include "tabmanager.h"

class MainWindow : public QMainWindow, public Ui::MainWindow
{
	Q_OBJECT

public:
	EditorInterface *customizerEditor;

	EditorInterface *activeEditor;
	TabManager *tabManager;

	//QAction *actionRecentFile[UIUtils::maxRecentFiles];
	QMap<QString, QString> knownFileExtensions;

	QLabel *versionLabel;
	QWidget *editorDockTitleWidget;
	QWidget *consoleDockTitleWidget;

	int compileErrors;
	int compileWarnings;

	MainWindow(const QStringList &filenames);
	~MainWindow();

protected:
	void closeEvent(QCloseEvent *event) override;

private slots:
	//void setTabToolBarVisible(int);
	//void updateUndockMode(bool undockMode);
	//void updateReorderMode(bool reorderMode);
	void setFont(const QString &family, uint size);
	void consoleOutput(const QString &msg);

public:
	static void consoleOutput(const std::string &msg, void *userdata);
	static void noOutput(const std::string &, void*) {};  // /dev/null

	bool fileChangedOnDisk();
	void parseTopLevelDocument(bool rebuildParameterWidget);
	void exceptionCleanup();

private:
	void initActionIcon(QAction *action, const char *darkResource, const char *lightResource);
	void handleFileDrop(const QString &filename);
	void updateCamera(const class FileContext &ctx);
	void updateTemporalVariables();
	void updateCompileResult();
	void compile(bool reload, bool forcedone = false, bool rebuildParameterWidget=true);
	void compileCSG();
	bool checkEditorModified();
	//QString dumpCSGTree(AbstractNode *root);

	void loadViewSettings();
	void loadDesignSettings();
	void updateWindowSettings(bool console, bool editor, bool customizer, bool toolbar);
	void saveBackup();
	void writeBackup(class QFile *file);
	void show_examples();
	void setDockWidgetTitle(QDockWidget *dockWidget, QString prefix, bool topLevel);
	void addKeyboardShortCut(const QList<QAction *> &actions);
	void updateStatusBar(class ProgressWidget *progressWidget);

  class LibraryInfoDialog* library_info_dialog;
  class FontListDialog *font_list_dialog;

public slots:

private slots:

private slots:

	void preferences();
	void hideToolbars();
	void hideEditor();
	void hideConsole();
	void showConsole();
	void hideParameters();

public slots:


private slots:

	//void findString(QString);


	//bool event(QEvent* event) override;
protected:
	//bool eventFilter(QObject* obj, QEvent *event) override;

private slots:




public:
	void viewModeActionsUncheck();
	void setCurrentOutput();
	void clearCurrentOutput();
	void hideCurrentOutput();
  bool isEmpty();

	void changedTopLevelConsole(bool);
	void changedTopLevelEditor(bool);

	QList<double> getTranslation() const;
	QList<double> getRotation() const;

public slots:

	void on_editorDock_visibilityChanged(bool);
	void on_consoleDock_visibilityChanged(bool);
	void on_parameterDock_visibilityChanged(bool);

	void editorTopLevelChanged(bool);
	void consoleTopLevelChanged(bool);
	void parameterTopLevelChanged(bool);
	void processEvents();

#ifdef ENABLE_OPENCSG
	void viewModePreview();
#endif
#ifdef ENABLE_CGAL
	void viewModeSurface();
	void viewModeWireframe();
#endif

	void dragEnterEvent(QDragEnterEvent *event) override;
	void dropEvent(QDropEvent *event) override;
	void helpAbout();
	void helpHomepage();
	void helpManual();
	void helpCheatSheet();
	void helpLibrary();
	void helpFontInfo();
	void quit();




private:
	bool network_progress_func(const double permille);
	static void report_func(const class AbstractNode*, void *vp, int mark);
	static bool undockMode;
	static bool reorderMode;
	static const int tabStopWidth;
	static QElapsedTimer *progressThrottle;

	shared_ptr<class CSGNode> csgRoot;		   // Result of the CSGTreeEvaluator
	shared_ptr<CSGNode> normalizedRoot;		  // Normalized CSG tree
 	shared_ptr<class CSGProducts> root_products;
	shared_ptr<CSGProducts> highlights_products;
	shared_ptr<CSGProducts> background_products;

	char const * afterCompileSlot;
	bool procevents;
	class QTemporaryFile *tempFile;
	class ProgressWidget *progresswidget;
	class CGALWorker *cgalworker;
	QMutex consolemutex;
	EditorInterface *renderedEditor; // stores pointer to editor which has been most recently rendered
	time_t includes_mtime;   // latest include mod time
	time_t deps_mtime;	  // latest dependency mod time
	//std::unordered_map<std::string, QString> export_paths; // for each file type, where it was exported to last
	//QString exportPath(const char *suffix); // look up the last export path and generate one if not found
	int last_parser_error_pos; // last highlighted error position
	int tabCount = 0;

signals:
	void highlightError(int);
	void unhighlightLastError();
};

class GuiLocker
{
public:
	GuiLocker() {
		GuiLocker::lock();
	}
	~GuiLocker() {
		GuiLocker::unlock();
	}
	static bool isLocked() { return gui_locked > 0; }
	static void lock() {
		gui_locked++;
	}
	static void unlock() {
		gui_locked--;
	}

private:
 	static unsigned int gui_locked;
};
