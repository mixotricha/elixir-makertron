/*
 *  OpenSCAD (www.openscad.org)
 *  Copyright (C) 2009-2011 Clifford Wolf <clifford@clifford.at> and
 *                          Marius Kintel <marius@kintel.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  As a special exception, you have permission to link this program
 *  with the CGAL library and distribute executables, as long as you
 *  follow the requirements of the GNU GPL in regard to all of the
 *  software in the executable aside from CGAL.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "openscad.h"
#include "stackcheck.h"
#include "MainWindow.h"
#include "WindowManager.h"
#include "OpenSCADApp.h"
#include "PlatformUtils.h"
#include "tabmanager.h"
#include "MainWindow.h"
#include "OpenSCADApp.h"
#include "editor.h"
#include "tabwidget.h"
#include "scintillaeditor.h"
#include "Console.h"
#include "printutils.h"
#include "OpenSCADApp.h"
#include "Dock.h"

#include <stdlib.h>
#include <cstdio>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <ciso646> // C alternative tokens (xor)
#include <iostream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/version.hpp>
#include <boost/foreach.hpp>

#include <sys/stat.h>

#include <QtPlugin>
#include <QString>
#include <QStringList>
#include <QDir>
#include <QMetaType>
#include <QTextCodec>

#include <QMenu>
#include <QTime>
#include <QMenuBar>
#include <QSplitter>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QFileInfo>
#include <QTextStream>
#include <QStatusBar>
#include <QDropEvent>
#include <QMimeData>
#include <QUrl>
#include <QTimer>
#include <QMessageBox>
#include <QDesktopServices>
#include <QProgressDialog>
#include <QMutexLocker>
#include <QTemporaryFile>
#include <QDockWidget>
#include <QClipboard>
#include <QDesktopWidget>
#include <QSettings> //Include QSettings for direct operations on settings arrays


#include <QFile>
#include <QSaveFile>
#include <QShortcut>
#include <Qsci/qscicommand.h>
#include <Qsci/qscicommandset.h>
#include <QChar>
#include <QTabBar>
#include <QStackedWidget>
#include <QMouseEvent>
#include <QList>

namespace fs = boost::filesystem;
using std::string;
using std::vector;

std::string currentdir;

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define QT_HTML_ESCAPE(qstring) (qstring).toHtmlEscaped()

// Global application state
unsigned int GuiLocker::gui_locked = 0;

static char copyrighttext[] =
	"Copyright (C) 2009-2019 The OpenSCAD Developers\n\n"
	"This program is free software; you can redistribute it and/or modify "
	"it under the terms of the GNU General Public License as published by "
	"the Free Software Foundation; either version 2 of the License, or "
	"(at your option) any later version.\n";
bool MainWindow::undockMode = false;
bool MainWindow::reorderMode = false;
const int MainWindow::tabStopWidth = 15;

// Window Manager CLASS

WindowManager::WindowManager()
{
}

WindowManager::~WindowManager()
{
}

void WindowManager::add(MainWindow *mainwin)
{
	this->windows.insert(mainwin);
}

void WindowManager::remove(MainWindow *mainwin)
{
	this->windows.remove(mainwin);
}

const QSet<MainWindow*> &WindowManager::getWindows() const
{
	return this->windows;
}

// Dock CLass

Dock::Dock(QWidget *parent) : QDockWidget(parent), action(nullptr), updateSettings(true){}

Dock::~Dock(){}

void Dock::disableSettingsUpdate(){}

void Dock::setVisible(bool visible){}

void Dock::setConfigKey(const QString configKey){}

void Dock::setAction(QAction *action){}

// OpenSCADApp
OpenSCADApp::OpenSCADApp(int &argc ,char **argv)
	: QApplication(argc, argv), fontCacheDialog(nullptr)
{
}

OpenSCADApp::~OpenSCADApp()
{
	delete this->fontCacheDialog;
}

// See: https://bugreports.qt.io/browse/QTBUG-65592
void OpenSCADApp::workaround_QTBUG_65592(QObject* o, QEvent* e)
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
	QMainWindow* mw;
	if (o->isWidgetType() && e->type() == QEvent::MouseButtonPress && (mw = qobject_cast< QMainWindow* >(o))) {
		for (auto& ch : mw->children()) {
			if (auto dw = qobject_cast< QDockWidget* >(ch)) {
				auto pname = "_wa-QTBUG-65592";
				auto v = dw->property(pname);
				if (v.isNull()) {
					dw->setProperty(pname, true);
					mw->restoreDockWidget(dw);
					auto area = mw->dockWidgetArea(dw);
					auto orient = area == Qt::TopDockWidgetArea || area == Qt::BottomDockWidgetArea ? Qt::Horizontal : Qt::Vertical;
					mw->resizeDocks({dw}, {orient == Qt::Horizontal ? dw->width() : dw->height() }, orient);
				}
			}
		}
	}
#endif
}

bool OpenSCADApp::notify(QObject *object, QEvent *event)
{
	QString msg;
	try {
		workaround_QTBUG_65592(object, event);
		return QApplication::notify(object, event);
	}
	catch (const std::exception &e) {
		msg = e.what();
	}
	catch (...) {
		msg = _("Unknown error");
	}
	// This happens when an uncaught exception is thrown in a Qt event handler
	QMessageBox::critical(nullptr, QString(_("Critical Error")), QString(_("A critical error was caught. The application may have become unstable:\n%1")).arg(QString(msg)));
	return false;
}

/*!
	Requests to open a file from an external event, e.g. by double-clicking a filename.
 */
void OpenSCADApp::requestOpenFile(const QString &filename)
{
	//for (auto win : this->windowManager.getWindows()) {
		// if we have an empty open window, use that one
		//if (win->isEmpty()) {
			//win->tabManager->createTab(filename);
			//return;
		//}
	//}

	// ..otherwise, create a new one
	//new MainWindow(QStringList(filename));
}

void OpenSCADApp::showFontCacheDialog()
{
	//if (!this->fontCacheDialog) this->fontCacheDialog = new QProgressDialog();
	//this->fontCacheDialog->setLabelText(_("Fontconfig needs to update its font cache.\nThis can take up to a couple of minutes."));
	//this->fontCacheDialog->setMinimum(0);
	//this->fontCacheDialog->setMaximum(0);
	//this->fontCacheDialog->setCancelButton(nullptr);
	//this->fontCacheDialog->exec();
}

void OpenSCADApp::hideFontCacheDialog()
{
//	assert(this->fontCacheDialog);
//	this->fontCacheDialog->reset();
}


void OpenSCADApp::releaseQSettingsCached() {
	//QSettingsCached{}.release();
}

// Console Class

Console::Console(QWidget *parent) : QPlainTextEdit(parent)
{
	setupUi(this);
	connect(this->actionClear, SIGNAL(triggered()), this, SLOT(actionClearConsole_triggered()));
	connect(this->actionSaveAs, SIGNAL(triggered()), this, SLOT(actionSaveAs_triggered()));
}

Console::~Console()
{
}

void Console::actionClearConsole_triggered()
{
	this->document()->clear();
}

void Console::actionSaveAs_triggered()
{
	const auto& text = this->document()->toPlainText();
	const auto fileName = QFileDialog::getSaveFileName(this, _("Save console content"));
	QFile file(fileName);
	if (file.open(QIODevice::ReadWrite)) {
		QTextStream stream(&file);
		stream << text;
		stream.flush();
		PRINTB("Console content saved to '%s'.", fileName.toStdString());
	}
}

void Console::contextMenuEvent(QContextMenuEvent *event)
{
	// Clear leaves characterCount() at 1, not 0
	const bool hasContent = this->document()->characterCount() > 1;
	this->actionClear->setEnabled(hasContent);
	this->actionSaveAs->setEnabled(hasContent);
	QMenu *menu = createStandardContextMenu();
	menu->insertAction(menu->actions().at(0), this->actionClear);
	menu->addSeparator();
	menu->addAction(this->actionSaveAs);
    menu->exec(event->globalPos());
	delete menu;
}

// Scintilla CLASS

QString ScintillaEditor::cursorPlaceHolder = "^~^";

class SettingsConverter {
public:

};

EditorColorScheme::~EditorColorScheme(){}

bool EditorColorScheme::valid() const
{
	return !_name.isEmpty();
}

const QString & EditorColorScheme::name() const
{
	return _name;
}

int EditorColorScheme::index() const
{
	return _index;
}

const boost::property_tree::ptree & EditorColorScheme::propertyTree() const
{
	return pt;
}

ScintillaEditor::ScintillaEditor(QWidget *parent) : EditorInterface(parent)
{
	scintillaLayout = new QVBoxLayout(this);
	qsci = new QsciScintilla(this);
}

QPoint ScintillaEditor::mapToGlobal(const QPoint &pos){}
QMenu * ScintillaEditor::createStandardContextMenu(){}
void ScintillaEditor::addTemplate(){}
void ScintillaEditor::displayTemplates(){}
/**
 * Apply the settings that are changeable in the preferences. This is also
 * called in the event handler from the preferences.
 */
void ScintillaEditor::applySettings(){}
void ScintillaEditor::fireModificationChanged(bool b){}
void ScintillaEditor::public_applySettings(){}
void ScintillaEditor::setPlainText(const QString &text){}
QString ScintillaEditor::toPlainText(){}
void ScintillaEditor::setContentModified(bool modified){}
bool ScintillaEditor::isContentModified(){}
void ScintillaEditor::highlightError(int error_pos){}
void ScintillaEditor::unhighlightLastError(){}
QColor ScintillaEditor::readColor(const boost::property_tree::ptree &pt, const std::string name, const QColor defaultColor){}
std::string ScintillaEditor::readString(const boost::property_tree::ptree &pt, const std::string name, const std::string defaultValue){}
int ScintillaEditor::readInt(const boost::property_tree::ptree &pt, const std::string name, const int defaultValue){}
void ScintillaEditor::setColormap(const EditorColorScheme *colorScheme){}
void ScintillaEditor::noColor(){}
ScintillaEditor::colorscheme_set_t ScintillaEditor::enumerateColorSchemes(){}
QStringList ScintillaEditor::colorSchemes(){}
bool ScintillaEditor::canUndo(){}
void ScintillaEditor::setHighlightScheme(const QString &name){}
void ScintillaEditor::insert(const QString &text){}
void ScintillaEditor::setText(const QString &text){}
void ScintillaEditor::undo(){}
void ScintillaEditor::redo(){}
void ScintillaEditor::cut(){}
void ScintillaEditor::copy(){}
void ScintillaEditor::paste(){}
void ScintillaEditor::zoomIn(){}
void ScintillaEditor::zoomOut(){}
void ScintillaEditor::initFont(const QString& fontName, uint size){}
void ScintillaEditor::initMargin(){}
void ScintillaEditor::onTextChanged(){}
int ScintillaEditor::updateFindIndicators(const QString &findText, bool visibility){}
bool ScintillaEditor::find(const QString &expr, bool findNext, bool findBackwards){}
void ScintillaEditor::replaceSelectedText(const QString &newText){}
void ScintillaEditor::replaceAll(const QString &findText, const QString &replaceText){}
void ScintillaEditor::getRange(int *lineFrom, int *lineTo){}
void ScintillaEditor::indentSelection(){}
void ScintillaEditor::unindentSelection(){}
void ScintillaEditor::commentSelection(){}
void ScintillaEditor::uncommentSelection(){}
QString ScintillaEditor::selectedText(){}
bool ScintillaEditor::eventFilter(QObject *obj, QEvent *e){}
bool ScintillaEditor::handleKeyEventBlockMove(QKeyEvent *keyEvent){}
bool ScintillaEditor::handleKeyEventBlockCopy(QKeyEvent *keyEvent){}
bool ScintillaEditor::handleKeyEventNavigateNumber(QKeyEvent *keyEvent){}
void ScintillaEditor::navigateOnNumber(int key){}
bool ScintillaEditor::modifyNumber(int key){}
void ScintillaEditor::onUserListSelected(const int, const QString &text){}
void ScintillaEditor::onAutocompleteChanged(bool state){}
void ScintillaEditor::onCharacterThresholdChanged(int val){}
void ScintillaEditor::setIndicator(const std::vector<IndicatorData>& indicatorData){}
void ScintillaEditor::onIndicatorClicked(int line, int col, Qt::KeyboardModifiers state){}
void ScintillaEditor::updateSymbolMarginVisibility(){}
void ScintillaEditor::toggleBookmark(){}
void ScintillaEditor::findMarker(int findStartOffset, int wrapStart, std::function<int(int)> findMarkerFunc){}
void ScintillaEditor::nextBookmark(){}
void ScintillaEditor::prevBookmark(){}
void ScintillaEditor::jumpToNextError(){}

// TAB WIDGET

TabWidget::TabWidget(QWidget *parent) : QTabBar(parent)
{
	stackWidget = new QStackedWidget(this);

	connect(this, SIGNAL(currentChanged(int)), this, SLOT(handleCurrentChanged(int)));
	connect(this, SIGNAL(tabMoved(int, int)), this, SLOT(handleTabMoved(int, int)));
}

TabWidget::~TabWidget()
{
}

QWidget *TabWidget::getContentWidget()
{
	return stackWidget;
}

int TabWidget::addTab(QWidget *w, const QString &label)
{
	stackWidget->addWidget(w);
	int idx = tabContent.size();
	tabContent.insert(idx, w);
	int i = this->insertTab(idx, label);
	fireTabCountChanged();
	return i;
}

void TabWidget::mouseReleaseEvent(QMouseEvent *event)
{
	QTabBar::mouseReleaseEvent(event);

	if (event->button() == Qt::MiddleButton) {
		emit middleMouseClicked(tabAt(event->pos()));
	}
}

void TabWidget::fireTabCountChanged()
{
	emit tabCountChanged(this->count());
}

void TabWidget::handleCurrentChanged(int i)
{
	stackWidget->setCurrentWidget(tabContent.at(i));
	emit currentTabChanged(i);
}

void TabWidget::handleTabMoved(int from, int to)
{
	tabContent.move(from, to);
}

void TabWidget::setCurrentWidget(int index)
{
	QTabBar::setCurrentIndex(index);
}

int TabWidget::indexOf(QWidget *w)
{
	return tabContent.indexOf(w);
}

QWidget *TabWidget::widget(int index)
{
	return tabContent.at(index);
}

void TabWidget::removeTab(int index)
{
	stackWidget->removeWidget(tabContent.at(index));
	tabContent.removeAt(index);
	QTabBar::removeTab(index);
}

// TAB MANAGER CLASS

TabManager::TabManager(MainWindow *o, const QString &filename)
{
    par = o;

    tabWidget = new TabWidget();
#if (QT_VERSION >= QT_VERSION_CHECK(5, 4, 0))
    tabWidget->setAutoHide(true);
#endif
    tabWidget->setExpanding(false);
    tabWidget->setTabsClosable(true);
    tabWidget->setMovable(true);
	tabWidget->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(tabWidget, SIGNAL(currentTabChanged(int)), this, SLOT(tabSwitched(int)));
    connect(tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTabRequested(int)));
    connect(tabWidget, SIGNAL(tabCountChanged(int)), this, SIGNAL(tabCountChanged(int)));
	connect(tabWidget, SIGNAL(middleMouseClicked(int)), this, SLOT(middleMouseClicked(int)));
	connect(tabWidget, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(showTabHeaderContextMenu(const QPoint&)));

    createTab(filename);

    connect(tabWidget, SIGNAL(currentTabChanged(int)), this, SLOT(stopAnimation()));
    connect(tabWidget, SIGNAL(currentTabChanged(int)), this, SLOT(updateFindState()));

    connect(par, SIGNAL(highlightError(int)), this, SLOT(highlightError(int)));
    connect(par, SIGNAL(unhighlightLastError()), this, SLOT(unhighlightLastError()));

    connect(par->editActionUndo, SIGNAL(triggered()), this, SLOT(undo()));
    connect(par->editActionRedo, SIGNAL(triggered()), this, SLOT(redo()));
    connect(par->editActionRedo_2, SIGNAL(triggered()), this, SLOT(redo()));
    connect(par->editActionCut, SIGNAL(triggered()), this, SLOT(cut()));
    connect(par->editActionCopy, SIGNAL(triggered()), this, SLOT(copy()));
    connect(par->editActionPaste, SIGNAL(triggered()), this, SLOT(paste()));

    connect(par->editActionIndent, SIGNAL(triggered()), this, SLOT(indentSelection()));
    connect(par->editActionUnindent, SIGNAL(triggered()), this, SLOT(unindentSelection()));
    connect(par->editActionComment, SIGNAL(triggered()), this, SLOT(commentSelection()));
    connect(par->editActionUncomment, SIGNAL(triggered()), this, SLOT(uncommentSelection()));

    connect(par->editActionToggleBookmark, SIGNAL(triggered()), this, SLOT(toggleBookmark()));
    connect(par->editActionNextBookmark, SIGNAL(triggered()), this, SLOT(nextBookmark()));
    connect(par->editActionPrevBookmark, SIGNAL(triggered()), this, SLOT(prevBookmark()));
    connect(par->editActionJumpToNextError, SIGNAL(triggered()), this, SLOT(jumpToNextError()));
}

QWidget *TabManager::getTabHeader()
{
    assert(tabWidget != nullptr);
    return tabWidget;
}

QWidget *TabManager::getTabContent()
{
    assert(tabWidget != nullptr);
    return tabWidget->getContentWidget();
}

void TabManager::tabSwitched(int x)
{
    assert(tabWidget != nullptr);
    editor = (EditorInterface *)tabWidget->widget(x);
    par->activeEditor = editor;

    if(editor == par->customizerEditor)
    {
        //par->parameterWidget->setEnabled(true);
    }
    else
    {
      //par->parameterWidget->setEnabled(false);
    }

    par->editActionUndo->setEnabled(editor->canUndo());
    par->changedTopLevelEditor(par->editorDock->isFloating());
    par->changedTopLevelConsole(par->consoleDock->isFloating());
    par->parameterTopLevelChanged(par->parameterDock->isFloating());
    par->setWindowTitle(tabWidget->tabText(x).replace("&&", "&"));

	for (int idx = 0;idx < tabWidget->count();idx++) {
		QWidget * button = tabWidget->tabButton(idx, QTabBar::RightSide);
		if (button) {
			button->setVisible(idx == x);
		}
	}
}

void TabManager::middleMouseClicked(int x)
{
	if (x < 0) {
		createTab("");
	} else {
		closeTabRequested(x);
	}
}

void TabManager::closeTabRequested(int x)
{
    assert(tabWidget != nullptr);
    if(!maybeSave(x))
        return;

    QWidget *temp = tabWidget->widget(x);
    editorList.remove((EditorInterface *)temp);
    tabWidget->removeTab(x);
    tabWidget->fireTabCountChanged();

    delete temp;
}

void TabManager::closeCurrentTab()
{
    assert(tabWidget != nullptr);

    /* Close tab or close the current window if only one tab is open. */
    if (tabWidget->count()>1)
        this->closeTabRequested(tabWidget->currentIndex());
    else
        par->close();
}

void TabManager::nextTab()
{
	assert(tabWidget != nullptr);
	tabWidget->setCurrentIndex((tabWidget->currentIndex() + 1) % tabWidget->count());
}

void TabManager::prevTab()
{
	assert(tabWidget != nullptr);
	tabWidget->setCurrentIndex((tabWidget->currentIndex() + tabWidget->count() - 1) % tabWidget->count());
}

void TabManager::actionNew()
{
    createTab("");
}

void TabManager::open(const QString &filename)
{
    assert(!filename.isEmpty());

    for(auto edt: editorList)
    {
        if(filename == edt->filepath)
        {
            tabWidget->setCurrentWidget(tabWidget->indexOf(edt));
            return;
        }
    }

    if(editor->filepath.isEmpty() && !editor->isContentModified())
    {
        openTabFile(filename);
    }
    else
    {
        createTab(filename);
    }
}

void TabManager::createTab(const QString &filename)
{
  assert(par != nullptr);
  editor = new ScintillaEditor(tabWidget);
  par->activeEditor = editor;
  // clearing default mapping of keyboard shortcut for font size
  QsciCommandSet *qcmdset = ((ScintillaEditor *)editor)->qsci->standardCommands();
  QsciCommand *qcmd = qcmdset->boundTo(Qt::ControlModifier | Qt::Key_Plus);
  qcmd->setKey(0);
  qcmd = qcmdset->boundTo(Qt::ControlModifier | Qt::Key_Minus);
  qcmd->setKey(0);
	connect(editor, SIGNAL(showContextMenuEvent(const QPoint&)), this, SLOT(showContextMenuEvent(const QPoint&)));
  ((ScintillaEditor *)editor)->public_applySettings();
	editor->addTemplate();
	QShortcut *viewTemplates = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Insert), editor);
	viewTemplates->setAutoRepeat(false);
  int idx = tabWidget->addTab(editor, _("Untitled.scad"));
  if(!editorList.isEmpty()) {
      tabWidget->setCurrentWidget(idx); // to prevent emitting of currentTabChanged signal twice for first tab
  }
}

int TabManager::count()
{
  return tabWidget->count();
}

void TabManager::highlightError(int i)
{
  editor->highlightError(i);
}

void TabManager::unhighlightLastError()
{
  editor->unhighlightLastError();
}

void TabManager::undo()
{
  editor->undo();
}

void TabManager::redo()
{
  editor->redo();
}

void TabManager::cut()
{
  editor->cut();
}

void TabManager::copy()
{
  editor->copy();
}

void TabManager::paste()
{
  editor->paste();
}

void TabManager::indentSelection()
{
    editor->indentSelection();
}

void TabManager::unindentSelection()
{
    editor->unindentSelection();
}

void TabManager::commentSelection()
{
    editor->commentSelection();
}

void TabManager::uncommentSelection()
{
    editor->uncommentSelection();
}

void TabManager::toggleBookmark()
{
    editor->toggleBookmark();
}

void TabManager::nextBookmark()
{
    editor->nextBookmark();
}

void TabManager::prevBookmark()
{
    editor->prevBookmark();
}

void TabManager::jumpToNextError()
{
    editor->jumpToNextError();
}

void TabManager::updateActionUndoState()
{
    par->editActionUndo->setEnabled(editor->canUndo());
}

void TabManager::applyAction(QObject *object, std::function<void(int, EditorInterface *)> func)
{
	QAction *action = dynamic_cast<QAction *>(object);
	if (action == nullptr) {
		return;
	}
	bool ok;
	int idx = action->data().toInt(&ok);
	if (!ok) {
		return;
	}

	EditorInterface *edt = (EditorInterface *)tabWidget->widget(idx);
	if (edt == nullptr) {
		return;
	}

	func(idx, edt);
}

void TabManager::copyFileName()
{
	applyAction(QObject::sender(), [](int, EditorInterface *edt){
		QClipboard *clipboard = QApplication::clipboard();
		clipboard->setText(QFileInfo(edt->filepath).fileName());
	});
}

void TabManager::copyFilePath()
{
	applyAction(QObject::sender(), [](int, EditorInterface *edt){
		QClipboard *clipboard = QApplication::clipboard();
		clipboard->setText(edt->filepath);
	});
}

void TabManager::closeTab()
{
	applyAction(QObject::sender(), [this](int idx, EditorInterface *){
		closeTabRequested(idx);
	});
}

void TabManager::showContextMenuEvent(const QPoint& pos)
{
	QMenu *menu = editor->createStandardContextMenu();

	QAction *separator = new QAction(menu->parent());
	separator->setSeparator(true);

	QAction *findAction = new QAction(menu->parent());
	findAction->setText(_("Find"));
	connect(findAction, SIGNAL(triggered()), par, SLOT(showFind()));

	bool enable = editor->findState != FIND_HIDDEN;
	QAction *findNextAction = new QAction(menu->parent());
	findNextAction->setText(_("Find Next"));
	findNextAction->setEnabled(enable);
	connect(findNextAction, SIGNAL(triggered()), par, SLOT(findNext()));

	QAction *findPrevAction = new QAction(menu->parent());
	findPrevAction->setText(_("Find Previous"));
	findPrevAction->setEnabled(enable);
	connect(findPrevAction, SIGNAL(triggered()), par, SLOT(findPrev()));

	menu->addSeparator();
	menu->addAction(findAction);
	menu->addAction(findNextAction);
	menu->addAction(findPrevAction);
	menu->exec(editor->mapToGlobal(pos));

	delete menu;
}

void TabManager::showTabHeaderContextMenu(const QPoint& pos)
{
	int idx = tabWidget->tabAt(pos);
	if (idx < 0) {
		return;
	}

	EditorInterface *edt = (EditorInterface *)tabWidget->widget(idx);

	QAction *copyFileNameAction = new QAction(tabWidget);
	copyFileNameAction->setData(idx);
	copyFileNameAction->setEnabled(!edt->filepath.isEmpty());
	copyFileNameAction->setText(_("Copy file name"));
	connect(copyFileNameAction, SIGNAL(triggered()), SLOT(copyFileName()));

	QAction *copyFilePathAction = new QAction(tabWidget);
	copyFilePathAction->setData(idx);
	copyFilePathAction->setEnabled(!edt->filepath.isEmpty());
	copyFilePathAction->setText(_("Copy full path"));
	connect(copyFilePathAction, SIGNAL(triggered()), SLOT(copyFilePath()));

	QAction *closeAction = new QAction(tabWidget);
	closeAction->setData(idx);
	closeAction->setText(_("Close Tab"));
	connect(closeAction, SIGNAL(triggered()), SLOT(closeTab()));

	QMenu menu;
	menu.addAction(copyFileNameAction);
	menu.addAction(copyFilePathAction);
	menu.addAction(closeAction);

	int x1, y1, x2, y2;
	tabWidget->tabRect(idx).getCoords(&x1, &y1, &x2, &y2);
	menu.exec(tabWidget->mapToGlobal(QPoint(x1, y2)));
}

void TabManager::setContentRenderState() //since last render
{
}

void TabManager::stopAnimation(){}

void TabManager::updateFindState(){}

void TabManager::setTabModified(bool mod, EditorInterface *edt){}

void TabManager::openTabFile(const QString &filename){}

void TabManager::setTabName(const QString &filename, EditorInterface *edt)
{
    if(edt == nullptr) {
        edt = editor;
    }

    QString fname;
    if (filename.isEmpty()) {
        edt->filepath.clear();
        fname = _("Untitled.scad");
        tabWidget->setTabText(tabWidget->indexOf(edt), fname);
        tabWidget->setTabToolTip(tabWidget->indexOf(edt), fname);
    } else {
        QFileInfo fileinfo(filename);
        edt->filepath = fileinfo.absoluteFilePath();
        fname = fileinfo.fileName();
        tabWidget->setTabText(tabWidget->indexOf(edt), QString(fname).replace("&", "&&"));
        tabWidget->setTabToolTip(tabWidget->indexOf(edt), fileinfo.filePath());
        //par->parameterWidget->readFile(edt->filepath);
        QDir::setCurrent(fileinfo.dir().absolutePath());
    }
    par->editorTopLevelChanged(par->editorDock->isFloating());
    par->changedTopLevelConsole(par->consoleDock->isFloating());
    par->parameterTopLevelChanged(par->parameterDock->isFloating());
    par->setWindowTitle(fname);
}

void TabManager::refreshDocument(){}

bool TabManager::maybeSave(int x){}

bool TabManager::shouldClose(){}

void TabManager::saveError(const QIODevice &file, const std::string &msg, EditorInterface *edt){}

void TabManager::save(EditorInterface *edt){}

void TabManager::saveAs(EditorInterface *edt){}

void TabManager::saveAll(){}

void TabManager::onHyperlinkIndicatorClicked(int val)
{
    const QString filename = QString::fromStdString(editor->indicatorData[val].path);
    this->open(filename);
}



// MAIN WINDOW CLASS

MainWindow::MainWindow(const QStringList &filenames)
	: library_info_dialog(nullptr), font_list_dialog(nullptr), procevents(false), tempFile(nullptr),
      progresswidget(nullptr), includes_mtime(0), deps_mtime(0), last_parser_error_pos(-1)
{
	setupUi(this);

	editorDockTitleWidget = new QWidget();
	consoleDockTitleWidget = new QWidget();

	this->editorDock->setConfigKey("view/hideEditor");
	this->editorDock->setAction(this->viewActionHideEditor);
	this->consoleDock->setConfigKey("view/hideConsole");
	this->consoleDock->setAction(this->viewActionHideConsole);
	this->parameterDock->setConfigKey("view/hideCustomizer");
	this->parameterDock->setAction(this->viewActionHideParameters);

	this->versionLabel = nullptr; // must be initialized before calling updateStatusBar()

	tabManager = new TabManager(this, filenames.isEmpty() ? QString() : filenames[0]);
	//connect(tabManager, SIGNAL(tabCountChanged(int)), this, SLOT(setTabToolBarVisible(int)));
	//this->setTabToolBarVisible(tabManager->count());
	tabToolBarContents->layout()->addWidget(tabManager->getTabHeader());
	editorDockContents->layout()->addWidget(tabManager->getTabContent());

	setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
	setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
	setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
	setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

	this->setAttribute(Qt::WA_DeleteOnClose);

	scadApp->windowManager.add(this);

	//animate_panel->hide();
	//this->hideFind();
	//frameCompileResult->hide();
	//this->labelCompileResultMessage->setOpenExternalLinks(false);
	//connect(this->labelCompileResultMessage, SIGNAL(linkActivated(QString)), SLOT(showConsole()));

	//this->menuOpenRecent->addSeparator();
	//this->menuOpenRecent->addAction(this->fileActionClearRecent);

	show();

}

MainWindow::~MainWindow(){}

void MainWindow::on_editorDock_visibilityChanged(bool)
{
	changedTopLevelEditor(editorDock->isFloating());
	tabToolBar->setVisible((tabCount > 1) && editorDock->isVisible());
}

void MainWindow::on_consoleDock_visibilityChanged(bool)
{
	changedTopLevelConsole(consoleDock->isFloating());
}

void MainWindow::on_parameterDock_visibilityChanged(bool)
{
    parameterTopLevelChanged(parameterDock->isFloating());
}

void MainWindow::changedTopLevelEditor(bool topLevel)
{
	setDockWidgetTitle(editorDock, QString(_("Editor")), topLevel);
}

void MainWindow::editorTopLevelChanged(bool topLevel)
{
	setDockWidgetTitle(editorDock, QString(_("Editor")), topLevel);
	if(topLevel)
	{
		this->removeToolBar(tabToolBar);
		((QVBoxLayout *)editorDockContents->layout())->insertWidget(0, tabToolBar);
	}
	else
	{
		editorDockContents->layout()->removeWidget(tabToolBar);
		this->addToolBar(tabToolBar);
	}
	tabToolBar->setVisible((tabCount > 1) && editorDock->isVisible());
}

void MainWindow::changedTopLevelConsole(bool topLevel)
{
	setDockWidgetTitle(consoleDock, QString(_("Console")), topLevel);
}

void MainWindow::consoleTopLevelChanged(bool topLevel)
{
	setDockWidgetTitle(consoleDock, QString(_("Console")), topLevel);

    Qt::WindowFlags flags = (consoleDock->windowFlags() & ~Qt::WindowType_Mask) | Qt::Window;
	if(topLevel)
	{
		consoleDock->setWindowFlags(flags);
		consoleDock->show();
	}
}

void MainWindow::parameterTopLevelChanged(bool topLevel)
{
    setDockWidgetTitle(parameterDock, QString(_("Customizer")), topLevel);
}

void MainWindow::setDockWidgetTitle(QDockWidget *dockWidget, QString prefix, bool topLevel)
{
	QString title(prefix);
	if (topLevel) {
		const QFileInfo fileInfo(activeEditor->filepath);
		QString fname = _("Untitled.scad");
		if(!fileInfo.fileName().isEmpty())
			fname = fileInfo.fileName();
		title += " (" + fname.replace("&", "&&") + ")";
	}
	dockWidget->setWindowTitle(title);
}

void MainWindow::hideToolbars(){}

void MainWindow::hideEditor(){}

void MainWindow::showConsole(){}

void MainWindow::hideConsole(){}

void MainWindow::hideParameters(){}

void MainWindow::dragEnterEvent(QDragEnterEvent *event){}

void MainWindow::dropEvent(QDropEvent *event)
{

}

void MainWindow::handleFileDrop(const QString &filename)
{

}

void MainWindow::helpAbout(){}

void MainWindow::helpHomepage(){}

void MainWindow::helpManual(){}

void MainWindow::helpCheatSheet(){}

void MainWindow::helpLibrary(){}

void MainWindow::helpFontInfo()
{
	/*if (!this->font_list_dialog) {
		auto dialog = new FontListDialog();
		this->font_list_dialog = dialog;
	}
	this->font_list_dialog->update_font_list();
	this->font_list_dialog->show();*/
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	//if (tabManager->shouldClose()) {
		//QSettingsCached settings;
		//settings.setValue("window/size", size());
		//settings.setValue("window/position", pos());
		//settings.setValue("window/state", saveState());
		//if (this->tempFile) {
		//	delete this->tempFile;
			//this->tempFile = nullptr;
		//}
		//this->editorDock->disableSettingsUpdate();
		//this->consoleDock->disableSettingsUpdate();
		//this->parameterDock->disableSettingsUpdate();
		//event->accept();
	//} else {
	//	event->ignore();
	//}
}

void MainWindow::preferences()
{
	//Preferences::inst()->show();
	//Preferences::inst()->activateWindow();
	//Preferences::inst()->raise();
}



void MainWindow::setFont(const QString &family, uint size)
{
	QFont font;
	if (!family.isEmpty()) font.setFamily(family);
	else font.setFixedPitch(true);
	if (size > 0)	font.setPointSize(size);
	font.setStyleHint(QFont::TypeWriter);
	activeEditor->setFont(font);
}

void MainWindow::quit()
{
	QCloseEvent ev;
	QApplication::sendEvent(QApplication::instance(), &ev);
	if (ev.isAccepted()) QApplication::instance()->quit();
  // FIXME: Cancel any CGAL calculations
#ifdef Q_OS_MAC
	CocoaUtils::endApplication();
#endif
}

void MainWindow::consoleOutput(const std::string &msg, void *userdata)
{
	// Invoke the method in the main thread in case the output
	// originates in a worker thread.
	auto thisp = static_cast<MainWindow*>(userdata);
	QMetaObject::invokeMethod(thisp, "consoleOutput", Q_ARG(QString, QString::fromStdString(msg)));
}

void MainWindow::consoleOutput(const QString &msg)
{
	auto c = this->console->textCursor();
	c.movePosition(QTextCursor::End);
	this->console->setTextCursor(c);

	if (msg.startsWith("WARNING:") || msg.startsWith("DEPRECATED:")) {
		this->compileWarnings++;
		this->console->appendHtml("<span style=\"color: black; background-color: #ffffb0;\">" + QT_HTML_ESCAPE(QString(msg)) + "</span>");
	} else if (msg.startsWith("UI-WARNING:") || msg.startsWith("FONT-WARNING:") || msg.startsWith("EXPORT-WARNING:")) {
		this->console->appendHtml("<span style=\"color: black; background-color: #ffffb0;\">" + QT_HTML_ESCAPE(QString(msg)) + "</span>");
	} else if (msg.startsWith("ERROR:")) {
		this->compileErrors++;
		this->console->appendHtml("<span style=\"color: black; background-color: #ffb0b0;\">" + QT_HTML_ESCAPE(QString(msg)) + "</span>");
	} else if (msg.startsWith("EXPORT-ERROR:") || msg.startsWith("UI-ERROR:") || msg.startsWith("PARSER-ERROR:")) {
		this->console->appendHtml("<span style=\"color: black; background-color: #ffb0b0;\">" + QT_HTML_ESCAPE(QString(msg)) + "</span>");
	} else if (msg.startsWith("TRACE:")) {
		this->console->appendHtml("<span style=\"color: black; background-color: #d0d0ff;\">" + QT_HTML_ESCAPE(QString(msg)) + "</span>");
	} else {
		QString qmsg = msg;
		if(qmsg.contains('\t') && !qmsg.contains("<pre>", Qt::CaseInsensitive))
			this->console->appendPlainText(qmsg);
		else {
			qmsg.replace("\n","<br>");
			this->console->appendHtml(qmsg);
		}
	}
	this->processEvents();
}

void MainWindow::setCurrentOutput()
{
	set_output_handler(&MainWindow::consoleOutput, this);
}

void MainWindow::hideCurrentOutput()
{
	set_output_handler(&MainWindow::noOutput, this);
}

void MainWindow::clearCurrentOutput()
{
	set_output_handler(nullptr, nullptr);
}



void MainWindow::processEvents()
{
	if (this->procevents) QApplication::processEvents();
}









int main(int argc, char **argv)
{
	int rc = 0;
	StackCheck::inst();
	{
    // Need a dummy app instance to get the application path but it needs to be destroyed before the GUI is launched.
		QCoreApplication app(argc, argv);
		PlatformUtils::registerApplicationPath(app.applicationDirPath().toLocal8Bit().constData());
	}
	//auto original_path = fs::current_path();
	//vector<string> inputFiles;
	//currentdir = fs::current_path().generic_string();
  OpenSCADApp app(argc, argv);

  app.setStyleSheet("QStatusBar::item { border: 0px solid black; }");
	QCoreApplication::setOrganizationName("OpenSCAD");
	QCoreApplication::setOrganizationDomain("openscad.org");
	QCoreApplication::setApplicationName("OpenSCAD");
	QCoreApplication::setApplicationVersion(TOSTRING(OPENSCAD_VERSION));
	QGuiApplication::setApplicationDisplayName("OpenSCAD");
	QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
	app.setWindowIcon(QIcon(":/icons/openscad.png"));
	QStringList inputFilesList;
	new MainWindow(inputFilesList);
	app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(releaseQSettingsCached()));
	app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
	return app.exec();
}
