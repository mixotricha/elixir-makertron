include(version.pri)
include(defaults.pri)
include(common.pri)

# Local settings are read from local.pri
exists(local.pri): include(local.pri)

INCLUDEPATH += src
DEPENDPATH += src

TARGET = openscad$${SUFFIX}

FULLNAME = openscad$${SUFFIX}
APPLICATIONID = org.openscad.OpenSCAD
!isEmpty(SUFFIX): DEFINES += INSTALL_SUFFIX="\"\\\"$${SUFFIX}\\\"\""

# Set same stack size for the linker and #define used in PlatformUtils.h
STACKSIZE = 8388608 # 8MB # github issue 116
QMAKE_CXXFLAGS += -DSTACKSIZE=$$STACKSIZE
DEFINES += STACKSIZE=$$STACKSIZE

QT += widgets printsupport

# Application configuration
CONFIG += qt
#object_parallel_to_source
CONFIG += c++11
CONFIG += boost
CONFIG += glib-2.0
CONFIG += scintilla

RESOURCES = openscad.qrc

FORMS   += src/MainWindow.ui \
           src/Console.ui

HEADERS += src/MainWindow.h \
           src/tabmanager.h \
           src/tabwidget.h \
           src/OpenSCADApp.h \
           src/WindowManager.h \
           src/boost-utils.h \
           src/Dock.h \
           src/Console.h \
           src/editor.h \
           src/scintillaeditor.h

SOURCES += \
           src/printutils.cc \
           src/boost-utils.cc \
           src/PlatformUtils.cc \
           src/version.cc \
           src/openscad.cc

unix:!macx {
  SOURCES += src/PlatformUtils-posix.cc
}

isEmpty(PREFIX):PREFIX = /usr/local

target.path = $$PREFIX/bin/
INSTALLS += target
